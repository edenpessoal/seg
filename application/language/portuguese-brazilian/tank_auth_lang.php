<?php

// Errors
$lang['auth_incorrect_password'] = 'Senha incorreta';
$lang['auth_incorrect_login'] = 'Login incorreto';
$lang['auth_incorrect_email_or_username'] = 'Login ou e-mail inexistente';
$lang['auth_email_in_use'] = 'Este e-mail já está em uso. Por favor, escolha outro e-mail.';
$lang['auth_username_in_use'] = 'Este nome de usuário já está em uso. Por favor, escolha outro nome de usuário.';
$lang['auth_current_email'] = 'Este é o seu e-mail atual.';
$lang['auth_incorrect_captcha'] = 'O código de confirmação não é o mesmo da imagem.';
$lang['auth_captcha_expired'] = 'Seu código de confirmação expirou. Por favor, tente novamente.';

// Notifications
$lang['auth_message_logged_out'] = 'Você foi deslogado com sucesso.';
$lang['auth_message_registration_disabled'] = 'O registro está desabilitado.';
$lang['auth_message_registration_completed_1'] = 'Vocẽ foi registrado com sucesso. Cheque seu e-mail para ativar o cadastro.';
$lang['auth_message_registration_completed_2'] = 'Você foi registrado com sucesso.';
$lang['auth_message_activation_email_sent'] = 'Novo e-mail de ativação enviado para %s. Siga as instruções do e-mail para ativar seu cadastro.';
$lang['auth_message_activation_completed'] = 'Cadastro ativado com sucesso.';
$lang['auth_message_activation_failed'] = 'O código de ativação inserido é inválido ou expirou.';
$lang['auth_message_password_changed'] = 'Senha alterada com sucesso.';
$lang['auth_message_new_password_sent'] = 'Um e-mail com instruções para criar uma nova senha foi enviado.';
$lang['auth_message_new_password_activated'] = 'Senha alterada com sucesso';
$lang['auth_message_new_password_failed'] = 'O código de ativação inserido é inválido ou expirou.';
$lang['auth_message_new_email_sent'] = 'Novo e-mail de ativação enviado para %s. Siga as instruções do e-mail para ativar seu cadastro.';
$lang['auth_message_new_email_activated'] = 'E-mail alterado com sucesso';
$lang['auth_message_new_email_failed'] = 'O código de ativação inserido é inválido ou expirou.';
$lang['auth_message_banned'] = 'Conta banida.';
$lang['auth_message_unregistered'] = 'Sua conta foi deletada...';

// Email subjects
$lang['auth_subject_welcome'] = 'Bem vindo ao %s!';
$lang['auth_subject_activate'] = 'Bem vindo ao %s!';
$lang['auth_subject_forgot_password'] = 'Esqueceu sua senha do %s?';
$lang['auth_subject_reset_password'] = 'Sua nova senha no %s';
$lang['auth_subject_change_email'] = 'Seu e-mail no %s';

