<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="">
<head>
	<meta charset="utf-8">

	<title></title>

	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/jquery-ui.css">

	<script src="<?php echo base_url(); ?>/assets/js/main.js"></script>

	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/main.css">

	<!-- Inicio CSS mimi -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap/bootstrap.min.css">
	<script src="<?php echo base_url(); ?>/assets/js/vendor/core/bootstrap.js"></script>
	<!-- Final  CSS mimi -->
</head>
<body>

<iframe name="iframeSubmit" id="iframeSubmit" width="0" height="0"></iframe>

<div id="primeira_tela">

	<div class="modal fade" id="forgot_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form class="form-horizontal" id="formForgotPassword" method="post">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Esqueci minha senha</h4>
					</div>
					<div class="modal-body form-horizontal">
						<div class="control-group">
							<label for="email" class="control-label">Digite seu e-mail</label>
							<div class="controls">
								<input name="email" id="email" type="email">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
						<button type="submit" id="forgotPasswordModalSend" data-loading-text="Enviando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Enviar</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php
		if($isResetPassword) {
	?>
			<div class="modal fade" id="reset_password_modal" tabindex="-1" role="dialog"
				 aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form class="form-horizontal" id="formResetPassword" method="post">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
										aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Cadastrar nova senha</h4>
							</div>
							<div class="modal-body form-horizontal">
								<div class="control-group">
									<label for="new_password" class="control-label">Nova senha</label>
									<div class="controls">
										<input name="new_password" id="new_password" type="password" required>
									</div>
								</div>
							</div>
							<div class="modal-body form-horizontal">
								<div class="control-group">
									<label for="confirm_new_password" class="control-label">Confirmar nova senha</label>
									<div class="controls">
										<input name="confirm_new_password" id="confirm_new_password" type="password" required>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
								<button type="submit" id="forgotPasswordResetSend" data-loading-text="Enviando..."
										class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1"
										data-animated>Enviar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	<?php
		}
	?>

	<!-- Primeira tela - Login e 1o cadastro -->
	<!-- Inicio  Top Header -->
	<div class="top-head">
		<div class="container">
			<!-- Login e senha -->
			<div class="row">
				<form target="iframeSubmit" id="formLogin" name="formLogin" method="post" action="<?php echo base_url(); ?>/login_cadastro/login">
					<div id="loginCampos" class="col-md-12 ">
						<div class="col-md-4"> </div>
					   <div class="col-md-8">
							<div class="col-md-4">					
									<label for="loginEmail">E-mail</label>
									<input  type="email" id="login" name="login" class="form-control" required />							
							</div>
							<div class="col-md-4">
									<label for="loginSenha">Senha</label>
									<input type="password" id="password" name="password" class="form-control" required/>
							</div>
							<div class="col-md-4 mt25">
								<input type="submit" data-loading-text="Carregando..." id="loginSubmit" name="loginSubmit" value="Entrar" class="btn btn-primary" />
							</div>
							<div class="row">
								<div class="col-md-12">	
										<div class="col-md-4">
									       <input type="checkbox" id="remember" name="remember" value="1"  class="">
									       <label for="remember" class="">Permanecer conectado</label>
									  </div>
									   <div class="col-md-4">
									       <a data-toggle="modal" data-target="#forgot_password_modal" href="#">Esqueci minha senha</a>
									   </div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<!-- FIM Login e senha -->
		</div>
	</div>
	<!-- FIM  Top Header -->


	<form id="formCadastroBasico" target="iframeSubmit"  action="<?php echo base_url(); ?>/login_cadastro/register" name="formCadastroBasico" method="post">
		<!-- Inicio primeiro cadastro -->
		<div class="sessaoprimeirocadastro">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1>Controle total de sua empresa!</h1>
						<h3>Tenha sua equipe integrada e distribua metas!</h3>
					</div>
					<div class="col-md-4 col-md-offset-2">
						<div id="cadastro_basico">
							<div class="col-md-12">
								<h2>Cadastre sua empresa</h2>
								<h5>3 meses grátis *</h5>
							</div>
							<div class="row">
								<!-- Cadastro (primeira parte) -->
								<div class="col-md-12 transparenciaFormularioprimeiraparte">
									<div class="col-md-12">
										<input type="email" id="email" name="email" placeholder="E-mail" class="form-control" required />
									</div>
									<div class="col-md-12">
										<input type="password" id="password" name="password" placeholder="Senha" class="form-control" required />
									</div>
									<div class="col-md-12">
										<input type="password" id="confirm_password" name="confirm_password" placeholder="Repita a senha" class="form-control" required />
									</div>
									<?php
									if ($captcha_registration) { ?>
									<div class="col-md-12">
										<?php echo $captcha_html; ?>
									</div>
	<div class="col-md-12">
		<input type="text" id="captcha" name="captcha" placeholder="Digite o texto acima" maxlength="4" required class="form-control"/>
	</div>
									<?php
									}
									?>
									<div class="col-md-12">
										<input type="submit" data-loading-text="Carregando..." id="envio_cadastro" name="envio_cadastro" value="Cadastrar" class="pull-right btn btn-primary"/>
									</div>
									<!-- FIM Cadastro (primeira parte) -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
		<!--FIM primeiro cadastro -->

</div>
</body>
</html>