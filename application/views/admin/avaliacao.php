<!-- Estilo da página-->
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/admin/avaliacao.css">

<?php 
	//var_dump($this->usuario); 
	//$this->usuario['foto_perfil']; 
?>

<?php if ($this->usuario->setor === \Application\Models\Funcionario\Setor::VENDAS):?>
<!-- 
    Condição caso o SETOR da session for setor V 
-->
 <div class="col-md-12  col-sm-12 fundobranco mb20 mt20 borda">
    <div class="row">
		<div class="col-md-12 col-sm-12 mb10">
		   <h3>Minhas Avaliações</h3>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
			 <small>
			        *Esses valores serão validados pelo seu supervisor.
			</small>
		<div class="box box-blue box-example-square">
			<div class="box-header">Vendas Faturadas nessa semana</div>
			           <div class="box-body" style="height: 138px;padding-top: 0px;">
							 <table id="vendaFaturamentoSemanaAtual">
							 	<thead>
							 		<tr>
							 			<th>Dom</th>
							 			<th>Seg</th>
							 			<th>Ter</th>
							 			<th>Qua</th>
							 			<th>Qui</th>
							 			<th>Sex</th>
							 			<th>Sab</th>
							 		</tr>
							 	</thead>
							 	<tbody>
							 	<tr class="valores"> 
							 		<td width="100px">
							 		-- 
							 		</td>
							 		<td width="100px">
									--
							 		</td>
							 		<td width="100px">
									--							 			
							 		</td>
							 		<td width="100px">
									--							 			
							 		</td>
							 		<td width="100px">
									--
							 		</td>
							 		<td width="100px">
									--
							 		</td>
							 		<td width="100px">
									--
							 		</td>
							 	</tr>
							 	<tr>
							 		<td class="semborda"></td>
							 		<td class="semborda"></td>	
							 		<td class="semborda"></td>
							 		<td class="semborda"></td>
							 		<td class="semborda" style="text-align: right;"> 
							 		  <label>
							 		    Total: 
							 		  </label>
							 		</td>
     						 		<td class="semborda">
							 		   <h4 style="text-align: right;">R$ </h4>
							 		</td>
							 		<td class="semborda">
							 		  <h4>
							 		    --
							 		  </h4> 
							 		</td>
							 	</tr>
							 	</tbody>
							 </table>
							 
			        </div>
	
			</div>
	    </div>

	<div class="col-md-6">
		<div class="box-header">FATURAMENTO DE ONTEM</div>	
		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Dia</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		         	    <!-- Inicio do template Colaborador  de VENDAS  -->           
			            <tr>  
					       <td>
					         <!-- local para o colaborador inserir o valor do dia de ontem--> 
								<div class="input-group input-group-lg">
								  <span class="input-group-addon" id="sizing-addon1">R$</span>
								  <input type="text" class="form-control" placeholder="0,00" aria-describedby="sizing-addon1" value="">
								</div>
					       </td>
			        	</tr>
						   <!-- final do template do Colaborador  de VENDAS  -->
						<tr>
					        <td>
					          <button class="btn btn-info" href="#"> Inserir </button>
					        </td>						   	
						</tr>
			   </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<div class="box-header">Histórico</div>
		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Valor soma da semana</th>
			           <th class="width-150 text-center">Data da aprovacao do supervisor</th>

			        </tr>
			     </thead>
			     <tbody>
	<!-- Inicio do template Colaborador  de VENDAS  -->
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
<img src="<?php echo !empty($this->usuario->foto_perfil) ? $this->usuario->foto_perfil : '/assets/img/semfoto.png'; ?>" class="media-photo imgmedalha" id="foto_perfil">					          
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>
										<?php echo $this->usuario->nome ?>
					               </span>
					            </td>    
					          <td>
		<!-- Soma da ultima semana aprovada valor aprovado-->
					         	--	            
					          </td>
                             <!-- data da inserçao do valor que foi aprovado-->
					          <td>
					          	<div class="avaliacaocalendariomes">AGO </div>
					          	<div class="avaliacaocalendariodia">8 </div>
					          </td>
			        	   </tr>
	<!-- final do template do Colaborador  de VENDAS  -->
			   </tbody>
		</table>



	</div>
</div>


<script src="/assets/js/pages/avaliacao/avaliacao_faturamento.js"></script>
<?php else:?>

<!-- 
Condição caso o SETOR da session for setor "O" 
-->
	<div class="col-md-12  col-sm-12 fundobranco mb20 mt20 borda">
	    <div class="row">
			<div class="col-md-12 col-sm-12 mb10"><h3>Minhas Avaliações</h3></div>
	    </div>
	</div>

	<div class="col-md-6 col-sm-6">
		<div class="box-header">
		    NOTA DE DESEMPENHO :
		   <label data-toggle="tooltip" 
		    title="Essa é a semana que você recebeu sua última avaliação" id="semananota">
		     Semana 12
		    </label>
		</div>	
		<table class="table text-subhead v-middle fundobranco" id="historicoavaliacaopontos">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		    <!-- Inicio do template do subordinado de pontos  -->
			        <tr class="hide@" id="">
				            <!--Imagem de foto perfil do subordinado-->	
					        <td>
								<img src="<?php echo !empty($this->usuario->foto_perfil) ? $this->usuario->foto_perfil : '/assets/img/semfoto.png'; ?>" class="media-photo imgmedalha" id="foto_perfil">			   
							</td> 
					         <!-- Nome do subordinado em questao-->  
					        <td> 
					            <span id="nome">
					                  <?php echo $this->usuario->nome ?>
					            </span>
					        </td>    
					        <td>
					        <!-- nota recebida --> 
					         <span class="btn-glyphicon notarecebidabloco" id="ponto">
					         1
					         </span>	            
					        </td>
					         <!-- data da inserçao da nota recebida--> 	 
					         <td>
					           <div class="avaliacaocalendariomes" id="mes">AGO</div>
					           <div class="avaliacaocalendariodia" id="dia">19</div>
					         </td>
			        </tr>
			<!-- final do template do subordinado de pontos  -->
			   </tbody>
		</table>

	</div>

<div class="col-md-6  col-sm-6">
		<div class="box-header">Histórico</div>
		<table class="table text-subhead v-middle fundobranco" 
		id="historicoavaliacaopontos">
		    <thead style="display: none;">
			    <tr>
			       <th class="width-150 text-center">Foto</th>
			       <th class="width-150 text-center">Nome</th>   
			       <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			   </tr>
			</thead>
			<tbody>
	 <!-- Inicio do template de pontos  -->           
			<tr class="hide@" id="">
				    <td>
					   <img src="<?php echo !empty($this->usuario->foto_perfil) ? $this->usuario->foto_perfil : '/assets/img/semfoto.png'; ?>" class="media-photo imgmedalha" id="foto_perfil">			   
					</td> 
					<td> 
					    <span id="nome">
					        <?php echo $this->usuario->nome ?>
					    </span>
					</td>    
					<td>
			<!--  nota recebida vinda do banco de dados--> 
					  <span class="btn-glyphicon" style="background: #bcbcbc;border-radius: 100%;color: #fff;padding: 9px 16px;" id="ponto">
					         2
					    </span>	            
					</td>
		    <!-- data da inserçao da nota recebida--> 	 
					<td>
					    <div class="avaliacaocalendariomes" id="mes">AGO</div>
					    <div class="avaliacaocalendariodia" id="dia">12</div>
				   </td>
			</tr>
	 <!-- Final do template de pontos  -->

			   </tbody>
		</table>

<!-- Inicio do Aviso Caso ele não tenha sido avaliado anteriormente -->	
<!-- 		<div class="alert alert-danger" role="alert" id="aviso">
		     <strong>Aviso:</strong>
			 Você ainda não tem pontos atribuidos para você. <br>
			 Aguarde até segunda-feira.
		</div> -->
<!-- Final do Aviso Caso ele não tenha sido avaliado anteriormente -->	


	</div>




<script src="/assets/js/pages/avaliacao/avaliacao_pontos.js"></script>


 <?php endif;?>

