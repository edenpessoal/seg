<link rel="stylesheet" href="/assets/css/admin/tarefas_equipe.css">
<script src="/assets/js/pages/tarefas-equipe.js"></script>

<div class="mt20">

<div class="row">
	    <div class="col-md-12">

			<div class="pull-right">
					<button class="mb30 btn btn-default  pull-right btn-sm" data-toggle="modal" data-target="#myModalAddTarefa">
					       <i class="fa fa-plus"></i> Nova Tarefa
					</button>
			</div>	
        </div>
</div>
	<div class="row">
        <div class="col-md-4">
            <!-- begin panel group -->
            <div class="panel-group" id="" role="tablist" aria-multiselectable="true">
 				<table class="table table-filter todoTabela" id="subordinadolistagem">
					<tbody>					

							<!-- Inicio de template -->
							<tr data-status="pendentes" id='id_funcionario' class="side-tab" data-target="#id_funcionario" data-toggle="tab" role="tab" aria-expanded="false">

										<td style="cursor: default;" id="id_funcionario2"  class="hide">
											<div class="media">
												<a href="#" class="pull-left">
											<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
											<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">
									               <span class="media-meta pull-right">
														 <a id='visualizar' href="#"  style="font-size: 18px;">
															 <span class="glyphicon btn-glyphicon fa fa-eye img-circle text-success"></span>
														 </a>									               

													 </span>
													<h4 class="title" id="nomeFuncionario"></h4>
												</div>
											</div>
										</td>
								</tr>
							<!-- Final de template -->
						

							<!-- Inicio de template -->
							<tr data-status="pendentes" id='id_funcionario' class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">

										<td style="cursor: default;" id="id_funcionario2"  >
											<div class="media">
												<a href="#" class="pull-left">
											<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
											<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">
									               <span class="media-meta pull-right">
														 <a id='visualizar' href="#"  style="font-size: 18px;">
															 <span class="glyphicon btn-glyphicon fa fa-eye img-circle text-success"></span>
														 </a>									               

													 </span>
													<h4 class="title" id="nomeFuncionario">Luana</h4>
												</div>
											</div>
										</td>
								</tr>
							<!-- Final de template -->

							<!-- Inicio de template -->
							<tr data-status="pendentes" id='id_funcionario' class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">

										<td style="cursor: default;" id="id_funcionario2"  >
											<div class="media">
												<a href="#" class="pull-left">
											<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
											<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">
									               <span class="media-meta pull-right">
														 <a id='visualizar' href="#"  style="font-size: 18px;">
															 <span class="glyphicon btn-glyphicon fa fa-eye img-circle text-success"></span>
														 </a>									               

													 </span>
													<h4 class="title" id="nomeFuncionario">teste</h4>
												</div>
											</div>
										</td>
								</tr>
							<!-- Final de template -->
							<!-- Inicio de template -->
							<tr data-status="pendentes" id='id_funcionario' class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">

										<td style="cursor: default;" id="id_funcionario2"  >
											<div class="media">
												<a href="#" class="pull-left">
											<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
											<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">
									               <span class="media-meta pull-right">
														 <a id='visualizar' href="#"  style="font-size: 18px;">
															 <span class="glyphicon btn-glyphicon fa fa-eye img-circle text-success"></span>
														 </a>									               

													 </span>
													<h4 class="title" id="nomeFuncionario">Michelle Pacheco2</h4>
												</div>
											</div>
										</td>
								</tr>
							<!-- Final de template -->
						</tbody>
					</table>               
               
            </div> <!-- / panel-group -->
             
        </div> <!-- /col-md-4 -->
        
        <div class="col-md-8">
            <!-- begin macbook pro mockup -->
            <div class="md-macbook-pro md-glare">
                <div class="md-lid">
                    <div class="md-screen">
                    <!-- content goes here -->                
                        <div class="tab-featured-image">

                           <div class="tab-content" style="padding: 0px;">
    <!-- inicio da aba da tarefa do subordinado--> 
           <div class="tab-pane  in active" id="tab1">
                               <!-- inicio do notepadTopchamps-->   
							 			<div class=" " id="notepadTopchamps">
											<div class="main-content panel panel-default no-margin">
												<div class="panel-body">
							                           <header class="panel-heading clearfix">
							                               <div class="view-switcher">
							                                   <h2>TO DO</h2>
							                               </div>
							                             </header>
													        <div class="table-container">
																		<table class="table table-filter todoTabela" id="tarefas_table">
																			<tbody>
																			<!-- Inicio de template com classe btn-default para tarefas não finalizadas -->
																				<tr data-status="pendentes" id='tarefa_pendente' class="hide">
																					<td style="cursor: default;"> 
																				        <label for="success" class="btn btn-default" style="cursor: default;">  
																				               <input type="checkbox" id="success_taskAberto" name='' class="badgebox" value="checado">
																				               <span class="badge">&check;</span>
																				         </label>
																					</td>
																			<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
																					<td style="cursor: default;"></td>
																					<td style="cursor: default;">
																						<div class="media">
																							<a href="#" class="pull-left">
																								<!--Imagem de foto perfil de quem criou a tarefa-->
																								<img src="/assets/img/avatar/default_user.png" class="media-photo">
																								<!--Imagem de foto perfil de quem criou a tarefa-->
																							</a>

																							<div class="media-body">							
																								<h4 class="title" id="task_details"></h4>
																							</div>
																						</div>
																					</td>
																				</tr>
																			<!-- Final de template com classe btn-default para tarefas não finalizadas -->



																			<!-- Inicio de template com classe btn-success para tarefas finalizadas -->
											<!--								-->
																			</tbody>
																		</table>
													             </div>
							                    </div>        	
							               </div>
							          </div>
							   <!-- final do notepadTopchamps-->   
             </div> 
<!-- final da aba da tarefa do subordinado-->   


<!-- inicio da aba da tarefa do subordinado--> 
     <div class="tab-pane " id="tab2">
                               <!-- inicio do notepadTopchamps-->   
							 			<div class="col-md-12" id="notepadTopchamps">
											<div class="main-content panel panel-default no-margin">
												<div class="panel-body">
							                           <header class="panel-heading clearfix">
							                               <div class="view-switcher">
							                                   <h2>TO DO</h2>
							                               </div>
							                             </header>
		<div class="table-container">

							<table class="table table-filter todoTabela" id="tarefas_tableSubordinados">
<tbody><tr data-status="pendentes" id="linha_58" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_58" name="check_58" class="badgebox" value="checado" checked="checked">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">

													<h4 class="title" id="task_details">tarefa 2</h4>
												</div>
											</div>
										</td>
									</tr><tr data-status="pendentes" id="linha_49" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_49" name="check_49" class="badgebox" value="checado">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">
													<h4 class="title" id="task_details">teste de tarefa subordinado teste</h4>
												</div>
											</div>
										</td>
									</tr><tr data-status="pendentes" id="linha_48" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_48" name="check_48" class="badgebox" value="checado" checked="checked">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">

													<h4 class="title" id="task_details">testando a subordinado tarefa</h4>
												</div>
											</div>
										</td>
									</tr>
								<!-- Inicio de template com classe btn-default para tarefas não finalizadas -->
									<tr data-status="pendentes" id="tarefa_pendente" class="hide">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="success_taskAberto" name="" class="badgebox" value="checado">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">

													<h4 class="title" id="task_details"></h4>
												</div>
											</div>
										</td>
									</tr>
								<!-- Final de template com classe btn-default para tarefas não finalizadas -->



								<!-- Inicio de template com classe btn-success para tarefas finalizadas -->
<!--								-->
								</tbody>
							</table>
		 </div>
							                    </div>        	
							               </div>
							          </div>
							   <!-- final do notepadTopchamps-->   
      </div> 
<!-- final da aba da tarefa do subordinado-->   







<!-- inicio da aba da tarefa do subordinado--> 
     <div class="tab-pane " id="tab3">
                               <!-- inicio do notepadTopchamps-->   
							 			<div class="col-md-12" id="notepadTopchamps">
											<div class="main-content panel panel-default no-margin">
												<div class="panel-body">
							                           <header class="panel-heading clearfix">
							                               <div class="view-switcher">
							                                   <h2>TO DO</h2>
							                               </div>
							                             </header>
		<div class="table-container">

							<table class="table table-filter todoTabela" id="tarefas_tableSubordinados">
<tbody><tr data-status="pendentes" id="linha_58" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_58" name="check_58" class="badgebox" value="checado" checked="checked">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">
													<h4 class="title" id="task_details">tarefa michelle</h4>
												</div>
											</div>
										</td>
									</tr><tr data-status="pendentes" id="linha_49" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_49" name="check_49" class="badgebox" value="checado">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">

													<h4 class="title" id="task_details">teste de tarefa subordinado michelle</h4>
												</div>
											</div>
										</td>
									</tr>

								<!-- Inicio de template com classe btn-default para tarefas não finalizadas -->
									<tr data-status="pendentes" id="tarefa_pendente" class="hide">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="success_taskAberto" name="" class="badgebox" value="checado">
									               <span class="badge">✓</span>
									         </label>
										</td>
								<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">

													<h4 class="title" id="task_details"></h4>
												</div>
											</div>
										</td>
									</tr>
								<!-- Final de template com classe btn-default para tarefas não finalizadas -->



								<!-- Inicio de template com classe btn-success para tarefas finalizadas -->
<!--								-->
								</tbody>
							</table>
		 </div>
							                    </div>        	
							               </div>
							          </div>
							   <!-- final do notepadTopchamps-->   
      </div> 
<!-- final da aba da tarefa do subordinado-->   



                            </div>
                        </div>
                    </div>
                </div>               
            </div> <!-- end macbook pro mockup -->

	</div>
</div>

</div>


   <div class="modal fade" id="myModalAddTarefa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">Definir Tarefa para:</h4>
            </div>
            <div class="modal-body">
               <form id="formCadastroTarefa" name="formCadastroTarefa" method="post">
                  <table class="table text-subhead v-middle">
                     <tbody>
                     <tr>
                        <div class="text-center col-md-4 mt10">
                           <select name="id_funcionario" id="id_funcionario" class="form-control">
                           </select>
                        </div>
                        <div class="text-center col-md-8 mt10">
                           <textarea required name="tarefa" id="tarefa" class="form-control"></textarea>
                        </div>
                        <div class="text-center col-md-12 mt10">
                              <button type="submit" id="cadastrar_tarefa" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative pull-right" data-z="0.5" data-hover-z="1" data-animated>Cadastrar</button>
                        </div>
                     </tr>
                     </tbody>
                  </table>
               </form>

            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
         </div>
      </div>
   </div>

  <style type="text/css"> 
		.modal-header{background: #0f75bc !important;}
		.modal .modal-dialog{margin: 30px auto !important;}
		.modal h4{color: #ffffff !important;}
		.modal-footer{background: #0f75bc !important;}
		.mt10{margin-top: 10px;}
  </style>}
