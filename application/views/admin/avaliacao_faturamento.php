<link rel="stylesheet" href="/assets/css/admin/avaliacao.css">
  <!-- Themes -->
<link rel="stylesheet" href="/assets/js/pages/avaliacao/themes/bars-square.css">

     <div class="col-md-12 fundobranco mb20 mt20 borda">
      <h3>Meu - Vendas</h3> 

						Puxar semana atual <br>
						X de (pegar quantidade total da semana) <br>
						inicio do periodo <br>
						final do periodo <br>
						(SEMANA) vigente<br>
			<!-- Pegar os dados acima e apresentar na tela -->
				<label data-toggle="tooltip" title="Inicio do período"></label>
    
     </div>


<div class="row">
<div class="col-md-12">
	 <small>
	        *Esses valores serão validados pelo seu supervisor.
	</small>
			  <div class="box box-blue box-example-square">
			       <div class="box-header">Vendas Faturadas nessa semana</div>
			           <div class="box-body" style="height: 138px;padding-top: 0px;">
							 <table id="vendaFaturamentoSemanaAtual">
							 	<thead>
							 		<tr>
							 			<th>Dom</th>
							 			<th>Seg</th>
							 			<th>Ter</th>
							 			<th>Qua</th>
							 			<th>Qui</th>
							 			<th>Sex</th>
							 			<th>Sab</th>
							 		</tr>
							 	</thead>
							 	<tbody>
							 	<tr class="valores"> 
							 		<td width="100px">R$ 0,00</td>
							 		<td width="100px">R$ 4.567,00</td>
							 		<td width="100px">R$ 1.200,00</td>
							 		<td width="100px">R$ 2.608,00</td>
							 		<td width="100px">R$ 3.200,00</td>
							 		<td width="100px">R$ 4.600,00</td>
							 		<td width="100px">R$ 3.488,00</td>
							 	</tr>
							 	<tr>
							 		<td class="semborda"></td>							 	
							 		<td class="semborda"></td>							 	
							 		<td class="semborda"></td>
							 		<td class="semborda"></td>							 		
							 		<td class="semborda" style="text-align: right;"> Total: </td>
     						 		<td class="semborda">
							 		   <h4 style="text-align: right;">R$ </h4>
							 		</td>
							 		<td class="semborda"><h4>19.663,00</h4> </td>
							 	</tr>
							 	</tbody>
							 </table>

							 
			          </div>
	
			         </div>
			   </div>
	<div class="col-md-6">
		<div class="box-header">FATURAMENTO DO DIA ONTEM</div>	
		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Dia</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		         	    <!-- Inicio do template Colaborador  de VENDAS  -->           
			            <tr>  
					       <td>
					         <!-- local para o colaborador inserir o valor do dia de ontem--> 
								<div class="input-group input-group-lg">
								  <span class="input-group-addon" id="sizing-addon1">R$</span>
								  <input type="text" class="form-control" placeholder="0,00" aria-describedby="sizing-addon1" value="">
								</div>
					       </td>
			        	</tr>
						   <!-- final do template do Colaborador  de VENDAS  -->
						<tr>
					        <td>
					          <button class="btn btn-info" href="#"> Inserir </button>
					        </td>						   	
						</tr>
			   </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<div class="box-header">Histórico</div>
		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Valor soma da semana</th>
			           <th class="width-150 text-center">Data da aprovacao do supervisor</th>

			        </tr>
			     </thead>
			     <tbody>
			         	    <!-- Inicio do template Colaborador  de VENDAS  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Carlos Santos</span>
					            </td>    
					          <td>
					         <!-- Soma da ultima semana aprovada valor aprovado--> 
					         	R$ 27.277,00	            
					          </td>
                             <!-- data da inserçao do valor que foi aprovado-->
					          <td>
					          	<div class="avaliacaocalendariomes">AGO </div>
					          	<div class="avaliacaocalendariodia">8 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do Colaborador  de VENDAS  -->
		         	    <!-- Inicio do template Colaborador  de VENDAS  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Carlos Santos</span>
					            </td>    
					          <td>
					         <!-- Soma da ultima semana aprovada valor aprovado--> 
					         	R$ 19.663,00	            
					          </td>
                             <!-- data da inserçao do valor que foi aprovado-->
					          <td>
					          	<div class="avaliacaocalendariomes">AGO </div>
					          	<div class="avaliacaocalendariodia">1 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do Colaborador  de VENDAS  -->
		         	    <!-- Inicio do template Colaborador  de VENDAS  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Carlos Santos</span>
					            </td>    
					          <td>
					         <!-- Soma da ultima semana aprovada valor aprovado--> 
					         	R$ 19.663,00	            
					          </td>
                             <!-- data da inserçao do valor que foi aprovado-->
					          <td>
					          	<div class="avaliacaocalendariomes">JUL </div>
					          	<div class="avaliacaocalendariodia">25 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do Colaborador  de VENDAS  -->


			   </tbody>
		</table>



	</div>
</div>




