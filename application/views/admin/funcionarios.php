<script src="/assets/js/pages/groups.js"></script>
<script src="/assets/js/pages/funcionarios.js"></script>
<script src="/assets/js/jquery.dataTables.min.js"></script>

<iframe id="hiddenSubmit" width="0" height="0"></iframe>

<div class="container-fluid">
    <div class="panel-heading">
        <div class=" form-group">
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModalAddFuncionario">Cadastrar funcinário</button>
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Importar Setor de Vendas Excel (*.csv)</button>
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal3">Importar Outros Excel (*.csv)</button>
        </div>
    </div>

    <!-- Aparece apenas ao clicar em cadastrar funcionario) -->
    <div class="modal fade" id="myModalAddFuncionario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cadastrar funcionário</h4>
                </div>
                <div class="modal-body">
                    <form id="formCadastroFuncionario" name="formCadastroFuncionario" method="post">
                        <input type="hidden" name="id" id="id" value="" />

                    <table class="table text-subhead v-middle">
                        <tbody>
                        <tr>
                            <td class="text-center" style="width: 250px; float: left;">
                                <input required name="nome" id="nome" class="form-control" type="text" placeholder="Nome">
                            </td>
                              <td class="text-center"  style="width: 250px; float: left;">
                                <input required name="salario" id="salario" class="form-control" type="number" placeholder="Salário">
                            </td>
                            <td class="text-center"  style="width: 250px; float: left;">
                                <input required name="email" id="email" class="form-control" type="email" placeholder="E-mail">
                            </td>
                            <td class="text-center"  style="width: 250px; float: left;">
                                <select name="setor" id="setor"  class="form-control">
                                    <option value="" >Setor</option>
                                    <option value="Vendas" >Vendas</option>
                                    <option value="Demais Setores" >Demais Setores</option>
                                </select>
                            </td>                            
                            <td class="text-center"  style="width: 250px; float: left;">
                                <select name="supervisor" id="supervisor"  class="form-control">
                                    <option value="" >Nenhum</option>
                                </select>
                            </td>
                            <td class="text-center"  style="width: 250px; float: left;">
                                <select name="isAdmin" id="isAdmin"  class="form-control">
                                    <option value="0">NÃO</option>
                                    <option value="1">SIM</option>
                                </select>
                            </td>

                            <td class="text-center"  style="width: 250px; float: left;">
                                <select name="id_group" required id="id_group" class="form-control">
                                    <option value="">Selecione o grupo</option>
                                </select>
                            </td>

                            <td class="text-center"  style="width: 250px; float: left;">
                                <button type="submit" id="cadastar_funcionario" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Cadastrar</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Aparece apenas ao clicar em cadastrar funcionario) -->
    
    <!-- aparece como preview do imoprt via csv -->
    <div class="modal fade" id="myModalImportFuncionarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Preview da Importação</h4>
                </div>
                
				<div class="modal-body form-horizontal">
					<div class="control-group">
						<div id="container">
							<div id="form"> 
								<span>Por favor, verifique se as informações estão de acordo com o esperado.</span>
								<div id="tabela_preview"></div>
								<span>Exibindo apenas os primeiros registros.</span>
							</div>
						</div>
					</div>
				</div>

                <div class="modal-footer">
                	<button id="import_ok" type="button" class="btn btn-success">Está OK! Pode importar!</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalEditFuncionario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar funcionário</h4>
                </div>
                <div class="modal-body">
                    <form id="formEditarFuncionario" name="formEditarFuncionario" method="post">
                        <input type="hidden" name="id" id="id" value="" />

                        <table class="table text-subhead v-middle">
                            <tbody>
                            <tr>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <input required name="nome" id="nome" class="form-control" type="text" placeholder="Nome">
                                </td>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <input required name="salario" id="salario" class="form-control" type="number" placeholder="Salário">
                                </td>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <input required name="email" id="email" class="form-control" type="email" placeholder="E-mail">
                                </td>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <select name="supervisor" id="supervisor" class="form-control" >
                                        <option value="0">Nenhum</option>
                                    </select>
                                </td>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <select name="isAdmin" id="isAdmin" class="form-control" >
                                        <option value="0">NÃO</option>
                                        <option value="1">SIM</option>
                                    </select>
                                </td>

                                <td class="text-center"  style="width: 250px; float: left;">
                                    <select name="id_group" required id="id_group" class="form-control">
                                        <option value="">Selecione o grupo</option>
                                    </select>
                                </td>

                                <td class="text-center" style="width: 250px; float: left;">
                                    <button type="submit" id="editar_funcionario" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Salvar</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" id="formChangePassword" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cadastrar Senha</h4>
                    </div>
                    <div class="modal-body form-horizontal">
                        <div class="control-group">
                            <label for="new_password" class="control-label">Nova Senha</label>
                            <div class="controls">
                                <input required id="password" name="password" type="password">
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="confirm_new_password" class="control-label">Repetir Nova Senha</label>
                            <div class="controls">
                                <input required id="confirm_password" name="confirm_password" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="button" id="passwordModalSave" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" enctype='multipart/form-data' action="/admin/funcionariosAPI/importCsvVendas" id="formUploadVendas" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Importar Funcionários de Venda</h4>
                    </div>
                    <div class="modal-body form-horizontal">
                        <div class="control-group">
                            <div id="container">
                                <div id="form"> 
                                    <span>Transferir novos arquivos CSV selecionando o arquivo e clicando no botão Upload  </span>
                                      <input size='50' type='file' id="file" name='file' />
                                      <br>    <br>        
                                      <input type='submit' class="btn btn-primary" id='vendasImportSubmit' value='Importar Funcionários de Venda' />
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="modal-footer">                   
                        <button type="button" id=""  class="btn btn-success paper-shadow relative">Assista o vídeo Tutorial</button>
                        <button type="button" id=""  class="btn btn-success paper-shadow relative">Baixar Planilha de Exemplo</button>                        
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" enctype='multipart/form-data' action="/admin/funcionariosAPI/importCsvOutros" id="formUploadOutros" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Exportar Funcionários das Demais Áreas</h4>
                    </div>
                    <div class="modal-body form-horizontal">
                        <div class="control-group">
                            <div id="container">
                                <div id="form"> 
                                    <span>Transferir novos arquivos CSV selecionando o arquivo e clicando no botão Upload  </span>
                                      <input size='50' type='file' name='filename'>
                                      <br>    <br>
                                      <input type='submit' class="btn btn-primary" id='outrosImportSubmit' name='submit' value='Importar Funcionários das Demais Áreas'>
                                </div>
                             </div>
                        </div>
                    </div>
                    <div class="modal-footer">                   
                        <button type="button" id=""  class="btn btn-success paper-shadow relative">Assista o vídeo Tutorial</button>
                        <button type="button" id=""  class="btn btn-success paper-shadow relative">Baixar Planilha de Exemplo</button>                        
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-default paper-shadow" data-z="0.5">
        <div class="table-responsive">
            <table class="table text-subhead v-middle" id="funcionariosDataTable">
                <thead>
                <tr>
<!--                    <th class="width-50">ID</th>-->
                    <th class="width-150 text-center">Nome</th>
                    <th class="width-150">Setor</th>
<!--                    <th class="width-250 text-center">Cargo</th>-->
                    <th class="width-150 text-center">Salário</th>
                    <th class="width-250 text-center">E-mail</th>
                    <th class="width-150 text-center">Supervisor</th>
<!--                    <th class="width-100 text-center">Admin</th>-->
                    <th class="width-250 text-center">Ações</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

</div>