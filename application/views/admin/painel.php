
    <div class="page-section">
      <div class="row">

        <div class="mt20">

          <div class="media messages-container media-clearfix-xs-min media-grid">

            <div class="media-body">

              <div class="form-group">               
                  <!-- /input-group -->
                  <textarea type="textarea" rows="5" class="form-control share-text" placeholder="Compartilhe com seu grupo..." ></textarea>
                  <div class="input-group col-md-9" style="margin-bottom: 20px; margin-top: 10px;">               
                    <div class="input-group-btn pull-right">
                          <a  class="btn btn-success" href="#">
                             <i class="fa fa-camera"></i> Anexar Foto 
                          </a>                    
                          <a class="btn btn-primary" href="#">
                            <i class="fa fa-envelope"></i> Enviar
                          </a>                    
                    </div>                
                  </div>
                  <!-- /input-group -->



    <!--INICIO Modelo para listagem de grupo de depoimentos -->
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                  <div class="panel-body">
                      <div class="media v-middle">
                        <div class="media-left">
                          <img src="/assets/img/avatar/default_user.png" alt="person" class="media-object img-circle width-50" />
                        </div>
                        <div class="media-body message">
                          <h4 class="text-subhead margin-none"><a href="#">Usuario Teste</a></h4>
                          <p class="text-caption text-light"><i class="fa fa-clock-o"></i> 1 dia atrás</p>
                        </div>
                      </div>
                      <p>Claro.</p>
                      <p>Meu aniversário é amanhã.</p>
                  </div>
              </div>
 <!--FINAL Modelo para listagem de grupo de depoimentos -->

    <!--INICIO Modelo para listagem de grupo de depoimentos -->
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media v-middle">
                    <div class="media-left">
                      <img src="/assets/img/avatar/default_user2.png" alt="person" class="media-object img-circle width-50" />
                    </div>
                    <div class="media-body message">
                      <h4 class="text-subhead margin-none"><a href="#">Usuario Teste 2</a></h4>
                      <p class="text-caption text-light"><i class="fa fa-clock-o"></i> 2 dias atrás</p>
                    </div>
                  </div>
                  <p>
                   Oi tudo bem?
                  </p>

                </div>
              </div>
 <!--FINAL Modelo para listagem de grupo de depoimentos -->

            </div>
          </div>

          <br/>
          <br/>

        </div>
      </div>
    </div>

  </div>