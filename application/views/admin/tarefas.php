
<link rel="stylesheet" href="/assets/css/admin/tarefas.css">
<script src="/assets/js/pages/tarefas.js"></script>
  
<div class="mt20">
	<div class="col-md-12">
		<section class="">  
			<div class="pull-right">
					<button class="mb30 btn btn-default  pull-right btn-sm" data-toggle="modal" data-target="#AdicionarTarefa">
					       <i class="fa fa-plus"></i> Nova Tarefa
					</button>
			</div>
			<div class="col-md-12" id="notepadTopchamps">
				<div class="main-content panel panel-default no-margin">
					<div class="panel-body">
                                       <header class="panel-heading clearfix">
                                             <div class="view-switcher">
                                                <h2>TO DO</h2>
                                            </div>
                                        </header>
						<div class="table-container">

							<table class="table table-filter todoTabela" id="tarefas_table">
								<tbody>
								<!-- Inicio de template com classe btn-default para tarefas não finalizadas -->
									<tr data-status="pendentes" id='tarefa_pendente' class="hide">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="success_taskAberto" name='' class="badgebox" value="checado">
									               <span class="badge">&check;</span>
									         </label>
										</td>
										<!-- Caso  não seja de um supervisor vai ter a classe star apenas / seja voce /for o proprio usuario-->
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">
									               <span class="media-meta pull-right">
														 <a id='success_task' href="#">
															 <span class="glyphicon btn-glyphicon fa fa-check img-circle text-success"></span>
														 </a>									               
		                                   <!-- Botao de excluir tarefa (aparece alert perguntando se quer mesmo deletar a tarefa -->	  
														 <a id='delete_task' href="#">
															 <span class="glyphicon btn-glyphicon fa fa-trash img-circle text-danger"></span>
														 </a>
										   <!-- Pode excluir de vez do banco de dados-->

										   <!-- Pode excluir de vez do banco de dados-->	
													 </span>
													<h4 class="title" id="task_details"></h4>
												</div>
											</div>
										</td>
									</tr>
								<!-- Final de template com classe btn-default para tarefas não finalizadas -->



								<!-- Inicio de template com classe btn-success para tarefas finalizadas -->
<!--								-->
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="content-footer">
                 </div>
			</div>
		</section>
		
	</div>
</div>


    <!-- Aparece apenas ao clicar em ADICIONAR NOVA TAREFA -->
				<form id="formCadastroTarefa">
				    <div class="modal fade" id="AdicionarTarefa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					        <div class="modal-dialog" role="document">
						          <div class="modal-content">
						                <div class="modal-body">
					 <!-- Tarefa aqui é o text area que salva a tarefa o limite de caracteres são 200, coloquei limite no proprio html mesmo-->
											 <div class="vbox b-b"> 
											      <div class="paper" id="note-detail">
												      <textarea type="text" name="tarefa" id="tarefa" class="form-control" 
												        maxlength="200" placeholder="Digite sua tarefa aqui"></textarea>
											      </div>
											 </div>  
					<!-- /.final tarefa text area que salva a tarefa  -->
						                </div>	
								         <div class="modal-footer">
								          		<button type="button" class="btn btn-primary" id="cadastrar_tarefa">Salvar</button>
								                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								         </div>
					              </div>       
				          </div>
				    </div>
				</form>
    <!-- Aparece apenas ao clicar em ADICIONAR NOVA TAREFA -->


<style type="text/css">
#tarefas_table .btn-default .badge {
    color: #4CAF50;
    background-color: rgba(255, 255, 255, 0);
    font-size: 31px;
    top: -14px;
    left: -31px;
    width: 5px;
    height: 0px;
}

#tarefas_table .btn-default {
    color: #444444;
    background-color: #ffffff;
    border-color: #e7e7e7;
    height: 30px;
    width: 30px;
    border-radius: 30px;
}


/*estilo de tarefas inicio */
#notepadTopchamps .panel-body {
    padding: 0px;
}
header.panel-heading {
    background: #eee;
    background: -moz-linear-gradient(top, #fafafa, #e1e1e1);
    background: -webkit-gradient(linear, left top, left bottom, from(#fafafa), to(#e1e1e1));
    border-bottom-color: #999;
    -moz-border-radius: 5px 5px 0 0;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -khtml-border-radius: 5px 5px 0 0;
    border-radius: 5px 5px 0 0;
    -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.2);
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.2);
    -khtml-box-shadow: 0 1px 1px rgba(0,0,0,0.2);
    box-shadow: 0 1px 1px rgba(0,0,0,0.2);
    padding-top: 15px;
    padding-bottom: 15px;
}

.panel-default>.panel-heading {
    background-image: -webkit-linear-gradient(top,#f5f5f5 0,#e8e8e8 100%);
    background-image: -o-linear-gradient(top,#f5f5f5 0,#e8e8e8 100%);
    background-image: -webkit-gradient(linear,left top,left bottom,from(#f5f5f5),to(#e8e8e8));
    background-image: linear-gradient(to bottom,#f5f5f5 0,#e8e8e8 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5', endColorstr='#ffe8e8e8', GradientType=0);
    background-repeat: repeat-x;
}
.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
}
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}

.no-margin {
    margin: 0 !important;
}
.main-content {
    -moz-box-shadow: 0 0 3px rgba(0,0,0,0.5);
    -webkit-box-shadow: 0 0 3px rgba(0,0,0,0.5);
    -khtml-box-shadow: 0 0 3px rgba(0,0,0,0.5);
    box-shadow: 0 0 3px rgba(0,0,0,0.5);
    min-height: 420px;
    position: relative;
    z-index: 1;
    border: none;
}
/*estilo de tarefas final */

.mb30{margin-bottom: 30px;}



</style>