<!DOCTYPE html>
<html class="st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="painel administrativo">
    <meta name="author" content="">
    <title></title>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/vendor/core/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.maskMoney.min.js"></script>
<!-- Js da % usada no painel  -->
     <script src="<?php echo base_url(); ?>/assets/js/jquery.knob.min.js"></script>
<!-- Moment.js - Usado na pagina de minhas metas -->
     <!-- TODO(pplanel): modificar lugar no npm install -->
    <script src="<?php echo base_url(); ?>/assets/js/pages/metas/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/pages/metas/moment-range.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap/bootstrap.min.css">
<!-- Inicio do Estilo do painel usado apeans para area interna do painel -->
    <link href="<?php echo base_url(); ?>/assets/css/vendor/all.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/css/app/app.css" rel="stylesheet">
 <!-- Final do Estilo do painel usado apeans para area interna do painel -->
    <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>/assets/js/pages/page.js"></script>
</head>
<body>
<div class="st-container">
    <!-- Fixed navbar -->
    <div class="linha0topoheader navbar navbar-size-large navbar-default navbar-fixed-top"  role="navigation">
        <div class="container">
            <div class="col-md-4 navbar-header">
                <div class="">
                    <a class="svg" href="usuario-dashboard.php">
                      <img src="<?php echo base_url(); ?>/assets/img/logo.png" alt="" 
                       style="width: 179px  !important;margin-top: 6px;">
                    </a>
                </div>
            </div>
            <!-- topo direito -->
            <div class="collapse navbar-collapse" id="main-nav">
                   <div class="col-md-3" style="margin-top: 10px;">
                       <div class="input-group">
                           <div class="input-group-btn">
                             <input type="text" class="form-control" name="buscatopo" id="cargo" placeholder="Procure por nome" style="height: 30px; border-radius: 5px 0px 0px 5px;">
                               <button type='button' class="btn btn-default paper-shadow relative" data-toggle="modal" data-target="#password_modal" data-z="0.5" data-hover-z="1" data-animated style="padding: 5px 16px;border-radius: 0px 5px 5px 0px;">
                                     <i class="fa fa-search"></i>
                               </button >
                              </div> 
                        </div>
                    </div>
                    <div class="navbar-right col-md-4">
                         <ul class="nav navbar-nav navbar-nav-bordered ">
                                 <li class="dropdown notifications updates">
                                    <a href="#notificacao" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false">
                                      <span class="ink animate" style="height: 71px; width: 71px; top: 3.5px; left: -4.9375px;"></span>
                                      <i class="fa fa-comment-o" aria-hidden="true" style="font-size: 26px !important;"></i>
                                       <span class="badge badge-danger">8</span>
                                    </a>
                                 </li>
                              <li class="dropdown notifications updates">
                                  <a href="#notificacao" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false">
                                        <span class="ink animate" style="height: 71px; width: 71px; top: 3.5px; left: -4.9375px;"></span>
                                        <img src="<?php echo base_url(); ?>/assets/img/icones-novos/todo-icone.png"
                                               style="width: 21px;"> 
                                         <span class="badge badge-danger">1</span>
                                   </a>
                              </li>
                              <li class="dropdown notifications updates">
                                 <a href="#notificacao" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false">
                                  <span class="ink animate" style="height: 71px; width: 71px; top: 3.5px; left: -4.9375px;"></span>
                                  <i class="fa fa-check" aria-hidden="true"></i>
                                  <span class="badge badge-danger">8</span>
                                 </a>
                              </li>
                             <li class="dropdown notifications updates">
                                <a href="#notificacao" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false">
                                  <span class="ink animate" style="height: 71px; width: 71px; top: 3.5px; left: -4.9375px;"></span>
                                  <i class="fa fa-cog " aria-hidden="true"></i>
                                  <span class="badge badge-danger">8</span>
                                 </a>
                            </li>
                          <li>
                          </li> 
                          <li class="dropdown notifications updates">  
                              <div class="media">
                                  <a href="#notificacao" class="dropdown-toggle ripple" data-toggle="dropdown" aria-expanded="false">
                                       <!--Imagem de foto perfil de quem criou a tarefa-->
                                    <img src="<?php echo base_url(); ?>/assets/empresas/imagens/26/michelle.jpg" class="media-photo" style="border: 3px solid #aeafaf;border-radius: 100%;width: 35px;height: 35px;">
                                    <img src="<?php echo base_url(); ?>/assets/img/prata.png" style="position: fixed;margin-top: 23px;width: 15px;margin-left: 36px;border-radius: 100%;">
                                        <!--Imagem de foto perfil de quem criou a tarefa-->
                                   </a>
                              </div>            
                         </li>
                    </ul>
                </div>
            </div>
          <!-- /.navbar-collapse -->
        </div>
<!--         <div class="linha1topoheader"></div>   -->
<!--         <div class="linha2topoheader"></div>-->
    </div>

 <div class="col-md-2 col-xs-2 paddingnone sidebar-direitasombra" id="sidebar-menu" data-type="collapse">
   <div data-scrollable tabindex="0" style="">
       <ul class="sidebar-menu">

           <li class="iconeimgpainel">
               <a  href="#">
                   <!--Imagem de foto perfil-->
                   <img src="<?php echo base_url(); ?>" class="media-photo">
                   <span class="title" id="nomeFuncionario">Nome usuario logado</span>
               </a>
           </li>

           <?=$menu?>

       </ul>
    </div>
  </div>
      <!-- Inicio do conteudo da pagina  -->
          <div class="col-md-8"  id="page_content"  style="height: 600px; overflow: auto;">
                    <?=$content?>
          </div> 
      <!-- Div final do content -->


 <div class="col-md-2 paddingnone sidebar-esquerdasombra" data-type="collapse">
 <div data-scrollable tabindex="0" style="">
     <ul>
       <!-- inicio do template da pessoa -->
          <li class="imgpessoaempresadireitalateral">
            <div class="media">
                        <a href="#" class="pull-left" style="margin-right: -10px;">
                      <!--Imagem de foto perfil de quem criou a tarefa-->
                          <img src="<?php echo base_url(); ?>/assets/empresas/imagens/26/starbucks-logo.jpg" class="media-photo" 
                          style="border-radius: 100%; width: 68px;">
                  <!--Imagem de foto perfil de quem criou a tarefa-->
                        </a>
                        <div class="nomefuncionario" 
                        style="margin-top: 25px;text-align: center;font-size: 15px;">
                          <span class="title" id="nomeFuncionario">
                               <a href="">Starbucks</a>
                          </span>
                        </div>
              </div>
          </li>
  <!-- final do template da pessoa -->
       </ul>       
        <ul class="listagemequipeempresa">
        <!-- inicio do template da pessoa -->
              <li class="imgpessoaempresadireitalateral">
                <div class="media">
                            <a href="#" class="pull-left">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                              <img src="<?php echo base_url(); ?>/assets/empresas/imagens/26/michelle.jpg" class="media-photo" 
                              style="border: 3px solid #aeafaf;border-radius: 100%;width: 50px;height: 50px;">
                              <img src="<?php echo base_url(); ?>/assets/img/prata.png" style="position: absolute;margin-top: -27px;width: 22px;margin-left: 36px;border-radius: 100%;">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                            </a>
                            <div class="nomefuncionario">
                              <span class="title" id="nomeFuncionario">Michelle Pacheco</span>
                            </div>
                  </div>
              </li>
      <!-- final do template da pessoa -->
         <!-- inicio do template da pessoa -->
              <li class="imgpessoaempresadireitalateral">
                <div class="media">
                            <a href="#" class="pull-left">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                              <img src="<?php echo base_url(); ?>/assets/empresas/imagens/26/PresidenteStarbukus.jpg" class="media-photo" 
                              style="border: 3px solid #b9682b;border-radius: 100%;width: 50px;height: 50px;">
                              <img src="<?php echo base_url(); ?>/assets/img/bronze.png" style="position: absolute;margin-top: -27px;width: 22px;margin-left: 36px;border-radius: 100%;">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                            </a>

                            <div class="nomefuncionario">
                              <span class="title" id="nomeFuncionario">Howard Schultz</span>
                            </div>
                  </div>
              </li>
      <!-- final do template da pessoa --> 
         <!-- inicio do template da pessoa -->
              <li class="imgpessoaempresadireitalateral">
                <div class="media">
                            <a href="#" class="pull-left">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                              <img src="<?php echo base_url(); ?>/assets/empresas/imagens/26/joao.jpg" class="media-photo" 
                              style="border: 3px solid #f6bf2b;border-radius: 100%;width: 50px;height: 50px;">
                              <img src="<?php echo base_url(); ?>/assets/img/ouro.png" style="position: absolute;margin-top: -27px;width: 22px;margin-left: 36px;border-radius: 100%;">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                            </a>
                            <div class="nomefuncionario">
                              <span class="title" id="nomeFuncionario">João Paulo</span>
                            </div>
                  </div>
              </li>
      <!-- final do template da pessoa --> 
        <!-- inicio do template da pessoa -->
              <li class="imgpessoaempresadireitalateral">
                   <div class="media">
                            <a href="#" class="pull-left">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                              <img src="<?php echo base_url(); ?>/assets/empresas/imagens/26/vanessa.jpg" class="media-photo" 
                              style="border: 3px solid #aeafaf;border-radius: 100%;width: 50px;height: 50px;">
                              <img src="<?php echo base_url(); ?>/assets/img/prata.png" style="position: absolute;margin-top: -27px;width: 22px;margin-left: 36px;border-radius: 100%;">
                          <!--Imagem de foto perfil de quem criou a tarefa-->
                            </a>
                            <div class="nomefuncionario">
                              <span class="title" id="nomeFuncionario">Vanessa Ribeiro</span>
                            </div>
                  </div>
              </li>
        </ul>
      </div>

    </div>    
<footer class="col-md-12 footer">
   &copy; Copyright 2016
</footer>

</div> 
<!-- Final  do conteudo da página  -->


</body>
</html>