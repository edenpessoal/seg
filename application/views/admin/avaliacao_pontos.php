<link rel="stylesheet" href="/assets/css/admin/avaliacao.css">
  <!-- Themes -->
<link rel="stylesheet" href="/assets/js/pages/avaliacao/themes/bars-square.css">

     <div class="col-md-12 fundobranco mb20 mt20 borda">
            <h3>Meu</h3>
				Puxar semana atual <br>
				X de (pegar quantidade total da semana) <br>
				inicio do periodo <br>
				final do periodo <br>
				(SEMANA) vigente<br>
			<!-- Pegar os dados acima e apresentar na tela -->
		<label data-toggle="tooltip" title="Inicio do período"></label>
			
     </div>


<div class="row">

	<div class="col-md-6">
		<div class="box-header">NOTA DE DESEMPENHO</div>	
		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		         	    <!-- Inicio do template do subordinado de pontos  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/empresas/imagens/26/michelle.jpg" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Michelle Pacheco</span>
					            </td>    
					          <td>
					         <!-- Ultima nota recebida da semana anterior--> 
					            <span class="btn-glyphicon" style="background: #bcbcbc;border-radius: 100%;color: #fff;padding: 9px 16px;">
					            		2
					            </span>
	            
					          </td>
                             <!-- data da inserçao da nota recebida--> 				          
					          <td>
					          	<div class="avaliacaocalendariomes">AGO </div>
					          	<div class="avaliacaocalendariodia">15 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do subordinado de pontos  -->
			   </tbody>
		</table>
	</div>
	<div class="col-md-6">
		<div class="box-header">Histórico</div>
		<table class="table text-subhead v-middle fundobranco" 
		id="historicoavaliacaopontos">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		         	    <!-- Inicio do template do subordinado de pontos  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/empresas/imagens/26/michelle.jpg" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Michelle Pacheco</span>
					            </td>    
					          <td>
					        <!--  nota recebida --> 
					            <span class="btn-glyphicon " style="background: #bcbcbc;border-radius: 100%;color: #fff;padding: 9px 16px;">
					            		3
					            </span>	            
					          </td>
					         <!-- data da inserçao da nota recebida--> 	 
					          <td>
					          	<div class="avaliacaocalendariomes">AGO </div>
					          	<div class="avaliacaocalendariodia">8 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do subordinado de pontos  -->
		         	    <!-- Inicio do template do subordinado de pontos  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/empresas/imagens/26/michelle.jpg" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Michelle Pacheco</span>
					            </td>    
					          <td>
					        <!-- nota recebida --> 
					            <span class="btn-glyphicon" style="background: #bcbcbc;border-radius: 100%;color: #fff;padding: 9px 16px;">
					            	2
					            </span>
	            
					          </td>
					          <td>
					          	<div class="avaliacaocalendariomes">AGO </div>
					          	<div class="avaliacaocalendariodia">1 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do subordinado de pontos  -->
		         	    <!-- Inicio do template do subordinado de pontos  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/empresas/imagens/26/michelle.jpg" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Michelle Pacheco</span>
					            </td>    
					          <td>
					        <!-- nota recebida --> 
					            <span class="btn-glyphicon" style="background: #bcbcbc;border-radius: 100%;color: #fff;padding: 9px 16px;">
					            		2
					            </span>
	            
					          </td>
					         <!-- data da inserçao da nota recebida--> 	 		          
					          <td>
					          	<div class="avaliacaocalendariomes">JULH </div>
					          	<div class="avaliacaocalendariodia">25 </div>
					          </td>
			        	   </tr>
						   <!-- final do template do subordinado de pontos  -->
			   </tbody>
		</table>

	</div>
</div>












<!-- Função script da tooltip -->
		<script type="text/javascript">
			$(function () {
		        $('[data-toggle="tooltip"]').tooltip()
				})
		</script>
<!-- Função script da tooltip -->
