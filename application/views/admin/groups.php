<script src="/assets/js/pages/groups.js"></script>
<script src="/assets/js/jquery.dataTables.min.js"></script>

<iframe id="hiddenSubmit" width="0" height="0"></iframe>

<div class="container-fluid">

    <div class="panel-heading">
        <div class=" form-group">
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModalAddGrupo">+ Novo Grupo</button>
        </div>
    </div>

    <div class="modal fade" id="myModalAddGrupo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cadastrar Grupo</h4>
                </div>
                <div class="modal-body">

                    <form id="formCadastroGrupo" name="formCadastroGrupo" method="post">

                        <input type="hidden" name="idGroup" id="idGroup" value="0">

                        <table class="table text-subhead v-middle" id="tabelaNovoGrupoHeader">
                            <thead>
                            <tr>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <input required name="nome" id="nome" class="form-control" type="text" placeholder="Nome do grupo">
                                </td>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <input id="selectAll" class="form-control" type="checkbox"><label for="selectAll">Selecionar tudo</label>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table text-subhead v-middle" id="tabelaNovoGrupo">
                            <tbody>
                            </tbody>
                        </table>

                        <table class="table text-subhead v-middle">
                            <tfoot>
                            <tr>
                                <td class="text-center" style="width: 250px; float: left;">
                                    <button type="submit" id="cadastar_grupo" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Cadastrar</button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default paper-shadow" data-z="0.5">
        <div class="table-responsive">
            <table class="table text-subhead v-middle" id="groupsDataTable">
                <thead>
                <tr>
                    <th class="width-150">ID</th>
                    <th class="width-150">Nome</th>
                    <th class="width-250 text-center">Ações</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

</div>