<script src="/assets/js/pages/dashboard.js"></script>

<div class="modal fade" id="password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="formChangePassword" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Alterar Senha</h4>
                </div>
                <div class="modal-body form-horizontal">
                    <div class="control-group">
                        <label for="old_password" class="control-label">Senha Atual</label>
                        <div class="controls">
                            <input name="old_password" type="password">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="new_password" class="control-label">Nova Senha</label>
                        <div class="controls">
                            <input name="new_password" type="password">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="confirm_new_password" class="control-label">Repetir Nova Senha</label>
                        <div class="controls">
                            <input name="confirm_new_password" type="password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button type="submit" id="passwordModalSave" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="tabbable paper-shadow relative" data-z="0.5">

   <!-- Painel 1 -->
    <div class="tab-content">

        <div id="account" class="tab-pane active">

            <form class="form-horizontal" id="formPerfil">
                <div class="form-group">
                      <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="media v-middle">

                                    <div class="media-left">
                                        <div class="icon-block width-100 bg-grey-100">
                                            <img src="">
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <!--   Não precisa ter funcionalidade na fase 1    -->
                                       <input type="file" name="Adicionar Sua foto" class="btn btn-white btn-sm paper-shadow relative" data-z="0.5" />
              
                                        <!--   Não precisa ter funcionalidade na fase 1    -->
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Nome Completo</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" class="form-control"  disabled="disabled" name="nome" id="nome" placeholder="Seu nome completo" 
                                    value="" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">E-mail</label>
                    <div class="col-md-8">
                        <div class="form-control-material">
                            <div class="input-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" disabled="disabled"  value="" >
                            </div>
                        </div>
                    </div>
                </div>
  <!-- Salário puxar do banco de dados que foi inserido pelo admin -->               
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Salário</label>
                    <div class="col-md-8">
                        <div class="form-control-material">
                            <div class="input-group">
                                <input type="text" class="form-control" name="salario" id="salario" 
                                value="" disabled="disabled">
                            </div>
                        </div>
                    </div>
                </div>
  <!-- Telefone puxar e salvar no banco de dados  -->               
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Telefone</label>
                    <div class="col-md-8">
                        <div class="form-control-material">
                            <div class="input-group">
                                <input type="text" class="form-control" name="telefone" id="telefone" value="" placeholder="Telefone">
                            </div>
                        </div>
                    </div>
                </div>
  <!-- Departamento puxar e salvar no banco de dados  -->                    
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Departamento</label>
                    <div class="col-md-8">
                        <div class="form-control-material">
                            <div class="input-group">
                                <select type="text" class="form-control"  value=""  name="departamento" 
                                id="departamento" >
                                    <option value="" placeholder="Escolha seu departamento">
                                    </option>
                                    <option value="RH">RH</option> 
                                    <option value="Departamento Pessoal">Departamento Pessoal</option>                              
                                </select> 
                            </div>
                        </div>
                    </div>
                </div>
  <!-- Cargo puxar e salvar no banco de dados  -->                         
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Cargo</label>
                    <div class="col-md-8">
                        <div class="form-control-material">
                            <div class="input-group">
                                <div class="input-group">                                
                                    <input type="text" class="form-control" value="" name="cargo" id="cargo" placeholder="Cargo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  <!-- Modificar senha botão -->  
                <div class="row">
                    <div class="col-md-offset-2 col-md-1">
                       <button type='button' class="btn btn-default paper-shadow relative" data-toggle="modal" data-target="#password_modal" data-z="0.5" data-hover-z="1" data-animated>
                          <i class="fa fa-lock"></i>
                      </button>
                    </div>
          <!-- Salvar botão --> 
                    <div class=" col-md-1">
                        <button type="submit" id="perfilSubmit" data-loading-text="Salvando..." class="btn btn-default paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>
                             <i class="fa fa-save"></i>
                        </button>
                    </div>

                </div>

            </form>
        </div>

    </div>
    <!-- // final aba  -->
</div>