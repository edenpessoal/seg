
  <div class="">
    <div class="">
      <div class="mt20">
        <div class="col-md-12">
           <div class="media messages-container media-clearfix-xs-min media-grid">

            <div class="media-left">
              <div class="messages-list">

                <div class="panel panel-default paper-shadow" data-z="0.5">

                  <ul class="list-group">

                    <li class="list-group-item active">
                      <a href="#">
                        <div class="media v-middle">
                          <div class="media-left">
                            <img src="/assets/img/avatar/default-2mulher.png" width="50" alt="" class="media-object" />
                          </div>
                          <div class="media-body">
                            <span class="date">Hoje</span>
                            <span class="user">Usuario Teste </span>
                            <div class="text-light">Preciso falar com ...</div>
                          </div>
                        </div>
                      </a>
                    </li>

                    <li class="list-group-item">
                      <a href="#">
                        <div class="media v-middle">
                          <div class="media-left">
                            <img src="/assets/img/avatar/default_user2.png" height="50" alt="" class="media-object" />
                          </div>
                          <div class="media-body">
                            <span class="date">Segunda</span>
                            <span class="user">Usuario Teste 2</span>
                            <div class="text-light">Olha a minha posta...</div>

                          </div>
                        </div>
                      </a>
                    </li>

                  </ul>
                </div>
              </div>
            </div>
            <div class="media-body">

    <!--INICIO Modelo para listagem de grupo de depoimentos -->
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media v-middle">
                    <div class="media-left">
                      <img src="/assets/img/avatar/default-2mulher.png" alt="person" class="media-object img-circle width-50" />
                    </div>
                    <div class="media-body message">
                      <h4 class="text-subhead margin-none"><a href="#">Usuario Teste</a></h4>
                      <p class="text-caption text-light"><i class="fa fa-clock-o"></i> 1 dia atrás</p>
                    </div>
                  </div>
                  <p>Claro.</p>
                  <p>Meu aniversário é amanhã.</p>
                </div>
              </div>
 <!--FINAL Modelo para listagem de grupo de depoimentos -->

    <!--INICIO Modelo para listagem de grupo de depoimentos -->
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media v-middle">
                    <div class="media-left">
                      <img src="/assets/img/avatar/default-2mulher.png" alt="person" class="media-object img-circle width-50" />
                    </div>
                    <div class="media-body message">
                      <h4 class="text-subhead margin-none"><a href="#">Usuario Teste</a></h4>
                      <p class="text-caption text-light"><i class="fa fa-clock-o"></i> 1 dia atrás</p>
                    </div>
                  </div>
                  <p>Claro.</p>
                  <p>Meu aniversário é amanhã.</p>
                </div>
              </div>
 <!--FINAL Modelo para listagem de grupo de depoimentos -->


              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-btn">
                    <a class="btn btn-primary" href="#">
                      <i class="fa fa-envelope"></i> enviar
                    </a>
                  </div>
                  <!-- /btn-group -->
                  <input type="text" class="form-control share-text" placeholder="Escreva sua mensagem aqui." />
                </div>
                <!-- /input-group -->
              </div>



            </div>
          </div>

          <br/>
          <br/>

        </div>



      </div>
    </div>
 </div>       