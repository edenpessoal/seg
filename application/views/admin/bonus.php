<script src="/assets/js/pages/bonus.js"></script>

<form id="formDadosBonus" name="formDadosBonus" method="post">
    <div id="cadastro_bonus">
        <div class="container border">
            <div class="col-md-12">
                <h3>Passo 4 - Definições de Bônus</h3>
                <hr>
            </div>
            <div class="col-md-12 mt10">
                <div class="col-md-7">
                    <span>O Presidente e/ou sócio(s) da empresa participarão da divisão do bônus? </span>
                </div>
                <div class="col-md-3">
                    <span><input type="radio" name="socios_participam" value="0" checked>Sim</span>
                    <span><input type="radio" name="socios_participam" value="1" >Não</span>
                </div>
            </div>
            <div class="col-md-12 mt10 bg-info">
                <h5> Nas <label>DEMAIS ÁREAS: </label></h5>
                <div class="col-md-8">
                    <p>
                        Os funcionários melhores rankiados e que também atingiram suas metas individuais, serão bonificados com a % de:
                    </p>
                </div>
                <div class="col-md-4">
                    <input type="text" name="pct_outros" id="pct_outros" class="miniinput form-control" placeholder="" required />
                </div>
            </div>
            <div class="col-md-12 mt10 bg-info">
                <h5> Em <label> VENDAS:</label></h5>
                <div class="col-md-8">
                    <p>
                        Para cada meta adicional não cumprida será subtraido a % de:
                    </p>
                </div>
                <div class="col-md-4">
                    <input type="text" name="pct_vendas" id="pct_vendas" class="miniinput form-control" placeholder="" />
                </div>
            </div>
            <div class="col-md-12 mt25">
                <label>Defina abaixo a melhor maneira de distribuição do bônus. </label>
                <p><input type="radio" name="distribuicao" value="0">
                    OPÇÃO 1 - POR COMISSIONAMENTO (% sobre faturamento previsto).
                </p>
                <p><input type="radio" name="distribuicao" value="1">
                    OPÇÂO 2 - POR APURAÇÃO DE LUCRO (% sobre lucratividade prevista).
                </p>
            </div>
        </div>
        <div class="col-md-12 mt25">
            <button type="button" id="saveBonus" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Salvar Alterações</button>
        </div>

    </div>
</form>