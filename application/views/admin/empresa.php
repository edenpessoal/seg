<script src="/assets/js/pages/empresa.js"></script>

<!-- Inicio de todas as listagem de paineis  -->
<div class="tabbable paper-shadow relative" data-z="0.5">

    <!-- listagem de paineis  -->
    <ul class="nav nav-tabs">

        <li  class="active">
            <a href="/admin/empresa"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Empresa Dados</span></a></li>


    </ul>
    <!-- // final listagem de paineis -->

    <!-- Painel 1 -->
    <div class="tab-content">

        <div id="account" class="tab-pane active">

            <form class="form-horizontal" id="formEmpresa">


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Logo Empresa</label>
                    <div class="col-md-6">
                        <div class="media v-middle">
                            <div class="media-left">
                                <div class="icon-block width-100 bg-grey-100">
                                    <i class="fa fa-photo text-light"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <!--   Não precisa ter funcionalidade na fase 1    -->
                                <a href="#" class="btn btn-white btn-sm paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>
                                    Adicionar Logo
                                    <i class="fa fa-upl"></i></a>
                                <!--   Não precisa ter funcionalidade na fase 1    -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Nome da Empresa</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome da Empresa" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Tipo de Negócio </label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" id="tipo_negocio" name="tipo_negocio" class="form-control" placeholder="Tipo de Negócio" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">CNPJ</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" id="cnpj" name="cnpj" class="form-control" placeholder="CNPJ " required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">CEP</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" id="cep" name="cep" class="form-control" placeholder="CEP" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Endereço  </label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" id="endereco" name="endereco" class="form-control" placeholder="Endereço" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Complemento </label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <input type="text" id="complemento" name="complemento" class="form-control" placeholder="Complemento">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Cidade  </label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <select id="id_tb_estados" name="id_tb_estados" required>
                                        <option value="">Selecione uma UF</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">UF</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-control-material">
                                    <select id="id_tb_cidades" name="id_tb_cidades" required disabled>
                                        <option value="">Selecione uma cidade</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group margin-none">
                    <div class="col-md-offset-2 col-md-10">
                        <button type="submit" id="empresaSubmit" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Salvar Mudanças</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <!-- // final aba  -->

</div>
<!-- // Final de todas as abas -->