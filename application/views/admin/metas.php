 <script src="/assets/js/pages/metas.js"></script>

<script src="/assets/js/pages/grafico.js"></script>
<link rel="stylesheet" href="/assets/css/admin/metas.css">

<!-- Inicio de todas as listagem de paineis  -->
<div class="tabbable paper-shadow relative" data-z="0.5">

    <!-- listagem de paineis  -->
    <ul class="nav nav-tabs">
        <li><a href="/admin/perfil"><i class="fa fa-fw fa-lock"></i><span class="hidden-sm hidden-xs">Perfil</span></a></li>
        <li><a href="/admin/"><i class="fa fa-user"></i><span>Equipe</span></a></li> 
        <li><a href="/admin/empresa"><i class="fa fa-building"></i><span>Empresa</span></a></li>
        <li><a href="/admin/funcionarios"><i class="fa fa-users"></i><span>Funcionários</span></a></li>
        <li class="active"><a href="/admin/empresa"><i class="fa fa-fw fa-bullseye"></i><span class="hidden-sm hidden-xs">Metas</span></a></li>
        <li ><a href="/admin/empresa"><i class="fa fa-fw fa-money "></i> <span class="hidden-sm hidden-xs">Bonus</span></a></li>

    </ul>
    <!-- // final listagem de paineis -->

    <!-- Painel 1 -->
    <div class="tab-content">

        <div id="account" class="tab-pane active">

			<form id="formDadosMetas" class="form-horizontal" name="formDadosMetas" method="post">
			    <div id="cadastro_metas">
			        <div class="container border">


        <div id="periodicidade" class="row">
						        <div class="col-md-3" style="padding-right: 0;">
					            <div class="panel panel-info blocoespecial">
					                <div class="panel-heading" style="border-left: none;">
					                    <h3 class="panel-title">Peridodicidade</h3>                  
					                </div>
					                <div class="panel-body ">
					                   <span class="form-control">Início</span>  
					                </div>
					                <div class="panel-footer">
					                    <span class="form-control">Fim</span>
					                </div>
					            </div>  
					        </div>
					        <div class="col-md-3" style="padding-left: 0;">
					            <div class="panel panel-info" style="border-left: none;">
					                <div class="panel-heading">
					                    <h3 class="panel-title">&nbsp;&nbsp;</h3>                  
					                </div>
					                <div class="panel-body">
					                     <input type="text" id="meta_inicio" name="meta_inicio" class="form-control" required />
					                </div>
					                <div class="panel-footer">
					                    <input type="text" id="meta_termino" name="meta_termino" class="form-control" required />
					                </div>
					            </div>  
					        </div>
			                <div class="col-md-6 mt10">
			                    <div class="col-md-3">
			                        <label> Qnt semanas: </label>
			                    </div>
			                    <div class="col-md-4">
			                        <input id="meta_qtd_semanas" value="" readonly  class="form-control" max-value="52" />
			                    </div>
			                </div>
		                    <div class="col-md-6 mt10">
			                    <div class="col-md-3">
			                        <label> Qnt Meses: </label>
			                    </div>
			                    <div class="col-md-4">
			                        <input id="meta_quantidade_mes" value=""  class="form-control" max-value="12" />
			                    </div>
			                </div>

        </div>

        <div id="realeprevisto" class="row">

						        <div class="col-md-3" style="padding-right: 0;">
					            <div class="panel panel-info blocoespecial">
					                <div class="panel-heading" style="border-left: none;">
					                    <h3 class="panel-title">Vendas</h3>                  
					                </div>
					                <div class="panel-body ">
					                   <span class="form-control">Faturamento </span>  
					                </div>
					                 <div class="panel-footer ">
					                   <span class="form-control">% de Lucro Liq. </span>  
					                </div>
					                <div class="panel-footer">
					                    <span class="form-control">% Lucro L. a Distr. </span>
					                </div>
					                <div class="panel-footer">
					                    <span class="form-control">Bônus / PRL </span>
					                </div>					                
					            </div>  
					        </div>
					        <div class="col-md-3" style="padding-left: 0;padding-right: 0;">
					            <div class="panel panel-info" style="border-left: none;">
					                <div class="panel-heading">
					                    <h3 class="panel-title">Metas</h3>                  
					                </div>
					                <div class="panel-body ">
					                	<!-- Faturamento -->
					               <input type="text" id="meta_faturamento_valor" name="meta_faturamento_valor" required class="form-control" />
 
					                </div>
					                 <div class="panel-footer ">
					                   <!-- % de Lucro Liq. -->
					                     <input disabled type="text" id="meta_lucro_liquido"  name="meta_lucro_liquido" required class="form-control" />
					                </div>
					                <div class="panel-footer">
					                    <!-- % Lucro L. a Distr. -->
					                  <input type="text" id="meta_lucro_distribuir" name="meta_lucro_distribuir" required class="form-control" />
					                </div>
					                <div class="panel-footer"> 
					                    <!-- Bônus / PRL  -->
					                    <input disabled type="text" id="meta_bonus_prl" name="meta_bonus_prl" required class="form-control" />
					                </div>	
					            </div>  
					        </div>
		  <div class="col-md-3" style="padding-left: 0;">
				<div class="panel panel-info" style="border-left: none;">
					    <div class="panel-heading">
					         <h3 class="panel-title">Previsto</h3>                  
					   </div>
					   <div class="panel-body">
					      <!-- Faturamento -->
					     <input disabled type="text" id="previsto_faturamento_valor" name="previsto_faturamento_valor" required class="form-control" />
 					   </div>
					   <div class="panel-footer ">
					     <!-- % de Lucro Liq.  APENAS DISABLED NA SEMANA 1--> 
					    <input disabled type="text" id="previsto_lucro_liquido" name="previsto_lucro_liquido" required class="form-control" />
					  </div>
					  <div class="panel-footer">
					      <!-- % Lucro L. a Distr. -->
					 <input disabled type="text" id="previsto_lucro_distribuir" name="previsto_lucro_distribuir" required class="form-control" />
					  </div>
					  <div class="panel-footer"> 
					     <!-- Bônus / PRL  -->
					  <input disabled type="text" id="previsto_bonus_prl" name="previsto_bonus_prl" required class="form-control" />
					 </div>	
				 </div>  
		</div>

        </div>
		  <div class="col-md-12" id="container" style="display:none; width:900px; height:400px;"></div>


	 <!-- Grafico -->
		  <div class="col-md-12" id="container_grafico" style="display:none; width:900px; height:400px;"></div>
	 <!-- END Grafico -->
	            <div class="col-md-12">	
				            <div id="cadastro_metas_faturamento" class="col-md-12 mt25">
			                    <div class="col-md-12 mt10">
				                    <div class="col-md-2 hidden">
				                           <input type="text" id="meta_faturamento_qtd_semanas" name="meta_faturamento_qtd_semanas" value="" readonly class="form-control" />
				                      </div>
			                    </div>
			                    <div class="col-md-12 mt25">
			                        <label>Divisão desta meta entre vendedor(es): </label>
			                        <ul style="list-style-type: none;">
			                          <li> 
			                        	 <input type="radio" id="meta_faturamento_divisao_0" name="meta_faturamento_divisao" value="0" checked />
			                            <label for="meta_faturamento_divisao_0">Proporcionalmente 
			                            </label> <small>(Aos dias do mes)</small>
			                          </li>
			                           <li>
			                             <input type="radio" id="meta_faturamento_divisao_1" name="meta_faturamento_divisao" value="1" />
			                             <label for="meta_faturamento_divisao_1">Individualmente</label>
			                           </li>
			                        </ul> 
			                    </div>
			                    <table class="table table-striped" id="tabela_meta_funcionarios_divisao">
			                        <thead>
				                        <tr>
				                            <th>Vendedor</th>
				                            <th colspan="2">Meta de Faturamento</th>
				                        </tr>
			                        </thead>
			                        <tbody>
				                        <tr id="meta_funcionarios_divisao">
				                            <input type="hidden" name="meta_funcionario_id[]" id="meta_funcionario_id" value="" class="form-control" />
				                            <td colspan="2" id="meta_funcionario_nome"></td>
				                            <td colspan="2">
												<div class="input-group">
												    <span class="input-group-addon">R$</span>
												  	<input type="text" idfuncionario="" name="meta_funcionario_faturamento[]" id="meta_funcionario_faturamento" class="form-control" readonly />
												</div>
				                            </td>
				                           <td colspan="2">
												<div class="input-group">
												    <span class="input-group-addon">%</span>
												  	<input type="text" idfuncionario="" name="[]" id="" class="form-control" readonly />
												</div>
											</td>
				                        </tr>
			                        </tbody>

			                        <tfoot>
				                        <tr>
				                            <td colspan="2">Total</td>
				                            <td colspan="2">				                            	
				                                <div class="input-group">
												  <span class="input-group-addon">R$</span>
												  	<input type="text" idfuncionario="" name="meta_funcionario_faturamento[]"  
												  	id="meta_funcionario_totalizacao" class="form-control" readonly />
												</div>
				                            </td>
				                           <td colspan="2">
												<div class="input-group">
												    <span class="input-group-addon">%</span>
												  	<input type="text" idfuncionario="" name="[]" id="" class="form-control" readonly />
												</div>
											</td>
				                        </tr>
			                        </tfoot>
			                    </table>
			                </div>
			          </div>
			            <!--  Inicio de Meta adicional de vendas -->


			            <div class="col-md-12 mt25">
			                <button type="submit" id="saveMetas" data-loading-text="Salvando..." class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Salvar Alterações</button>
			            </div>
			            <!--   Final de Meta adicional de vendas -->
			        </div>
			    </div>
			</form>



        </div>

    </div>
    <!-- // final aba  -->

</div>
<!-- // Final de todas as abas -->



<!-- Cadastro (metas) -->



<script type='text/javascript'>

$("#meta_faturamento_valor").change(function(){
});

</script>
<!-- FIM Cadastro (metas) -->
<script src="/assets/js/pages/metas/highcharts.js"></script>
<script src="/assets/js/pages/metas/draggable-points.js"></script>
<script src="/assets/js/pages/metas/grafico.js"></script>

