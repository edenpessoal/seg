<h3>VENDEDOR</h3>




<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


<script type="text/javascript">
	$(function () {
    $('#container').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'RESULTADO VS METAS - Vendedor 1:'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 30000,
            y: 40000,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: [
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '10',
                '11',
                '34'
                
            ],
            plotBands: [{ // visualize the weekend
                from: 3,
                to: 7, 
                color: 'rgba(68, 170, 213, .2)'
            }]
        },
        yAxis: {
            title: {
                text: 'RESULTADO VS METAS - Vendedor 1:'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: 'R$/semana'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0
            }
        },
        series: [{
            name: 'Previsto',
            data: [12.500, 12500, 12500, 500, 4, 10, 12]
        }, {
            name: 'Realizado',
            data: [17647, 17647, 17647, 17647, 17647, 17647, 17647]
        }]
    });
});


</script>