  <ul class="sidebar-menu">
    <!-- inicio do template da pessoa -->
          <li class="iconeimgpainel">
              <a href="#">
           <!--Imagem de foto perfil-->
               <img src="<?php echo base_url(); ?>" class="media-photo">
                 <span class="title" id="nomeFuncionario">Nome usuario logado</span>
              </a>
          </li>
  <!-- final do template da pessoa -->
            <li class="iconeimgpainel hasSubmenu">
                 <a href="#menu-mural" data-toggle="collapse" class="collapsed" aria-expanded="false">
                   <img src="<?php echo base_url(); ?>/assets/img/icones-novos/news-icon.png" >
                   <span>Mural</span>
                 </a>
                 <ul id="menu-mural" class="collapse" aria-expanded="false"
                    style="padding-left: 18px; background: #fff; width: 100%;">
                      <li>
                        <a href="<?php echo base_url(); ?>/admin/painel">
                          <img src="<?php echo base_url(); ?>/assets/img/menu/equipe.png" >
                          <span>Comercial</span>
                        </a>
                      </li>
                  </ul>  
              </li>
              <li class="iconeimgpainel">
                <a href="<?php echo base_url(); ?>/admin/mensagens">
                   <img src="<?php echo base_url(); ?>/assets/img/icones/mensagem-nv.png">
                   <span>Mensagens</span>
                </a>
              </li>
              <li class="iconeimgpainel hasSubmenu">
                  <a href="#todo-menu" data-toggle="collapse" class="collapsed" aria-expanded="true">
                     <img src="<?php echo base_url(); ?>/assets/img/icones-novos/todo-icone-verde.png" >
                     <span>Tarefas</span>
                  </a> 
                   <ul id="todo-menu" class="collapse" aria-expanded="false" 
                       style="padding-left: 18px; background: #fff; width: 100%;">
                        <li>
                          <a href="<?php echo base_url(); ?>/admin/tarefas">
                             <img src="<?php echo base_url(); ?>/assets/img/menu/eu.png" >
                             <span>Meu</span>
                          </a>
                        </li>
                   </ul>   
              </li>
            <li class="iconeimgpainel hasSubmenu">
                <a href="#menu-resultados" data-toggle="collapse" class="collapsed" aria-expanded="false">
                      <img src="<?php echo base_url(); ?>/assets/img/icones-novos/resultado.png"> 
                      <span>Resultados</span>
                 </a>
                   <ul id="menu-resultados" class="collapse" aria-expanded="false" 
                       style="padding-left: 18px; background: #fff; width: 100%;">
                        <li>
                          <a href="<?php echo base_url(); ?>/admin/resultados_my">
                             <img src="<?php echo base_url(); ?>/assets/img/menu/eu.png" >
                             <span>Meu</span>
                          </a>
                        </li>
                        <li>
                          <a href="<?php echo base_url(); ?>/admin/resultados_empresa">
                             <img src="/assets/img/menu/empresa.png" >
                             <span>Empresa</span>
                          </a>
                        </li>
                   </ul>  
              </li>
               <li class="iconeimgpainel">
                   <a  href="#menu-recompensas" data-toggle="collapse" class="collapsed" aria-expanded="false"> 
                     <img src="<?php echo base_url(); ?>/assets/img/icones-novos/recompensas.png">
                     <span>Recompensas</span>
                   </a>
                </li>
               <li class="iconeimgpainel" style="padding-left: 20px;">
                   <a href="<?php echo base_url(); ?>/admin/recompensa_proventos"> 
                     <img src="<?php echo base_url(); ?>/assets/img/menu/proventos.png">
                     <span>Proventos</span>
                   </a>
                </li>
               <li class="iconeimgpainel" style="padding-left: 20px;">
                   <a href="<?php echo base_url(); ?>/admin/recompensa_bonus"> 
                     <img src="<?php echo base_url(); ?>/assets/img/menu/money.png">
                     <span>Bonus /PLR</span>
                   </a>
                </li>

                 <li class="iconeimgpainel" style="padding-left: 20px;">
                     <a href="<?php echo base_url(); ?>/admin/recompensa_promocao"> 
                       <img src="<?php echo base_url(); ?>/assets/img/menu/promocao.png">
                       <span>Promocão</span>
                     </a>
                  </li>
                 <li class="iconeimgpainel" style="padding-left: 20px;">
                     <a href="<?php echo base_url(); ?>/admin/recompensa_acoes"> 
                       <img src="<?php echo base_url(); ?>/assets/img/menu/acoes.png">
                       <span>Ações</span>
                     </a>
                  </li>
                 <li class="iconeimgpainel" style="padding-left: 20px;">
                     <a href="<?php echo base_url(); ?>/admin/recompensa_stockoptions"> 
                       <img src="<?php echo base_url(); ?>/assets/img/menu/estoqueoption.png">
                       <span>Stock Option</span>
                     </a>
                  </li>
                   <li class="iconeimgpainel hasSubmenu">
                          <a href="#menu-avaliacoes" data-toggle="collapse" class="collapsed" aria-expanded="false">
                              <img src="<?php echo base_url(); ?>/assets/img/icones/avaliacao.png"> 
                              <span>Avaliações</span>
                          </a>
                                <ul id="menu-avaliacoes" class="collapse" aria-expanded="false" 
                                     style="padding-left: 18px; background: #fff; width: 100%;">
                                     <li>
                                      <a href="<?php echo base_url(); ?>/admin/avaliacao_meu">
                                           <img src="<?php echo base_url(); ?>/assets/img/menu/eu.png" >
                                           <span>Meu</span>
                                        </a>
                                      </li>
                                 </ul>  
                      </li>
                       <li class="iconeimgpainel hasSubmenu">
                           <a href="#menu-config" data-toggle="collapse" class="collapsed" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>/assets/img/icones-novos/configuracao.png">
                                <span>Configuração</span>
                            </a>
                              <ul id="menu-config" class="collapse" aria-expanded="false" 
                                         style="padding-left: 18px; background: #fff; width: 100%;">
                                  <li>
                                    <a href="<?php echo base_url(); ?>/admin/">
                                       <img src="<?php echo base_url(); ?>/assets/img/menu/eu.png" >
                                       <span>Meu</span>
                                    </a>
                                  </li>
                              </ul> 
                       </li> 
                   <li class="iconeimgpainel hasSubmenu">
                        <a href="#menu-ranking" data-toggle="collapse" class="collapsed" 
                           aria-expanded="false">
                            <img src="<?php echo base_url(); ?>/assets/img/icones-novos/ranking.png" >
                            <span>Rankings</span>
                        </a>
                          <ul id="menu-ranking" class="collapse" aria-expanded="false" 
                                     style="padding-left: 18px; background: #fff; width: 100%;">
                              <li class="iconeimgpainel">
                                  <a href="<?php echo base_url(); ?>/admin/ranking_vendas">
                                     <img src="<?php echo base_url(); ?>/assets/img/menu/vendas.png" >
                                     <span>Vendas</span>
                                  </a>
                             </li>
                            <li class="iconeimgpainel">
                                <a href="<?php echo base_url(); ?>/admin/ranking_outrasareas">
                                   <img src="<?php echo base_url(); ?>/assets/img/menu/empresa.png" >
                                   <span>Outras Áreas</span>
                               </a>
                            </li>
                          </ul>  
                  </li>
                  <li class="iconeimgpainel">
                      <a href="<?php echo base_url(); ?>/admin/valuation">
                            <img src="<?php echo base_url(); ?>/assets/img/menu/valuation.png" >
                            <span>Valuation </span>
                      </a>
                   </li>   
                  <li class="iconeimgpainel hasSubmenu" style="margin-bottom: 30px;">
                      <a href="#menu-regras" data-toggle="collapse" class="collapsed" aria-expanded="false">
                      <img src="<?php echo base_url(); ?>/assets/img/icones-novos/regras.png" >
                        <span>Regras</span>
                      </a>
                      <ul id="menu-regras" class="collapse" aria-expanded="false" 
                                 style="padding-left: 18px; background: #fff; width: 100%;">
                            <li>
                              <a href="<?php echo base_url(); ?>/admin/regras_outrasareas">
                                  <img src="<?php echo base_url(); ?>/assets/img/menu/empresa.png" >
                                  <span>Outras Áreas</span>
                               </a>
                            </li>
                         <li>
                              <a href="<?php echo base_url(); ?>/admin/regras_vendas">
                              <img src="<?php echo base_url(); ?>/assets/img/menu/vendas.png" >
                                 <span>Venda</span>
                              </a>
                           </li>

                      </ul>  
                 </li>
          </ul>