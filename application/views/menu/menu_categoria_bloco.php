<li class="iconeimgpainel hasSubmenu">

    <a href="<?php echo base_url(); ?><?=$link?>" data-toggle="collapse" class="collapsed" aria-expanded="false">
        <img src="<?php echo base_url(); ?><?=$imagem?>">
        <span><?=$valor?></span>
    </a>

    <ul id="<?=$id_bloco?>" class="collapse" aria-expanded="true"
        style="padding-left: 18px; background: #fff; width: 100%;">

        <?=$bloco?>

    </ul>
</li>

<?=$scriptAbreMenuAtualPosicao?>