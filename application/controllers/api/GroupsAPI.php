<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class GroupsAPI extends My_Controller
{

    const ERROR_MESSAGES = [
        'NAME_ALREADY_EXISTS' => 'Já existe um grupo com este nome',
    ];

    function __construct()
    {
        parent::__construct();
       
    }

    public function getGroups_get()
    {

        $result = $this->getGroups()->getAllGroups();

        $output = [];
        for ($i = 0; $i < count($result); $i++) {
            foreach ($result[$i] as $key => $value) {
                $output[$i][] = $value;
            }

        }

        $this->response(array('data' => $output));

    }

    public function getSublinks_get()
    {

         $result = $this->getGroups()->getAllSublinks();

        $output = [];
        for ($i = 0; $i < count($result); $i++) {
            foreach ($result[$i] as $key => $value) {
                $output[$i][] = $value;
            }

        }

        $this->response($output);

    }

    public function addGroup_post(){

        $nome = $this->post('nome');
        $permissoes = $this->post('permissoes');
        $idGroup = $this->post('idGroup');

        try{
            if(!isset($idGroup) || $idGroup == '0') {
                $this->getGroups()->addGroup($nome, $permissoes);
            }
            else{
                $this->getGroups()->editGroup($idGroup, $nome, $permissoes);
            }
        }catch(Exception $e){

            $erro = $e->getMessage();
            $response = array("errors" => $erro);
            $this->response($response);
        }

        $this->response(array("success" => "ok"));
    }

    public function getSublinksForEdit_get(){
        $idGroup = $this->get('idGroup');

        $resultSublinks =  $this->getGroups()->getSublinksForEdit($idGroup);
        $resultName =  $this->getGroups()->getGroupNameById($idGroup);

        $output = [];
        for ($i = 0; $i < count($resultSublinks); $i++) {
            foreach ($resultSublinks[$i] as $key => $value) {
                $output[$i][] = $value;
            }

        }

        $this->response(array('name' => $resultName, 'data' => $output));
    }

    public function deleteGroup_post(){

        $idGroup = $this->post('idGroup');
        $this->getUsers();

        try {

            if($this->getUsers()->getUsersByIdGroup($idGroup)){
                throw new Exception("Este grupo contém usuários");
            }

            $this->getGroups()->deleteGroupByIdGroup($idGroup);

        }catch(Exception $e){
            $this->response(array('errors' => $e->getMessage()));
        }

        $this->response(array('success' => 'ok'));

    }

}