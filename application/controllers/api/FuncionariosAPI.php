<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class FuncionariosAPI extends My_Controller
{
    const ERROR_MESSAGES = [
        'TOO_MANY_FILES' => 'Apenas um arquivo deve ser enviado',
        'INCORRECT_FORMAT' => 'O arquivo deve vir no formato CSV',
        'INCORRECT_COLUMN_COUNT' => 'Todas as linhas devem ter 3 colunas',
        'INVALID_EMAIL' => 'E-mail inválido',
    ];

    public function getFuncionarios_get(){

        $result = $this->getFuncionario()->getFuncionariosByIdUser($this->userId);

        $output = [];
        for ($i = 0; $i < count($result); $i++) {
            foreach ($result[$i] as $key => $value) {
                $output[$i][] = $value;
            }
        }

        $this->response(array('data' => $output));

    }

    public function getFuncionariosForMetas_get(){

        $result = $this->getFuncionario()->getFuncionariosForMetasByIdUser($this->userId);

        $this->response($result);

    }

    protected function checaArquivo( $files )
    {
        $errors = [];

        if( count( $files ) > 1 )
            $errors[] = $this::ERROR_MESSAGES[ 'TOO_MANY_FILES' ];

        $file = $files['file'];

//        if( $file['type'] != "text/csv" )
//            $errors[] = $this::ERROR_MESSAGES[ 'INCORRECT_FORMAT' ];

        $arquivo = fopen ($file['tmp_name'], 'r');
        while (( $data = fgetcsv($arquivo, 0, ",") ) !== FALSE)
        {
            if( count($data) != 3 )
                $errors[] = $this::ERROR_MESSAGES[ 'INCORRECT_COLUMN_COUNT' ];

            $email = filter_var($data[2], FILTER_SANITIZE_EMAIL);
            if ( ! filter_var($email, FILTER_VALIDATE_EMAIL))
                $errors[] = $this::ERROR_MESSAGES[ 'INVALID_EMAIL' ];
        }
        fclose($arquivo);

        return $errors;

    }

    public function importCsvVendas_post()
    {
        $errors = $this->checaArquivo( $_FILES );
        if( count($errors) != 0 )
        {
            $this->response( array('errors' => $errors) );
        }

        $this->load->library('session');

        $arquivo = fopen ($_FILES['file']['tmp_name'], 'r');
        $dadosSession = [];
        while (( $linha = fgetcsv($arquivo, 0, ",") ) !== FALSE)
        {
            $data = [];
            $data['isAdmin'] = 1;
            $data['email'] = $linha[2];
            $data['password'] = rand(111111, 999999);
            $data['confirm_password'] = '';
            $data['nome'] = $linha[0];
            $data['salario'] = $linha[1];
            $data['setor'] = 'V';

            $dadosSession[] = $data;

            //$this->addFuncionario($data, true);

        }

        fclose($arquivo);

        // Salva os dados do CSV na sessao para importacao apos confirmacao do usuario
        $this->session->set_userdata(array(
        		'dados'	=> $dadosSession,
        ));

        $tablePreview = $this->load->view('admin/import_csv_funcionarios_preview', array('funcionarios' => $dadosSession), true);

        $this->response( array("preview" => $tablePreview) );
    }

    public function importCsvFinalize_get()
    {
    	$funcionarios = $this->session->userdata('dados');

    	foreach($funcionarios as $funcionario)
    	{
			$this->addFuncionario($funcionario, true);
    	}

    	$this->response( array("success" => "ok") );
    }

    public function importCsvOutros_post()
    {
        $errors = $this->checaArquivo( $_FILES );
        if( count($errors) != 0 )
        {
            $this->response( array('errors' => $errors) );
        }

        $this->load->library('session');

        $arquivo = fopen ($_FILES['file']['tmp_name'], 'r');
        $dadosSession = [];
        while (( $linha = fgetcsv($arquivo, 0, ",") ) !== FALSE)
        {
            $data = [];
            $data['isAdmin'] = 1;
            $data['email'] = $linha[2];
            $data['password'] = rand(111111, 999999);
            $data['confirm_password'] = '';
            $data['nome'] = $linha[0];
            $data['salario'] = $linha[1];
            $data['setor'] = 'O';

            $dadosSession[] = $data;

            //$this->addFuncionario($data, true);

        }

        fclose($arquivo);

        // Salva os dados do CSV na sessao para importacao apos confirmacao do usuario
        $this->session->set_userdata(array(
        		'dados'	=> $dadosSession,
        ));

        $tablePreview = $this->load->view('admin/import_csv_funcionarios_preview', array('funcionarios' => $dadosSession), true);

        $this->response( array("preview" => $tablePreview) );
    }

    protected function addFuncionario( $data, $static = false )
    {
        if($data['supervisor'] == 0 || empty($data['supervisor']))
            unset($data['supervisor']);

        $_POST['password'] = $data['password'] = rand(100000, 999999);
        $_POST['confirm_password'] = $data['confirm_password'] = $data['password'];

        if($data['isAdmin'] == 1)
        {
            try {
                $data['fk_users'] = (!$static) ? $this->getFuncionario()->createNewUser() : $this->getFuncionario()->createUserStatic( $data );
            }catch(Exception $e)
            {
                $this->response(array("errors" => $e->getMessage()));
            }
        }

        unset($data['isAdmin']);
        unset($data['password']);
        unset($data['confirm_password']);
        unset($data['id_funcionario']);

        $result = $this->getFuncionario()->addFuncionario($this->userId, $data);

        $output = [];
        for ($i = 0; $i < count($result); $i++) {
            foreach ($result[$i] as $key => $value) {
                $output[$i][] = $value;
            }
        }
        return $output;
    }

    public function addFuncionario_post()
    {
        $data = $this->post();

        $output = $this->addFuncionario($data);

        if( count($output) <= 0 )
            $this->response(array('errors' => 'Houve um erro ao inserir o usuário.'));
        else
            $this->response($output);
    }

    public function deleteFuncionario_post()
    {
        $id_funcionario = $this->post("funcionarioId");

        if($this->getFuncionario()->deleteFuncionarioById($id_funcionario, $this->userId)){
            $this->response(array("success" => true));
        }else{
            $this->response(array("error" => true));
        }
    }

    public function getFuncionarioById_get(){

        $id_funcionario = $this->get("funcionarioId");

        $result = $this->getFuncionario()->getFuncionarioByIdFuncionario($this->userId, $id_funcionario);

        $output = [];
        for ($i = 0; $i < count($result); $i++) {
            foreach ($result[$i] as $key => $value) {
                $output[$i][$key] = $value;
            }
        }
        $this->response($output);
    }

    public function editFuncionario_post(){

        $data = $this->post();
        // var_dump($data);die();
        $idFuncionario = $data['id_funcionario'];

        // antes de qualquer coisa
        $this->isThisMyFuncionario($idFuncionario);

        if($data['supervisor'] == 0 || empty($data['supervisor']))
            unset($data['supervisor']);

        $wasAdmin = $this->getFuncionario()->getUsersIdByIdFuncionario($idFuncionario);

        try {
            if ($data['isAdmin'] == 0) {
                // funcionario nao sera admin
                unset($data['isAdmin']);
                // checa se o funcionario já era admin
                if ($wasAdmin) {
                    // caso fosse admin, remove o usuario associado a ele
                    $this->getFuncionario()->deleteUsersIdByIdFuncionario($idFuncionario);
                    $data['fk_users'] = null;
                }
            } else {
                // funcionario sera admin
                // checa se ele já era admin
                if ($wasAdmin) {
                    // caso seja admin
                    // update no email e password
                    $this->getFuncionario()->changeUsersEmail($wasAdmin, $data['email']);
//                    $this->getFuncionario()->changeUsersPassword($wasAdmin, $data['password']);
                } else {
                    // caso nao seja admin
                    //cria novo usuario
                    if(isset($data['confirm_password']) && isset($data['password']))
                        $data['fk_users'] = $this->getFuncionario()->createNewUser();
                    else
                        $this->response(array("message" => "NEEDS_PASSWORD"));
                }
            }
            // edita o funcionario
            unset($data['isAdmin']);
            unset($data['password']);
            unset($data['confirm_password']);
            unset($data['id_funcionario']);

            $count = $this->getFuncionario()->updateFuncionarioById($this->userId, $idFuncionario, $data);

            if ($count === false) {
                throw new Exception('Houve um problema ao editar o usuário.');
            } else {
                $this->response(array('success' => 'ok'));
            }
        }
        catch(Exception $e)
        {
            $this->response(array('errors' => $e->getMessage()));
        }
    }

    public function changePassword_post()
    {
        $data = $this->post();
        $idFuncionario = $data['idFuncionario'];

        $this->isThisMyFuncionario($idFuncionario);

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'Confirmação de senha', 'trim|required|xss_clean|matches[password]');

        try {
            if ($this->form_validation->run()) {
                $userId = $this->getFuncionario()->getUsersIdByIdFuncionario($idFuncionario);
                $this->getFuncionario()->changeUsersPassword($userId, $data['password']);
                $this->response(array("success" => "ok"));
            } else {
                $errors = "";
                foreach ($this->form_validation->error_array() as $k => $v) {
                    $errors .= $v . "\n";
                }
                throw new Exception($errors);
            }
        }catch(Exception $e){
            $this->response(array('errors' => $e->getMessage()));
        }
    }

    private function isThisMyFuncionario($idFuncionario)
    {
        $this->getUsers();
        $user = $this->getUsers()->getUserById($this->userId);

        if($user->id_empresa != $this->getFuncionario()->getIdEmpresaByIdFuncionario($idFuncionario))
        {
            // tira esse malandrinho daqui
            $this->response('ACESSO NEGADO', 403);
        }
    }


}