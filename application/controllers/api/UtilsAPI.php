<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class UtilsAPI extends My_Controller
{

    public function getEstados_get()
    {
        $this->getEstados();
        $ufs = $this->getEstados()->getEstados();

        $this->response($ufs);
    }

    public function getCidades_get()
    {
        $this->getCidades();
        $id_uf =  $this->get('idUf');

        $cidades = $this->getCidades()->getCidadesByEstado($id_uf);
        $this->response($cidades);
    }

    public function getPercentage_get()
    {
        // Mensagem padrão para os itens faltantes
        // "Campos que ainda precisam ser preenchidos: %s"

        $perPoints['users'] = array(
            'name' => 'Nome Completo na tela de Perfil'
        );
        $perPoints['empresa'] = array(
            'nome' => 'Nome na Empresa na tela de Empresa',
            'cnpj' => 'CNPJ na tela de Empresa',
            'tipo_negocio' => 'Tipo de Negócio na tela de Empresa',
            'cep' => 'CEP na tela de Empresa',
            'endereco' => 'Endereço na tela de Empresa',
            'id_tb_cidades' => 'Cidade na tela de Empresa',
            'id_tb_estados' => 'UF na tela de Empresa'
        );
        $perPoints['funcionario'] = array(
            'id' => 'Você precisa cadastrar pelo menos um funcionário'
        );
        $perPoints['meta'] = array(
            'inicio' => 'Início na tela Minhas Metas',
            'fim' => 'Término na tela Minhas Metas'
        );
        $perPoints['meta_faturamento'] = array(
            'faturamento' => 'Meta de Faturamento na tela Minhas Metas'
        );
        $perPoints['meta_faturamento_divisao'] = array(
            'valor' => 'Você precisa definir a divisão da Meta de Faturamento por Funcionários na tela Minhas Metas'
        );
        $perPoints['bonus'] = array(
            'pct_vendas' => 'Você precisa definir o percentual de bonificação para os funcionários das demais áreas na tela Bônus',
            'pct_outros' => 'Você precisa definir o percentual de bonificação para os funcionários de vendas na tela Bônus',
            'distribuicao' => 'Você precisa definir a melhor maneira de distribuir os bônus na tela Bônus'
        );


        $totalPerPoints = sizeof($perPoints, 1);

        $nullFields = [];

        $this->load->model('UsersModel');
        $this->UsersModel->whichAreNull('users', array_keys($perPoints['users']), $nullFields);

        $this->getEmpresa()->whichAreNull('empresa', array_keys($perPoints['empresa']), $nullFields);

        $this->getFuncionario()->whichAreNull('funcionario', array_keys($perPoints['funcionario']), $nullFields);

        $this->getMetas()->whichAreNull('meta', array_keys($perPoints['meta']), $nullFields);
        $this->getMetas()->whichAreNull('meta_faturamento', array_keys($perPoints['meta_faturamento']), $nullFields);
        $this->getMetas()->whichAreNull('meta_faturamento_divisao', array_keys($perPoints['meta_faturamento_divisao']), $nullFields);

        $this->load->model('BonusModel');
        $this->BonusModel->whichAreNull('bonus', array_keys($perPoints['bonus']), $nullFields);

        $totalMissing = sizeof($nullFields, 1);
        $perc = round((($totalPerPoints - $totalMissing) / $totalPerPoints) * 100);
        $retorno['percentual'] = $perc;

        foreach($nullFields as $table => $nullField)
        {
            foreach($nullField as $k => $v) $retorno['messages'][] = $perPoints[$table][$v];
        }

        $this->response($retorno);
    }

    /**
     * Retorna os nomes dos meses
     *
     * @todo usar o para recuperar os nomes dos meses
     *
     * @return array
     */
    static function getMeses()
    {
    	return array(
    			1  => 'Janeiro',
    			2  => 'Fevereiro',
    			3  => 'Março',
    			4  => 'Abril',
    			5  => 'Maio',
    			6  => 'Junho',
    			7  => 'Julho',
    			8  => 'Agosto',
    			9  => 'Setembro',
    			10 => 'Outubro',
    			11 => 'Novembro',
    			12 => 'Dezembro'
    	);
    }
    /**
     * Retorna o nome de um mês
     *
     * @todo usar o para recuperar os nomes dos meses
     *
     * @return string
     */
    static function getMes($m)
    {
    	// Recupera os meses
    	$meses = self::getMeses();
    	// Retorna se o mes existir
    	return (isset($meses[$m])) ? $meses[$m] : null;
    }
    /**
     * Retornar qual é a semana
     *
     * @var $d Array||int
     *
     * @return string;
     */
    static public function getSemana($d = null)
    {
    	// Configura a semana
    	$nome_semana = array('domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado');
    	if (is_string($d)) $tempData = strlen($d) > 1 ? $d : (int) $d;
    	// Verifica se foi passado uma data
    	if (is_array($d) || is_string($tempData)) {
    		// Configura a data
    		if (is_array($d)) {
    			$temp = $d['ano']."-".$d['mes']."-".$d['dia'];
    		} else {
    			$temp = self::toMySQL($d);
    		}
    		// converte para a semnana
    		$w = date('w', strtotime($temp));
    		// Retorna qual é a semana
    		return $nome_semana[$w];
    		// Verifica se é um semana
    	} elseif (is_numeric($tempData)) {
    		return isset($nome_semana[$d]) ? $nome_semana[$d] : null;
    	} else {
    		// Retorna as semanas
    		return $nome_semana;
    	}
    }
}