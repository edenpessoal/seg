<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class BonusApi extends My_Controller
{

    public function upsertBonus_post(){

        $this->load->model("BonusModel");

        $formData = $this->post();

        $this->load->model('EmpresaModel');
        $empresa = $this->EmpresaModel->getEmpresaByIdUser($this->userId);

        if($empresa)
        {
            $idEmpresa = $empresa->id;

            $data = array();
            $data['id_empresa'] = $idEmpresa;
            $data['socios_participam'] = $formData['socios_participam'];
            $data['pct_vendas'] = $formData['pct_vendas'];
            $data['pct_outros'] = $formData['pct_outros'];
            $data['distribuicao'] = $formData['distribuicao'];

            $this->BonusModel->upsertBonus($data);

            $this->response(array("success" => "ok"));
        }

        $this->response(array("errors" => "Nenhum resultado encontrado."));

    }

    public function getBonus_get(){

        $this->load->model('BonusModel');
        $result = $this->BonusModel->getBonusByIdUser($this->userId);

        if($result)
            $this->response($result);
        else
            $this->response(array('errors' => 'Nenhum resultado encontrado!'));
    }

}