<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class TarefasApi extends My_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function addTarefa_post()
    {
        $data = $this->post();

        if($data['fk_funcionario'] == 0 || empty($data['fk_funcionario'])) {
            unset($data['fk_funcionario']);
        }
        else
        {
            $this->getFuncionarios();
            $fk_funcionario = $data['fk_funcionario'];
            $id_user = $this->getFuncionarios()->getUsersIdByIdFuncionario($fk_funcionario);
            if(!is_null($id_user))
                $data['fk_funcionario'] = $id_user;
        }


        $data['fk_supervisor'] = $this->userId;
        $data['data_abertura'] = date("Y-m-d H:i:s");

        $this->getTarefas()->addTarefa($data);
        $this->response(array('success' => 'ok'));
    }

    public function updateTarefa_post()
    {
        $data = $this->post();

        $this->getTarefas()->updateTarefaById($data['id'], $data);

        $this->response(array('success' => 'ok'));
    }

    public function getMeusSubordinados_get()
    {

        $subordinados = $this->getTarefas()->getSubordinadosByIdUser($this->userId);
        $this->response($subordinados);

    }
 function index() {
    $data['titulo'] = "CodeIgniter | funcionario";
 
    $this->getTarefas();
    $data['funcionario'] = $this->getTarefas()->listarSubordinados();
 
    $this->load->view('funcionario', $data);
}

    public function getTarefas_get(){

        // metodo que busca as tarefas atribuidas ao usuario
        // deve trazer todas as tarefas desde a última segunda-feira, independente de estarem abertas ou nao
        // antes da última segunda-feira, deve trazer somente as ainda em aberto
        //  echo date('Y-m-d 00:00:00'); exit;
        //echo date('Y-m-d',strtotime('last monday'));
        //exit;

        $this->getTarefas();
        $result = $this->getTarefas()->getTarefasVigentesByIdUser($this->userId);

//        $output = [];
        for ($i = 0; $i < count($result); $i++) {
            // eu sou o supervisor desta tarefa?
            if($result[$i]['supervisor'] == $this->userId)
                $result[$i]['myOwnTask'] = true;
            else
                $result[$i]['myOwnTask'] = false;

            if( is_null($result[$i]['data_fechamento']) )
                $result[$i]['status'] = 'Pendente';
            else
                $result[$i]['status'] = 'Finalizado';
        }

        $this->response($result);
    }

    public function getTarefa_get(){

        $tarefaId = $this->get('tarefaId');

        $this->getTarefas();
        $result = $this->getTarefas()->getTarefaById($tarefaId);

        $this->response($result);
    }

    public function deleteTarefa_post()
    {
        $tarefaId = $this->post('tarefaId');
        $this->getTarefas()->softDeleteTarefaById($this->userId, $tarefaId);
        $this->response(array('success' => 'ok'));
    }

    public function finalizaTarefa_post()
    {
        $tarefaId = $this->post('tarefaId');
        $this->getTarefas()->finalizaTarefaById($this->userId, $tarefaId);
        $this->response(array('success' => 'ok'));
    }

    public function getComments_get()
    {
        $tarefaId = $this->get('tarefaId');
        $comments = $this->getTarefas()->getCommentsByIdTarefa($tarefaId);
        $this->response($comments);
    }

    public function insertComment_post()
    {
        $data = $this->post();

        $data['id_user'] = $this->userId;
        $data['data'] = date("Y-m-d H:i:s");

        $this->getTarefas()->addComment($data);
        $this->response(array('success' => 'ok'));
    }
}