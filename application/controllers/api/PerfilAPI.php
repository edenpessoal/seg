<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class PerfilAPI extends My_Controller
{
    public function getPerfil_get(){

        $result = $this->getUsers()->getUserById($this->userId);

        if($result) {
            $this->response($result);
        }

        else {
            $this->response(array('errors' => 'Nenhum resultado encontrado!'));
        }

    }

    public function updatePerfil_post(){

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');

        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required|xss_clean|min_length[10]|max_length[11]');
        $this->form_validation->set_rules('departamento', 'Departamento', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cargo', 'Cargo', 'trim|required|xss_clean');

        $data['errors'] = array();

        if ($this->form_validation->run()) {

            $values['telefone'] = $this->form_validation->set_value('telefone');
            $values['departamento'] = $this->form_validation->set_value('departamento');
            $values['cargo'] = $this->form_validation->set_value('cargo');

            $result = $this->getUsers()->updateUserById($this->userId, $values);

            if ($result !== false)
            {
                $this->response(array("success" => true));

            } else {
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v) {
                    $data['errors'][$k] = $this->lang->line($v);
                }

                $this->response(array("success" => false, "errors" => $data['errors']));
            }
        }else{
            $this->response(array("success" => false, "errors" => $this->form_validation->error_array()));
        }
    }

    public function changePassword_post()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $this->form_validation->set_rules('old_password', 'Senha Atual', 'trim|required|xss_clean');
        $this->form_validation->set_rules('new_password', 'Nova Senha', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Repetir Nova Senha', 'trim|required|xss_clean|matches[new_password]');

        $data['errors'] = array();

        if ($this->form_validation->run()) {
            if ($this->tank_auth->change_password(
                $this->form_validation->set_value('old_password'),
                $this->form_validation->set_value('new_password')))
            {
                $this->response(array("success" => true));

            } else {
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);

                $this->response(array("errors" => $data['errors']));
            }
        }else{
            $this->response(array("errors" => $this->form_validation->error_array()));
        }

    }

    public function uploadFoto_post()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');

        $data = array();

        if(!empty($_FILES))
        {
            $error = false;
            $files = array();
            $uploaddir = BASEPATH . '../assets/uploads/usuarios/';

            if (!is_dir($uploaddir)) {
                $old_umask = umask(0);
                if (!mkdir($uploaddir, 0777, true)) {
                    throw new Exception('Falha ao criar pasta publica de upload');
                }
                umask($old_umask);
            }

            foreach($_FILES as $file)
            {
                $filename = $this->userId.'.jpg';
                if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($filename)))
                {
                    #TODO salvar uld da foto no banco!
                    $files = '/assets/uploads/usuarios/'.$filename;
                    $this->getUsers()->updateUserById($this->userId, array('foto_perfil' => $files));
                    $this->session->set_userdata('foto_perfil', $files);
                }
                else
                {
                    $error = true;
                }
            }

            $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
        }
        else
        {
            $data = array('success' => 'Form was submitted', 'formData' => $_POST);
        }

        echo json_encode($data);
        die();
    }
}