<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class EmpresaAPI extends My_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
	}

    public function getEmpresa_get(){

        $result = $this->getEmpresa()->getEmpresaByIdUser($this->userId);

        if($result)
            $this->response($result);
        else
            $this->response(array('errors' => 'Nenhum resultado encontrado!'));
    }

    public function updateEmpresa_post(){

        $data = $this->post();

        $result = $this->getEmpresa()->upsertEmpresaByIdUser($this->userId, $data);

        if($result !== FALSE)
            $this->response(array('success' => true));
        else
            $this->response(array('errors' => 'Nenhum resultado encontrado!'));

    }

    public function updateEmpresaRegras_post(){

    	$values = $this->post();

    	// Verifica se os valores foram preenchidos
    	$error = false;
    	$requiredFields = array('bon_dem_porc_funcionarios' => 'Porcentagem bônus funcionários',
    							'prom_supmeta_venda_semanas' => 'Promoção Vendas semanas',
    							'prom_supmeta_demais_semanas' => 'Promoção Demais Áreas semanas',
    							'stock_supmeta_demais_semanas'=> 'Stock Option Demais Áreas semanas',
						    	'stock_supmeta_vendas_semanas' => 'Stock Option Vendas semanas');

    	foreach ($requiredFields as $key=>$label) {
    		$this->form_validation->set_rules($key, $label, 'integer|trim|required|xss_clean|min_length[1]|max_length[5]');
    	}

    	$this->form_validation->set_data($values);
    	// Verifica se teve erro no envio dos dados
    	if ($this->form_validation->run()) {

    		$regra = $this->getEmpresa()->getRegras()->getRegrasByIdEmpresa($this->getUsuario(true)->fk_empresa);

    		if (empty($regra)) {
    		    $values['fk_empresa'] = $this->getUsuario(true)->fk_empresa;
    		    $result = $this->getEmpresa()->getRegras()->insert($values);
    		} else {
    		    $result = $this->getEmpresa()->getRegras()->update($values, $regra->id_regra);
    		}

    		if($result !== false) {
    			$response['success'] = true;
    		} else {
    			$response['success'] = false;
    			$response['errors'] = 'Nenhum resultado encontrado!';
    		}
    	} else {
    		$response['success'] = false;
    		$response['errors'] = $this->form_validation->error_array();
    	}

    	$this->response($response);
    }

}