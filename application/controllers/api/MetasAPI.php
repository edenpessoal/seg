<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class MetasApi extends My_Controller
{

    public function getMetas_get(){

        $this->load->model('EmpresaModel');
        $empresa = $this->EmpresaModel->getEmpresaByIdUser($this->userId);
        $idEmpresa = $empresa->id;

        $data = array();
        $this->load->model('MetasModel');
        $this->load->helper('utils');

        $meta = $this->MetasModel->getMetaByIdEmpresa($idEmpresa);
        if($meta)
        {
            $idMeta = $meta->id;

            // metas primeira parte
            $this->load->helper('utils');
            $data['meta_inicio'] = formataDataParaTela($this->issetOrEmpty($meta, 'inicio'));
            $data['meta_termino'] = formataDataParaTela($this->issetOrEmpty($meta, 'fim'));

            // meta faturamento
            $metaFaturamento = $this->MetasModel->getMetaFaturamentoByIdMeta($idMeta);
            $data['meta_faturamento_valor'] = $this->issetOrEmpty($metaFaturamento, 'faturamento');

            // meta faturamento divisao
            $radioEqual = true;
            $ultimoValor = false;
            $metaFaturamentoDivisao = $this->MetasModel->getMetasFaturamentoDivisao($metaFaturamento->id);

            if($metaFaturamentoDivisao) {
                foreach ($metaFaturamentoDivisao as $mfd) {
                    $data['meta_faturamento_divisao'][$mfd->id_funcionario] = $mfd->valor;

                    if ($radioEqual) {
                        if (!$ultimoValor) {
                            $ultimoValor = $mfd->valor;
                        } else {
                            if ($ultimoValor != $mfd->valor)
                                $radioEqual = false;
                        }

                    }
                }
            }
            $data['radioEqual'] = $radioEqual;

            // meta faturamento por produtos e servicos (produtos x servicos = PS)
            $data['meta_produtos_servicos'] = false;
            $metaFaturamentoPSItens = $this->MetasModel->getMetaFaturamentoPSItens($idEmpresa);
            if($metaFaturamentoPSItens) {
                foreach ($metaFaturamentoPSItens as $mps) {
                    $data['meta_produtos_servicos'][$mps->id] = $mps->produto_servico;
                }
            }

            // meta adesao de novos clientes
            $metaAdesaoClientes = $this->MetasModel->getMetaAdesaoClientesByIdMeta($idMeta);
            if($metaAdesaoClientes)
            {
                $data['meta_novos_clientes_value'] = $metaAdesaoClientes->quantidade;
            }

            // meta de visitas a clientes
            $metaVisitaClientes = $this->MetasModel->getMetaVisitaClientesByIdMeta($idMeta);
            if($metaVisitaClientes)
            {
                $data['meta_visitas_quantidade'] = $metaVisitaClientes->quantidade;
                $data['apenas_novos'] = $metaVisitaClientes->apenas_novos;
            }
        }

        $this->response($data);

    }

    public function getProdutosEServicos_get()
    {
        // resultado da meta so pode ser buscado pelo proprio usuario
        $this->load->model('EmpresaModel');
        $empresa = $this->EmpresaModel->getEmpresaByIdUser($this->userId);
        $idEmpresa = $empresa->id;

        $this->load->model('MetasModel');
        $this->load->helper('utils');

        $meta = $this->MetasModel->getMetaByIdEmpresa($idEmpresa);
        if($meta)
        {
            $data = array();
            $idMeta = $meta->id;
            $metasDados = $this->MetasModel->getProdutoServicoByIdMeta($idMeta);

            // vamos simplificar esse vetor
            foreach($metasDados as $value)
            {
                $data[$value->id_funcionario][$value->id_meta_produto_servico_itens] = $value->valor;
            }

            $this->response($data);
        }
        else
        {
            $this->response(array("error" => "Nenhum resultado encontrado"));
        }

    }

    public function upsertMetas_post(){

        $formData = $this->post();

        $this->load->model('EmpresaModel');
        $empresa = $this->EmpresaModel->getEmpresaByIdUser($this->userId);
        $idEmpresa = $empresa->id;

        $this->load->model('MetasModel');
        $this->load->helper('utils');

        // metas primeira parte
        $data['inicio'] = formataData($formData['meta_inicio']);
        $data['fim'] = formataData($formData['meta_termino']);
        $data['id_empresa'] = $idEmpresa;
        $idMeta = $this->MetasModel->upsertMeta($data);

        // meta faturamento
        $data = array();
        $data['faturamento'] = $formData['meta_faturamento_valor'];
        $data['id_meta'] = $idMeta;
        $idMetaFaturamento = $this->MetasModel->upsertMetaFaturamento($idMeta, $data);

        // meta faturamento funcionarios
        $data = array();
        for($i=1; $i<count($formData['meta_funcionario_id']); $i++)
        {
            $data[$i]['id_meta_faturamento'] = $idMetaFaturamento;
            $data[$i]['id_funcionario'] = $formData['meta_funcionario_id'][$i];
            $data[$i]['valor'] = $formData['meta_funcionario_faturamento'][$i];
        }

        if(count($data) > 0)
        {
            $this->MetasModel->upsertMetaFaturamentoDivisao($data);
        }


        if(isset($formData['meta_produtos']) && $formData['meta_produtos'] == "on")
        {
            $produto_servico_ids = array();

            // deleta os registros anteriores desta empresa
            $this->MetasModel->deletarMetaProdutoServicoItensByIdEmpresa($idEmpresa);
            for($i=1; $i<count($formData['produto_servico_nome']); $i++)
            {
                $data = array();
                $data['id_empresa'] = $idEmpresa;
                $data['produto_servico'] = $formData['produto_servico_nome'][$i];
                $produto_servico_ids[] = $this->MetasModel->inserirMetaProdutoServicoItens($data);
            }

            $data = array();
            $this->MetasModel->deletarMetaProdutoServicoByIdMeta($idMeta);
            for($i=1; $i<count($formData['vendedor_ps_funcionario_id']); $i++){

                $id_funcionario = $formData['vendedor_ps_funcionario_id'][$i];

                $data['id_meta'] = $idMeta;
                $data['id_funcionario'] = $id_funcionario;
//                $data['id_meta_produto_servico_itens'] = $produto_servico_ids[$i - 1];

                foreach($formData['vendedor_ps_meta'][$id_funcionario] as $chave => $valor_meta){

                    $data['id_meta_produto_servico_itens'] = $produto_servico_ids[$chave];
                    $data['valor'] = $valor_meta;

                    $this->MetasModel->inserirMetaProdutoServico($data);

                }

            }
        }else
        {
            // se nao tem produto ou desmarcou o checkbox, apago qualquer registro que esteja na tabela
            $this->MetasModel->deletarMetaProdutoServicoItensByIdEmpresa($idEmpresa);
            $this->MetasModel->deletarMetaProdutoServicoByIdMeta($idMeta);
        }


        if(isset($formData['meta_adesao']) && $formData['meta_adesao'] == "on"){
            $data = array();
            $data['id_meta'] = $idMeta;
            $data['quantidade'] = $formData['meta_novos_clientes_value'];

            $this->MetasModel->upsertMetaAdesaoClientes($data);
        }
        else
        {
            // se desmarcou, deleta o registro
            $this->MetasModel->deleteMetaAdesaoClientes($idMeta);
        }

        if(isset($formData['meta_visitas']) && $formData['meta_visitas'] == "on"){
            $data = array();
            $data['id_meta'] = $idMeta;
            $data['apenas_novos'] = $formData['meta_visitas_somente_novos'];
            $data['quantidade'] = $formData['meta_visitas_quantidade'];

            $this->MetasModel->upsertMetaVisitaClientes($data);
        }
        else
        {
            $this->MetasModel->deleteMetaVisitaClientes($idMeta);
        }

        $this->response(array("success" => "ok"));
    }
}