<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class AvaliacaoAPI extends My_Controller
{

	    function __construct()
	    {
	        parent::__construct();
	    }
	 
	    public function getPontos_get()
	    {
      	  $result = $this->getAvaliacoes()->getPontos->id_Funcionario;
          $this->response($result);
	    }

	    public function getPontosListagem_get()
	    {
      	  	$result = $this->getAvaliacoes()->getPontosListagem()->getUsuario->id_Funcionario;
        	$this->response($result);
	    }

}