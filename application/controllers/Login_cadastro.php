<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/controllers/Auth.php';

class Login_cadastro extends Auth {

	private $_isResetPassword = false;

	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
	}

	public function index_get()
	{
		$use_username = $this->config->item('use_username', 'tank_auth');

		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect('/admin/');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');

		} elseif (!$this->config->item('allow_registration', 'tank_auth')) {	// registration is off
			$this->_show_message($this->lang->line('auth_message_registration_disabled'));

		} else {

			$captcha_registration	= $this->config->item('captcha_registration', 'tank_auth');
			$use_recaptcha			= $this->config->item('use_recaptcha', 'tank_auth');


			if ($captcha_registration) {
				if ($use_recaptcha) {
					$data['recaptcha_html'] = $this->_create_recaptcha();
				} else {
					$data['captcha_html'] = $this->_create_captcha();
				}
			}
			$data['use_username'] = $use_username;
			$data['captcha_registration'] = $captcha_registration;
			$data['use_recaptcha'] = $use_recaptcha;

			$data['isResetPassword'] = $this->_isResetPassword;

			$this->load->view('login_cadastro', $data);
		}
	}

	public function register_post(){

		$use_username = $this->config->item('use_username', 'tank_auth');
		if ($use_username) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash');
		}
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|xss_clean|valid_email');
		$this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
		$this->form_validation->set_rules('confirm_password', 'Confirmação de senha', 'trim|required|xss_clean|matches[password]');

		$captcha_registration	= $this->config->item('captcha_registration', 'tank_auth');
		$use_recaptcha			= $this->config->item('use_recaptcha', 'tank_auth');

		if ($captcha_registration) {
			if ($use_recaptcha) {
				$this->form_validation->set_rules('recaptcha_response_field', 'Código de confirmação', 'trim|xss_clean|required|callback__check_recaptcha');
			} else {
				$this->form_validation->set_rules('captcha', 'Código de confirmação', 'trim|xss_clean|required|callback__check_captcha');
			}
		}

		$response['success'] = false;
		$response['errors'] = [];


		$email_activation = $this->config->item('email_activation', 'tank_auth');

		if ($this->form_validation->run()) {								// validation ok
			if (!is_null($data = $this->tank_auth->create_user(
				$use_username ? $this->form_validation->set_value('username') : '',
				$this->form_validation->set_value('email'),
				$this->form_validation->set_value('password'),
				$email_activation)))
			{
				// success
				// cria a empresa do usuario
				$this->load->model('EmpresaModel', 'empresamodel');
				$this->empresamodel->registerEmpresa($data['user_id']);

				$data['site_name'] = $this->config->item('website_name', 'tank_auth');

				if ($email_activation) {
					$data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

					$this->_send_email('activate', $data['email'], $data);

					unset($data['password']);

				} else {
					if ($this->config->item('email_account_details', 'tank_auth')) {	// send "welcome" email

						$this->_send_email('welcome', $data['email'], $data);
					}

					$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
						$this->config->item('use_username', 'tank_auth'));
					$data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');


					if ($this->tank_auth->login(
						$this->form_validation->set_value('email'),
						$this->form_validation->set_value('password'),
						$this->form_validation->set_value('remember'),
						0,
						$data['login_by_username'],
						$data['login_by_email']))
					{
						// success
						// unica saida de sucesso no registro
						unset($data['password']);
						$response['success'] = true;
						$response['errors'] = false;

					} else {
						unset($data['password']);

						$errors = $this->tank_auth->get_error_message();
						if (isset($errors['banned'])) {
							$this->_show_message($this->lang->line('auth_message_banned').' '.$errors['banned']);

						} elseif (isset($errors['not_activated'])) {
							redirect('/auth/send_again/');

						} else {
							foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
							$response['success'] = false;
							$response['errors'] = $data['errors'];
						}
					}
				}

			} else {
				$errors = $this->tank_auth->get_error_message();
				foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);

				$response['success'] = false;
				$response['errors'] = $data['errors'];
			}
		}else{

			$response['success'] = false;
			$response['errors'] = $this->form_validation->error_array();
		}

		$this->response($response);
	}

	public function forgot_password_post()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect('');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');

		} else {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->forgot_password(
					$this->form_validation->set_value('email')))) {

					$data['site_name'] = $this->config->item('website_name', 'tank_auth');

					// Send email with password activation link
					$this->_send_email('forgot_password', $data['email'], $data);

					$this->response(array("success" => "ok"));

				} else {
					$error = "";
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$error .= $this->lang->line($v)."\n";
				}
			}
			else
			{
				$error = "";
				$errors = $this->form_validation->error_array();
				foreach ($errors as $k => $v)	$error .= $this->lang->line($v)."\n";
			}

			$this->response(array("errors" => $errors));
		}
	}

	public function reset_password_get()
	{
		$user_id		= $this->uri->segment(3);
		$new_pass_key	= $this->uri->segment(4);

		if ($this->tank_auth->can_reset_password($user_id, $new_pass_key)) {
			
			$this->_isResetPassword = true;
		}

		$this->index_get();
	} 

	public function reset_password_post()
	{
		$user_id		= $this->uri->segment(3);
		$new_pass_key	= $this->uri->segment(4);

		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

		$data['errors'] = array();

		if ($this->form_validation->run()) {								// validation ok
			if (!is_null($data = $this->tank_auth->reset_password(
				$user_id, $new_pass_key,
				$this->form_validation->set_value('new_password')))) {	// success

				$data['site_name'] = $this->config->item('website_name', 'tank_auth');

				// Send email with new password
				$this->_send_email('reset_password', $data['email'], $data);

				$data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
					$this->config->item('use_username', 'tank_auth'));
				$data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');

				$this->tank_auth->login($data['email'], $this->form_validation->set_value('new_password'), 0, 0, $data['login_by_username'], $data['login_by_email']);

				$this->response(array("success" => "ok"));

			} else {
				$this->response(array("errors" => $this->lang->line('auth_message_new_password_failed')));
			}
		}
		else
		{
			$error = "";
			$errors = $this->form_validation->error_array();
			foreach ($errors as $k => $v)	$error .= $this->lang->line($v)."\n";
			$this->response(array("errors" => $errors));
		}
	}

}
