<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/My_Controller.php';

class Admin extends My_Controller
{

    public function index_get()
    {
        $this->perfil_get();
    }

    public function perfil_get(){
        $content = $this->load->view('admin/dashboard',
            '',
            true
        );

        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function groups_get(){
        $content = $this->load->view('admin/groups',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function equipe_get(){
        $content = $this->load->view('admin/equipe',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function empresa_get(){
        $content = $this->load->view('admin/empresa',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));

    }

    public function funcionarios_get(){
        $content = $this->load->view('admin/funcionarios',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function metas_get(){
        $content = $this->load->view('admin/metas',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function bonus_get(){
        $content = $this->load->view('admin/bonus',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function tarefas_get(){
        $content = $this->load->view('admin/tarefas',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function tarefas_equipe_get(){
        $content = $this->load->view('admin/tarefas_equipe',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function painel_get(){
        $content = $this->load->view('admin/painel',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function painel_empresa_get(){
        $content = $this->load->view('admin/painel_empresa',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function mensagens_get(){
        $content = $this->load->view('admin/mensagens',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function resultados_my_get(){
        $content = $this->load->view('admin/resultados_my',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function resultados_equipe_get(){
        $content = $this->load->view('admin/resultados_equipe',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function resultados_empresa_get(){
        $content = $this->load->view('admin/resultados_empresa',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function avaliacao_get(){

         $content = $this->load->view('admin/avaliacao',
            '',
            true
        );
        $this->load->view('admin/master_page',
            array(
                'content' => $content,
                'menu' => $this->userMenu,
                'usuario' => $this->getUsuario(true)
                )
            );
    }

//// // //  Colocar dentro de condição as view de avaliacao por setor // // // // // 
    public function avaliacao_pontos_get(){
        $content = $this->load->view('admin/avaliacao_pontos',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));

    }
    public function avaliacao_faturamento_get(){
        $content = $this->load->view('admin/avaliacao_faturamento',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }
// // // // // // // // // // // // // // // // // // 

    public function avaliacao_equipe_get(){
        $content = $this->load->view('admin/avaliacao_equipe',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function avaliacao_empresa_get(){
        $content = $this->load->view('admin/avaliacao_empresa',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }


    public function regras_outrasareas_get(){
        $content = $this->load->view('admin/regras_outrasareas',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }


    public function regras_vendas_get(){
        $content = $this->load->view('admin/regras_vendas',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function recompensa_proventos_get(){
        $content = $this->load->view('admin/recompensa_proventos',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function recompensa_bonus_get(){
        $content = $this->load->view('admin/recompensa_bonus',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function recompensa_promocao_get(){
        $content = $this->load->view('admin/recompensa_promocao',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function recompensa_acoes_get(){
        $content = $this->load->view('admin/recompensa_acoes',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function recompensa_stockoptions_get(){
        $content = $this->load->view('admin/recompensa_stockoptions',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function ranking_vendas_get(){
        $content = $this->load->view('admin/ranking_vendas',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function ranking_outrasareas_get(){
        $content = $this->load->view('admin/ranking_outrasareas',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    }

    public function valuation_get(){
        $content = $this->load->view('admin/valuation',
            '',
            true
        );
        $this->load->view('admin/master_page', array('content' => $content, 'menu' => $this->userMenu));
    } 


}