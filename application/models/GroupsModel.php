<?php defined('BASEPATH') OR exit('No direct script access allowed');

class GroupsModel extends MY_Model {

    private $ci;

    public $table = 'group'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->timestamps_format = 'Y-m-d H:i:s';
        $this->return_as = 'object';

        parent::__construct();
    }

    public function getAllGroups(){

        $this->db->select ( '*' );
        $this->db->from ( 'group' );

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;

    }

    public function getGroupNameById($idGroup){

        $query = $this->db->select('name')
            ->where('id', $idGroup)
            ->get('group');

        $ret = $query->row();
        return $ret->name;
    }

    public function getAllSublinks(){

        $query = $this->db->query('SELECT l.valor, s.link, s.id 
                          FROM link AS l 
                          LEFT JOIN sublink AS s ON (s.sublinkDe=l.id) 
                          WHERE s.sublinkDe IS NOT NULL
                          AND s.sublinkDe != 0
                          ORDER BY l.ordem ASC ');

        $results = $query->result_array();

        return $results;

    }

    public function addGroup($nome, $permissoes)
    {

        if($this->getGroupByName($nome)){
            throw new Exception( GroupsAPI::ERROR_MESSAGES['NAME_ALREADY_EXISTS'] );
        }

        $data['name'] = $nome;
        $this->db->insert('group', $data);
        unset($data['name']);

        $data['id_group'] = $this->db->insert_id();

        foreach($permissoes as $permissao){
            $data['id_sublink'] = $permissao;
            $this->db->insert('group_sublinks', $data);
        }
    }

    public function editGroup($idGroup, $nome, $permissoes)
    {

        $data = array(
            'name' => $nome
        );

        $this->db->where('id', $idGroup);
        $this->db->update('group', $data);

        unset($data);

        $this->db->delete('group_sublinks', array('id_group' => $idGroup));

        $data['id_group'] = $idGroup;
        foreach($permissoes as $permissao){
            $data['id_sublink'] = $permissao;
            $this->db->insert('group_sublinks', $data);
        }

        return true;
    }

    public function getGroupByName($nome){
        $query = $this->db->where('name', $nome)
            ->get('group');

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    public function getSublinksByIdGroup($idGroup){
        $query = $this->db->select('id_sublink')
            ->where('id_group', $idGroup)
            ->get('group_sublinks');

        $retorno = [];
        foreach($query->result_array() as $result){
            $retorno[] = $result['id_sublink'];
        }

        return $retorno;
    }

    public function getSublinksForEdit($idGroup){

        $allSublinks = $this->getAllSublinks();
        $sublinksForGroup = $this->getSublinksByIdGroup($idGroup);

        $retorno = [];

        foreach($allSublinks as &$sublink){
            $sublink['checked'] = in_array($sublink['id'], $sublinksForGroup);
        }

        return $allSublinks;

    }

    public function deleteGroupByIdGroup($idGroup){

        $this->db->delete('group_sublinks', array('id_group' => $idGroup));
        $this->db->delete('group', array('id' => $idGroup));

    }
}