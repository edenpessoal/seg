<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BonusModel extends MY_Model {

    public $table = 'bonus'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->return_as = 'object';
        parent::__construct();
    }

    public function upsertBonus($data)
    {
        $idEmpresa = $data['id_empresa'];

        // checa se há algum bonus ja cadastrada para este usuario
        $result = $this->getBonusByIdEmpresa($idEmpresa);
        if(!$result){
            $this->insertBonus($data);
        }else{
            $this->updateBonusByIdEmpresa($idEmpresa, $data);
        }
    }

    function insertBonus($data){
        $this->db->insert('bonus', $data);
    }

    public function updateBonusByIdEmpresa($idEmpresa, $data)
    {
        $this->db->where('id_empresa',$idEmpresa);
        $this->db->update('bonus',$data);

        return $this->db->affected_rows();
    }

    public function getBonusByIdUser($idUser)
    {
        $this->load->model('EmpresaModel');
        $empresa = $this->EmpresaModel->getEmpresaByIdUser($idUser);
        $idEmpresa = $empresa->id;

        return $this->getBonusByIdEmpresa($idEmpresa);
    }

    public function getBonusByIdEmpresa($idEmpresa)
    {
        $query = $this->db->select("socios_participam, pct_vendas, pct_outros, distribuicao")
            ->where('id_empresa', $idEmpresa)
            ->get('bonus');

        $result = $query->result();

        if($query->num_rows() > 0) {
            return $result[0];
        }
        else {
            return false;
        }
    }

}