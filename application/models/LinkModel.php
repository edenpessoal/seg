<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LinkModel extends MY_Model {

    public $table = 'link';
    public $primary_key = 'id';

    public function __construct()
    {
        $this->return_as = 'object';
        parent::__construct();
    }

    public function getMenuLinks(){

        $query = $this->db->query("SELECT id, valor, id_bloco, imagem FROM link ORDER BY ordem ASC");
        return $query->result_array();

    }

    public function getMenuSublinkByIdLink($idLink){

        $query = $this->db->query("SELECT id, url, link, imagem FROM sublink WHERE isApi = 0 AND sublinkDe = " . $idLink);
        return $query->result_array();

    }

    public function getMenuSublinkByIdLinkAndIdUser($idLink, $idUser){

        $this->load->model('UsersModel');
        $idGroup = $this->UsersModel->getGroupIdByIdUser($idUser);

        $query = $this->db->query("SELECT s.id, s.url, s.link, s.imagem FROM sublink AS s WHERE s.isApi = 0 AND s.sublinkDe = {$idLink}
         AND EXISTS (
            SELECT 1 FROM group_sublinks AS gs WHERE gs.id_group = {$idGroup} AND gs.id_sublink = s.id
         )
        ");
        
        return $query->result_array();

    }

}