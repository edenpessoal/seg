<?php

namespace Application\Models;

use MY_Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Model {

    public $table = 'users'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->return_as = 'object';
        parent::__construct();
    }

    public function getUserById($idUser){

        $query = $this->db->where('id', $idUser)
            ->get('users');

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    public function updateUserById($idUser, $data){

        $this->db->where('id', $idUser)
            ->update('users', $data);

        return $this->db->affected_rows();
    }

    public function getUsersByIdGroup($idGroup){

        $query = $this->db
            ->where('id_group', $idGroup)
            ->get('users');

        if($query->num_rows() > 0) {
            return $query->result();
        }
        else{
            return false;
        }

    }

    public function getUserByFuncionarioId($idUser)
    {
            $sql = "   SELECT
                        funcionario.fk_users,
                        funcionario.fk_empresa,
                        funcionario.nome,
                        funcionario.email,
                        funcionario.salario,
                        funcionario.setor,
                        funcionario.supervisor,
                        users.cargo,
                        users.departamento,
                        users.telefone,
                        users.foto_perfil,
                        users.foto_capa
                        from funcionario
                        LEFT OUTER JOIN  users ON
                        funcionario.fk_users = users.id
                        where users.id = ?
                        ORDER BY funcionario.nome ASC";

        $query = $this->db->query($sql, array($idUser));
        $results = $query->result_array();

        if (count($results) >= 1) {
        	$results = $results[0];
        }

        return $results;
    }

    public function getFuncionarioEmpresaByFuncionarioIdAndUserId($idUser)
    {
            $sql = "   SELECT
                        funcionario.fk_users,
                        funcionario.fk_empresa,
                        funcionario.nome,
                        funcionario.email,
                        funcionario.salario,
                        funcionario.setor,
                        funcionario.supervisor,
                        users.cargo,
                        users.departamento,
                        users.telefone,
                        users.foto_perfil,
                        users.foto_capa
                        from funcionario A
                        LEFT OUTER JOIN  users B ON
                        funcionario.fk_users = users.id
                        ORDER BY funcionario.nome ASC";
        $query = $this->db->query($sql, array($idUser));
        $results = $query->result_array();
        return $results;
    }



}