<?php
/**
 * Model para gerenciar os status dos usuários
 *
 * @copyright Copyright (c) 2016 TopChamps (http://topchamps.com.br)
 */
namespace Application\Models\Funcionario;

use Application\Models\StatusAbstract;

class Setor extends StatusAbstract
{
    CONST VENDAS  = 'V';
    CONST OUTROS  = 'O';

    static protected $status = array(
        self::VENDAS   => array('Vendas',  'Vendas'),
        self::OUTROS   => array('Outros',  'Outros'),
    );
}