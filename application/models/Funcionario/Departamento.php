<?php
/**
 * Model para listar os departamentos dos colaboradores
 *
 * @copyright Copyright (c) 2016 TopChamps (http://topchamps.com.br)
 */
namespace Application\Models;

use MY_Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends MY_Model
{

    private $ci;

    public $table = 'departamento'; // Set the name of the table for this model.
    public $primary_key = 'id_departamento'; // Set the primary key


    public function getdepartamentoByEmpresaId($idUser){

        $this->db->select ( 'departamento.id_departamento, departamento.nome' );
        $this->db->from ( 'departamento' );
        $this->db->join ( 'empresa', 'departamento.fk_empresa = empresa.id_empresa');
        $this->db->join ( 'users', 'users.id_empresa = empresa.id_empresa');
        $this->db->where( 'users.id', $idUser );

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;

    }

    public function getdepartamento($idUser){

        $this->db->select ( 'departamento.id_departamento, departamento.nome' );
        $this->db->from ( 'departamento' );

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;

    }

}