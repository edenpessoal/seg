<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MetasModel extends MY_Model {

    private $userId = null;

    public function upsertMeta($data){

        $this->load->library('tank_auth');
        $this->userId = $this->tank_auth->get_user_id();

        // checa se há alguma empresa ja cadastrada para este usuario
        $this->load->model('EmpresaModel');
        $empresaId = $this->EmpresaModel->getIdEmpresaByIdUser($this->userId);

        $metaData = $this->getMetaByIdEmpresa($empresaId);

        $ret = false;
        if(!$metaData){
            $ret = $this->inserirMeta($data);
        }else{
            $ret = $this->updateMeta($metaData->id, $data);
        }

        return $ret;

    }

    function getMetaByIdEmpresa($idEmpresa)
    {
        $query = $this->db->where('id_empresa', $idEmpresa)
            ->get('meta');

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    private function inserirMeta($data){
        $this->db->insert('meta', $data);
        return $this->db->insert_id();
    }

    public function updateMeta($idMeta, $data)
    {
        $this->db->where('id',$idMeta);
        $this->db->update('meta',$data);

        return $idMeta;
    }

    public function upsertMetaFaturamento($idMeta, $data)
    {
        $metaFaturamento = $this->getMetaFaturamentoByIdMeta($idMeta);
        if($metaFaturamento)
        {
            $this->updateMetaFaturamento($metaFaturamento->id, $data);
            return $metaFaturamento->id;
        }
        else
        {
            return $this->inserirMetaFaturamento($data);
        }
    }

    public function getMetaFaturamentoByIdMeta($idMeta){
        $query = $this->db->where('id_meta', $idMeta)
            ->get('meta_faturamento');

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }

    }

    private function updateMetaFaturamento($idMetaFaturamento, $data)
    {
        $this->db->where('id',$idMetaFaturamento);
        $this->db->update('meta_faturamento',$data);

        return $idMetaFaturamento;
    }

    private function inserirMetaFaturamento($data){
        $this->db->insert('meta_faturamento', $data);
        return $this->db->insert_id();
    }


    public function upsertMetaFaturamentoDivisao($data){

        foreach($data as $dataMetaFaturamento)
        {
            if($this->getMetaFaturamentoDivisao($dataMetaFaturamento['id_meta_faturamento'], $dataMetaFaturamento['id_funcionario']))
            {
                $this->updateMetaFaturamentoDivisao($dataMetaFaturamento);
            }
            else
            {
                $this->inserirMetaFaturamentoDivisao($dataMetaFaturamento);
            }
        }
    }

    private function updateMetaFaturamentoDivisao($data)
    {
        $this->db->where('id_meta_faturamento', $data['id_meta_faturamento']);
        $this->db->where('id_funcionario', $data['id_funcionario']);
        $this->db->update('meta_faturamento_divisao', array('valor' => $data['valor']));
    }


    public function getMetaFaturamentoDivisao($idMetaFaturamento, $idFuncionario)
    {
        $this->db->where('id_meta_faturamento', $idMetaFaturamento);
        $this->db->where('id_funcionario', $idFuncionario);
        $query = $this->db->get('meta_faturamento_divisao');

        if($query->num_rows() > 0) {
            return true;
        }
        else {
            return false;
        }

    }

    public function getMetasFaturamentoDivisao($idMetaFaturamento)
    {
        $this->db->where('id_meta_faturamento', $idMetaFaturamento);
        $query = $this->db->get('meta_faturamento_divisao');

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else {
            return false;
        }

    }

    function inserirMetaFaturamentoDivisao($data)
    {
        $this->db->insert('meta_faturamento_divisao', $data);
    }

    function getMetaFaturamentoPSItens($idEmpresa)
    {
        $this->db->where('id_empresa', $idEmpresa);
        $query = $this->db->get('meta_produto_servico_itens');

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else {
            return false;
        }
    }

    public function getProdutoServicoByIdMeta($idMeta)
    {
        $this->db->select('id_funcionario, id_meta_produto_servico_itens, valor');
        $this->db->where('id_meta', $idMeta);
        $query = $this->db->get('meta_produto_servico');

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else {
            return false;
        }
    }

    public function deletarMetaProdutoServicoItensByIdEmpresa($idEmpresa)
    {
        $this->db->delete('meta_produto_servico_itens', array('id_empresa' => $idEmpresa));
    }

    public function deletarMetaProdutoServicoByIdMeta($idMeta)
    {
        $this->db->delete('meta_produto_servico', array('id_meta' => $idMeta));
    }

    function inserirMetaProdutoServicoItens($data){
        $this->db->insert('meta_produto_servico_itens', $data);
        return $this->db->insert_id();
    }

    function inserirMetaProdutoServico($data){
        $this->db->insert('meta_produto_servico', $data);
    }

    function upsertMetaAdesaoClientes($data)
    {
        $metaAdesaoClientes = $this->getMetaAdesaoClientesByIdMeta($data['id_meta']);
        if($metaAdesaoClientes)
        {
            $this->updateMetaAdesaoCliente($metaAdesaoClientes->id, $data);
        }
        else
        {
            $this->inserirMetaAdesaoClientes($data);
        }
    }

    public function updateMetaAdesaoCliente($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('meta_adesao_clientes', $data);
    }

    public function deleteMetaAdesaoClientes($idMeta)
    {
        $this->db->delete('meta_adesao_clientes', array('id_meta' => $idMeta));
    }

    public function getMetaAdesaoClientesByIdMeta($idMeta)
    {
        $this->db->where('id_meta', $idMeta);
        $query = $this->db->get('meta_adesao_clientes');

        if($query->num_rows() > 0)
        {
            return $query->result()[0];
        }
        else {
            return false;
        }
    }

    function inserirMetaAdesaoClientes($data){
        $this->db->insert('meta_adesao_clientes', $data);
    }

    public function upsertMetaVisitaClientes($data)
    {
        $idMeta = $data['id_meta'];

        $metaVisitaClientes = $this->getMetaVisitaClientesByIdMeta($idMeta);
        if($metaVisitaClientes)
        {
            $this->updateMetaVisitaClientes($metaVisitaClientes->id, $data);
        }
        else
        {
            $this->inserirMetaVisitaClientes($data);
        }

    }

    public function getMetaVisitaClientesByIdMeta($idMeta)
    {
        $this->db->where('id_meta', $idMeta);
        $query = $this->db->get('meta_visita_clientes');

        if($query->num_rows() > 0)
        {
            return $query->result()[0];
        }
        else {
            return false;
        }
    }

    public function updateMetaVisitaClientes($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('meta_visita_clientes', $data);
    }

    function inserirMetaVisitaClientes($data){
        $this->db->insert('meta_visita_clientes', $data);
    }

    public function deleteMetaVisitaClientes($idMeta)
    {
        $this->db->delete('meta_visita_clientes', array('id_meta' => $idMeta));
    }
}