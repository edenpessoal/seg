<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TbEstadosModel extends MY_Model {

    public $table = 'tb_estados'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->return_as = 'object';

        parent::__construct();
    }

    public function getEstados(){

        $query = $this->db->get('tb_estados');
        return $query->result();

    }

}