  <!-- CSS da página  -->
<link rel="stylesheet" href="/assets/css/admin/avaliacao.css">
<link rel="stylesheet" href="/assets/css/admin/tarefas_equipe.css">
 <!-- theme do avaliacao -template de modelo para "votar" -->
<link rel="stylesheet" href="/assets/js/pages/avaliacao/themes/bars-square.css">
  
    <div class="col-md-12 fundobranco mb20 mt20 borda">
		<h3>Equipe</h3>
        <div class="col-md-12 col-sm-12 col-xs-12">			
          Atribua as notas de desempenho de sua equipe usando como parâmetros:
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 mt10">
			<i class="fa fa-check text-success"></i>
			A importância das atividades concluídas ao longo da semana apresentadas no TO DO.
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 mt10">
			<i class="fa fa-check text-success"></i>
			A correlação das atividades concluídas com os macro-objetivos de cada funcionário.
		</div>
		<br><br><br>
    </div>


<div class="row">
	<div class="col-md-6">
		<div class="box-header">Avaliações Pendentes</div>

<!--// TODO : Colocar esses ALERTAS -->

			<!-- ALerta para caso já tenha avaliado todos os subordinados nessa semana -->
<!-- 					<div class="alert alert-success" role="alert">
					    <strong>Aviso:</strong>
					    Você não tem nenhum colaborador para avaliar! 
					</div>	 -->


				<!-- ALerta para caso tenha passado de 00 horas de segunda-feira , pois o sistema atribui nota 2 caso o superior não atribua uma nota para o usuario ou  -->
<!-- 					<div class="alert alert-warning" role="alert">
					    <strong>Aviso:</strong>
					    O sistema atribuiu a nota 2 para os seus subordinados e ou aprovou as informações inseridas por eles.
					</div> -->
<!--Template GERAL para avaliacao
só disponível toda segunda-feira até as 00horas-->


		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		         	    <!-- Inicio do template do subordinado de pontos -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Daniela Merculles</span>
					            </td>    
					          <td>
					        <!-- Lista do TODO do subordinado em questao--> 
					            <span class="btn-glyphicon clicavel" data-toggle="modal" data-target="#todo_modal" style="background: #0f75bc; border-radius: 100%;">
			                      <img src="/assets/img/icones-novos/todo-icone.png"  style="width: 21px;">
					            </span>
					        <!-- Abre modal para avaliar o subordinado que NÃO é de vendas--> 
					               <span class="glyphicon btn-glyphicon clicavel fa fa-check img-circle text-success" data-toggle="modal" data-target="#nota_modal" style="font-size: 21px;background: #ededed;"></span>		            
					          </td>
			        	   </tr>
						   <!-- final do template do subordinado de pontos  -->
						   <!-- Inicio do template do subordinado de VENDAS  -->           
			               <tr>
			            	<!--Imagem de foto perfil do subordinado-->	
				            	<td>
									<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
				            	</td> 
				            <!-- Nome do subordinado em questao-->  
				            	<td> 
				            	    <span>Carlos Santos </span>
				            	</td>         		
				            	<td>
				            		<a href="#valor_modal" data-toggle="modal" data-target="#valor_modal">
				            		<span class="fa fa-usd btn-glyphicon img-circle"  style="font-size: 21px;background: #0f75bc;padding: 8px 14px;color: #fff;"></span>
				            		</a>
				            	</td>
			               </tr>
				          <!-- final do template do subordinado de VENDAS  -->
			   </tbody>
		</table>
	</div>


	<div class="col-md-6">
	<!--Template GERAL para avaliacao
só disponível toda segunda-feira até as 00horas-->
		<div class="box-header">Histórico</div>


		<table class="table text-subhead v-middle fundobranco">
				 <thead style="display: none;">
			        <tr>
			           <th class="width-150 text-center">Foto</th>
			           <th class="width-150 text-center">Nome</th>   
			           <th class="width-150 text-center">Todo / Detalhe / Nota</th>
			        </tr>
			     </thead>
			     <tbody>
		         	    <!-- Inicio do template do subordinado de pontos  -->           
			             <tr>
				            <!--Imagem de foto perfil do subordinado-->	
					          <td>
								<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
					          </td> 
					         <!-- Nome do subordinado em questao-->  
					            <td> 
					               <span>Daniela Merculles</span>
					            </td>    
					          <td>
					        <!-- Ultima nota recebida da semana anterior--> 
					            <span class="btn-glyphicon clicavel" style="background: #bcbcbc;border-radius: 100%;color: #fff;padding: 9px 16px;">
					            		3
					            </span>	            
					          </td>
					          <td>
					          	<div class="avaliacaocalendariomes"> AGO </div>
					          	<div class="avaliacaocalendariodia"> 12 </div>
					          </td>						          
			        	   </tr>
						   <!-- final do template do subordinado de pontos  -->
						   <!-- Inicio do template do subordinado de VENDAS  -->           
			               <tr>
			            	<!--Imagem de foto perfil do subordinado-->	
				            	<td>
									<img src="/assets/img/avatar/default_user.png" class="media-photo imgmedalhaprata">
				            	</td> 
				            <!-- Nome do subordinado em questao-->  
				            	<td> 
				            	    <span>Carlos Santos </span>
				            	</td>    
				            	<!--Soma do valor total aprovado da ultima semana--> 
				            	<td>
									<label>R$  19.663,00</label>
				            	</td>
					          <td>
					          	<div class="avaliacaocalendariomes"> AGO </div>
					          	<div class="avaliacaocalendariodia"> 12 </div>
					          </td>				            	
			               </tr>
				          <!-- final do template do subordinado de VENDAS  -->
			   </tbody>
		</table>
	</div>
</div>
</div>

<!-- Função para supervisor dar nota  Modal deve fechar ao clicar na nota  -->

<div class="modal fade" id="nota_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document" style="width: 215px;margin-right: 480px;margin-top: 160px;">
                <form class="form-horizontal" id="formNotaSemanal" method="post">
			 <!-- Cadastrar nota selecionada no banco de dados  com o ID do funcionario ou usuario (essa avaliacao será realizada toda semana pelo supervisor do colaborador. -->
			            <div class="col-md-12">
			                <div class="box box-blue box-example-square">
			      <button type="button" class="close fecharnotadesempenho" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                  </button>			                
			                    <div class="box-header">NOTA DE DESEMPENHO</div>
			                    <div class="box-body">
									   <div  class="col-md-5 conteudoblocodesempenho">
											 <div class="br-widget">
												<a href="#" class="br-selected3">3</a>
												<a href="#" class="br-selected2">2</a>
												<a href="#" class="br-selected1">1</a>
												<a href="#" class="br-selected0">0</a>
											 </div>
										     <br><br><br><br>                  	
									    </div>
					                    <div  class="col-md-6">                    
					                         <span style="line-height: 33px;"> Excelente</span>
					                         <span style="line-height: 29px;"> Bom</span>
					                         <span style="line-height: 41px;"> Regular</span> 
					                         <span> Insuficiente</span> <br>
					                   </div>
			                    </div>
			                    <div class="box-footer">
						            <button class="btn btn-danger" href="#"> X 
						            </button>  
					                <label style="margin-top: 11px;">Desligar</label>
			                    </div>
			                </div>
			            </div>
                </form>
        </div>
</div>
<!-- Função para supervisor dar nota -->


<!-- 
Função para supervisor aprovar ou editar  valor da venda que foi inserido durante a semana pelo subordinado de VENDAS Modal deve 
-->

<div class="modal fade" id="valor_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document" style="width: 800px;margin-top: 160px;">
          <form class="form-horizontal" id="formAprovarValorSemanal" method="post">
			 <!-- Cadastrar nota selecionada no banco de dados  com o ID do funcionario ou usuario (essa avaliacao será realizada toda semana pelo supervisor do colaborador. -->
			<div class="col-md-12">
			    <div class="box box-blue box-example-square">
			      <button type="button" class="close fecharnotadesempenho" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                  </button>	

			      <div class="box-header">Vendas Faturadas (nome do subordinado)</div>
			           <div class="box-body">
							 <table>
							 	<thead>
							 		<tr>
							 			<th>Dom</th>
							 			<th>Seg</th>
							 			<th>Ter</th>
							 			<th>Qua</th>
							 			<th>Qui</th>
							 			<th>Sex</th>
							 			<th>Sab</th>
							 		</tr>
							 	</thead>
							 	<tbody>
							 	<tr> 
							 		<td>
							 			<input type="text" name="" value="0,00" disabled="disabled" class="form-control" >
							 		</td>
							 		<td>
							 			<input type="text" name="" value="4.567,00" disabled="disabled" class="form-control" >
							 		</td>
							 		<td>
							 			<input type="text" name="" value="1.200,00" disabled="disabled" class="form-control" >
							 		</td>
							 		<td>
							 			<input type="text" name="" value="2.608,00" disabled="disabled" class="form-control" >
							 		</td>
							 		<td>
							 			<input type="text" name="" value="3.200,00" disabled="disabled" class="form-control" >
							 		</td>
							 		<td>
							 			<input type="text" name="" value="4.600,00" disabled="disabled" class="form-control" >
							 		</td>
							 		<td>
							 			<input type="text" name="" value="3.488,00" disabled="disabled" class="form-control" >
							 		</td>
							 	</tr>
							 	<tr>
							 		<td> Valor Total</td>
							 	</tr>
							 	</tbody>
							 </table>
			          </div>

			         <div class="box-footer-edicao">
<!-- Aprovar vai dar um update na tabela confirmando os 
valores que já foram salvos pelos usuarios. -->
						<button class="btn btn-success" href="#"> Aprovar</button> 
<!-- 
editar vai fazer com que o input fique ativado podendo o superior modificar o valor do dia do subordinado dele -->
						<button class="btn btn-info" href="#"> Editar </button> 

				     </div>			          
			           <div class="box-footer">
						    <button class="btn btn-danger" href="#"> X </button>  
					         <label style="margin-top: 11px;"> Desligar Colaborador</label>
			            </div>
			         </div>
			      </div>
                </form>
        </div>
</div>
<!-- Função para supervisor validar ou editar valor da venda -->






<!-- 
Aqui vai listar o TODO do subordinado que foi clicado 
Esse modelo de TODO é o mesmo que foi criado no TODO individual mesmas classes e mesma estrutura apenas não tendo as acoes
-->

<div class="modal fade" id="todo_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">

         <!-- inicio do notepad do todo-->   
				<div class="col-md-12" id="notepadTopchamps">
		      <button type="button" class="close fecharnotadesempenho" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
               </button>	

				    <div class="main-content panel panel-default no-margin" style="position: initial;">
						<div class="panel-body">
							 <header class="panel-heading clearfix">
							    <div class="view-switcher">
							        <h2>TO DO</h2> 
							        <!--Nome do dono das tarefas listadas-->   
							          <span>Daniela Merculles </span>
	     					        <!--Nome do dono das tarefas listadas--> 
							    </div>
							 </header>
		               <div class="table-container">
							<table class="table table-filter todoTabela" id="tarefas_tableSubordinados">
                               <tbody>		
                 	<!-- Inicio de template-->	
                                 <tr data-status="pendentes" id="linha_58" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_58" name="check_58" class="badgebox" value="checado" checked="checked">
									               <span class="badge">✓</span>
									         </label>
										</td>
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
											<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
											<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">
													<h4 class="title" id="task_details">Teste de tarefa</h4>
												</div>
											</div>
										</td>
									</tr>
			<!-- Inicio de template-->					
								<tr data-status="pendentes" id="linha_48" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_48" name="check_48" class="badgebox" value="checado" checked="checked" disabled="disabled">
									               <span class="badge">✓</span>
									         </label>
										</td>
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">
													<h4 class="title" id="task_details"> Testando a subordinado tarefa
													</h4>
												</div>
											</div>
										</td>
								</tr>
				<!-- Inicio de template-->										
								<tr data-status="pendentes" id="linha_49" class="">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="check_49" name="check_49" class="badgebox" value="checado">
									               <span class="badge">✓</span>
									         </label>
										</td>
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>

												<div class="media-body">
													<h4 class="title" id="task_details">teste de tarefa subordinado teste</h4>
												</div>
											</div>
										</td>
							    </tr>

								<tr data-status="pendentes" id="tarefa_pendente" class="hide">
										<td style="cursor: default;"> 
									        <label for="success" class="btn btn-default" style="cursor: default;">  
									               <input type="checkbox" id="success_taskAberto" name="" class="badgebox" value="checado">
									               <span class="badge">✓</span>
									         </label>
										</td>
										<td style="cursor: default;"></td>
										<td style="cursor: default;">
											<div class="media">
												<a href="#" class="pull-left">
													<!--Imagem de foto perfil de quem criou a tarefa-->
													<img src="/assets/img/avatar/default_user.png" class="media-photo">
													<!--Imagem de foto perfil de quem criou a tarefa-->
												</a>
												<div class="media-body">

													<h4 class="title" id="task_details"></h4>
												</div>
											</div>
										</td>
								</tr>
							</tbody>
						</table>
		 </div>
							                    </div>        	
							               </div>
							          </div>
		  <!-- final do notepad todo-->   
        </div>
</div>
<!-- Função para supervisor dar nota -->
