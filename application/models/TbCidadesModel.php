<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TbCidadesModel extends MY_Model {

    public $table = 'tb_cidades'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->return_as = 'object';

        $this->has_one['estado'] = array('TbEstadosModel','tb_estados.id','estado');

        parent::__construct();
    }

    public function getCidadesByEstado($idUf){

        $query = $this->db->where('estado', $idUf)
            ->get('tb_cidades');

        $result = $query->result();

        if($query->num_rows() > 1) {
            return $result;
        }
        else {
            return false;
        }

    }

}