<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmpresaModel extends MY_Model {

    public $table = 'empresa'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->has_one['tb_cidades'] = array('CidadesModel','tb_cidades.id','id_tb_cidades');
        $this->has_one['tb_estados'] = array('EstadosModel','tb_estados.id','id_tb_estados');

        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->timestamps_format = 'Y-m-d H:i:s';
        $this->return_as = 'object';

        parent::__construct();
    }

    public function getEmpresaByIdUser($idUser){

        $this->db->select("empresa.*");
        $this->db->from("empresa");
        $this->db->join("users", "users.id_empresa = empresa.id");
        $this->db->where('users.id', $idUser);
        $query = $this->db->get();

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    public function getIdEmpresaByIdUser($idUser){

        $this->db->select("empresa.*");
        $this->db->from("empresa");
        $this->db->join("users", "users.id_empresa = empresa.id");
        $this->db->where('users.id', $idUser);
        $query = $this->db->get();

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0]->id;
        }
        else {
            return false;
        }
    }

    public function upsertEmpresaByIdUser($idUser, $data)
    {
        $ret = false;

        // checa se há alguma empresa ja cadastrada para este usuario
        $result = $this->getEmpresaByIdUser($idUser);
        if(!$result){
            $ret = $this->registerEmpresa($data);
        }else{
            $ret = $this->updateEmpresaByIdUser($idUser, $data);
        }

        return $ret;

    }

    public function registerEmpresa($idUser, $data=[])
    {
        if(count($data) != 1)
        {
            $data = array(
                'cnpj' => '',
                'cep' => ''
            );
        }

        $this->db->insert('empresa', $data);
        $idEmpresa = $this->db->insert_id();

        $this->load->model('UsersModel');
        $this->UsersModel->updateUserById($idUser, array('id_empresa' => $idEmpresa));

        return true;
    }

    public function updateEmpresaByIdUser($idUser, $data)
    {
        $idEmpresa = $this->getIdEmpresaByIdUser($idUser);

        $this->db->where('id', $idEmpresa);
        $this->db->update('empresa',$data);

        return $this->db->affected_rows();
    }

}