<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TarefasModel extends MY_Model {

    public $table = 'tarefas'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->return_as = 'object';
        parent::__construct();
    }

    public function addTarefa($data)
    {
        $this->db->insert('tarefas', $data);
        return true;
    }

    public function addComment($data)
    {
        $this->db->insert('tarefas_comentarios', $data);
        return true;
    }

    public function getTarefasVigentesByIdUser($idUser)
    {
        // Varivel que recebe o dia da semana (0 = Domingo, 1 = Segunda ...)
        $hoje = date('w');

        // Hoje eh segunda?
        if($hoje != 1)
            $data_inicial = date('Y-m-d 00:00:00', strtotime('last monday'));
        else
            $data_inicial = date('Y-m-d 00:00:00');


        $sql = "SELECT tarefas.id, users.name, funcionario.nome, tarefas.data_abertura, tarefas.data_fechamento, tarefas.tarefa, tarefas.id_supervisor as supervisor
                FROM tarefas
                LEFT JOIN funcionario ON tarefas.id_funcionario = funcionario.id
                JOIN users ON tarefas.id_supervisor=users.id
                WHERE (tarefas.id_supervisor = ? OR tarefas.id_funcionario = ?)
                AND tarefas.data_abertura >= ?
                AND tarefas.data_exclusao is null
                ORDER BY funcionario.nome ASC";

        $query = $this->db->query($sql, array($idUser, $idUser, $data_inicial));
        $results = $query->result_array();

        return $results;
    }

    public function getSubordinadosByIdUser($idUser)
    {
        $this->db->select ( 'id, nome' );
        $this->db->from ( 'funcionario' );
        $this->db->where( 'supervisor', $idUser );

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }

    public function getTarefaById($idTarefa)
    {
        $this->db->select ( 'id, DATE_FORMAT(data_abertura, "%d/%m/%Y %H:%i:%s") as data_abertura, tarefa' );
        $this->db->from ( 'tarefas' );
        $this->db->where( 'tarefas.id', $idTarefa);

        $query = $this->db->get();
        $results = $query->result_array();

        return $results[0];
    }

    public function softDeleteTarefaById($userId, $tarefaId)
    {
        $data['data_exclusao'] = date('Y-m-d H:i:s');
        $this->db->where( 'tarefas.id', $tarefaId );
        $this->db->where( 'tarefas.id_supervisor', $userId );
        $this->db->update('tarefas',$data);
    }

    public function finalizaTarefaById($userId, $tarefaId)
    {
        $data['data_fechamento'] = date('Y-m-d H:i:s');
        $this->db->where( 'tarefas.id', $tarefaId );
        $this->db->where( 'tarefas.id_supervisor', $userId );
        $this->db->update('tarefas',$data);
    }

    public function getCommentsByIdTarefa($idTarefa)
    {
        $this->db->select ( 'users.name, tc.comentario, tc.data' );
        $this->db->from ( 'tarefas_comentarios as tc' );
        $this->db->join ( 'users', 'tc.id_user=users.id');
        $this->db->where( 'tc.id_tarefa', $idTarefa );

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }

    public function updateTarefaById($idTarefa, $data)
    {
        $this->db->where('id', $idTarefa);
        $this->db->update('tarefas',$data);
        return $this->db->affected_rows();
    }


}