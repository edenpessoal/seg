<?php
namespace Application\Models;

use MY_Model;

defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionario extends MY_Model {

    private $ci;

    public $table = 'funcionario'; // Set the name of the table for this model.
    public $primary_key = 'id_funcionario'; // Set the primary key

    public function __construct()
    {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->timestamps_format = 'Y-m-d H:i:s';
        $this->return_as = 'object';

        $this->has_one['empresa'] = array('Empresa','empresa.id_empresa','fk_empresa');

        parent::__construct();
    }

    public function getFuncionariosByIdUser($idUser){

        $sql = "SELECT 
                funcionario.id_funcionario, 
                funcionario.setor, 
                funcionario.nome, 
                funcionario.cargo, 
                funcionario.salario,
                funcionario.email, 
                funcionario.supervisor, 
                f.nome AS supervisor_nome,
                IF(funcionario.fk_users IS NULL, 'NÃO', 'SIM') AS admin
                FROM funcionario
                JOIN empresa ON funcionario.fk_empresa = empresa.id_empresa
                JOIN users ON users.id_empresa=empresa.id_empresa
                LEFT JOIN funcionario AS f ON funcionario.supervisor=funcionario.id_funcionario
                WHERE users.id = ?
                ORDER BY funcionario.nome ASC";

        $query = $this->db->query($sql, array($idUser));
        $results = $query->result_array();

        return $results;

    }


    public function getFuncionariosForMetasByIdUser($idUser){

        $this->db->select ( 'funcionario.id_funcionario, funcionario.nome' );
        $this->db->from ( 'funcionario' );
        $this->db->join ( 'empresa', 'funcionario.fk_empresa = empresa.id_empresa');
        $this->db->join ( 'users', 'users.id_empresa = empresa.id_empresa');
        $this->db->where( 'funcionario.setor', \Application\Models\Funcionario\Setor::VENDAS );
        $this->db->where( 'users.id', $idUser );

        $query = $this->db->get();
        $results = $query->result_array();

        return $results;

    }

    public function addFuncionario($idUser, $data)
    {
        $idEmpresa = $this->getIdEmpresaByIdUser($idUser);

        if($idEmpresa)
        {
            $data['fk_empresa'] = $idEmpresa;
            $this->db->insert('funcionario', $data);

            // pega o funcionario inserido para retornar o json
        return $this->getFuncionarioByIdFuncionario($idUser, $this->db->insert_id());
        }
        else
        {
            return false;
        }
    }

    public function getFuncionarioByIdFuncionario($idUser, $idFuncionario)
    {

        $sql = "SELECT 
                funcionario.id_funcionario, 
                funcionario.setor, 
                funcionario.nome, 
                funcionario.cargo, 
                funcionario.salario,
                funcionario.email, 
                funcionario.supervisor, 
                funcionario.nome AS supervisor_nome,
                IF(funcionario.fk_users IS NULL, 'NÃO', 'SIM') AS admin
                FROM funcionario
                JOIN empresa ON funcionario.fk_empresa = empresa.id_empresa
                JOIN users ON users.id_empresa = empresa.id_empresa
                LEFT JOIN funcionario AS f ON funcionario.supervisor=funcionario.id_funcionario
                WHERE users.id = ?
                AND funcionario.id_funcionario = ?
                ORDER BY funcionario.nome ASC";

        $query = $this->db->query($sql, array($idUser, $idFuncionario));
        $results = $query->result_array();

        return $results;
    }

    public function deleteFuncionarioById($idFuncionario, $idUser){

        $this->db->query("
            delete funcionario
            from funcionario
            join empresa on (funcionario.fk_empresa = empresa.id_empresa)
            join users on (users.id_empresa = empresa.id_empresa)
            where users.id=?
            and funcionario.id_funcionario=?;",
            array($idUser, $idFuncionario));

        // TODO: se for admin, tem q apagar tambem da tabela users

        return $this->db->affected_rows();


    }

    public function updateFuncionarioById($idUser, $idFuncionario, $data){

        // FIXME: validar o id do usuário também!!!! (IMPORTANTE)
        $this->db->where( 'funcionario.id_funcionario', $idFuncionario );
        $this->db->update('funcionario',$data);

        // TODO: checar se mudou para admin e add na tabela users

        return $this->db->affected_rows();

    }

    public function getFuncionarioById($idFuncionario){

        $query = $this->db->where('id', $idFuncionario)
            ->get('funcionario');

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }


    }

    public function getIdEmpresaByIdUser($idUser){

        $this->db->select ( 'id_empresa' );
        $this->db->from ( 'users' );
        $this->db->where( 'id', $idUser );

        $query = $this->db->get();
        $results = $query->result();

        if(count($results) > 0) {
            return $results[0]->id_empresa;
        }else{
            return false;
        }

    }

    public function getIdEmpresaByIdFuncionario($idFuncionario){

        $this->db->select ( 'fk_empresa' );
        $this->db->from ( 'funcionario' );
        $this->db->where( 'id_funcionario', $idFuncionario );

        $query = $this->db->get();
        $results = $query->result();

        if(count($results) > 0) {
            return $results[0]->fk_empresa;
        }else{
            return false;
        }

    }

    public function getFuncionariosByIdEmpresa($idEmpresa){

        $this->db->select ( '*' );
        $this->db->from ( 'funcionario' );
        $this->db->where( 'fk_empresa', $idEmpresa );

        $query = $this->db->get();
        $results = $query->result();

        return $results;

    }

    public function getUsersIdByIdFuncionario($idFuncionario)
    {
        $this->db->select ( 'fk_users' );
        $this->db->from ( 'funcionario' );
        $this->db->where( 'id_funcionario', $idFuncionario );

        $query = $this->db->get();
        $results = $query->result();

        if(count($results) > 0 && !is_null($results[0]->fk_users)) {
            return $results[0]->fk_users;
        }else{
            return false;
        }
    }

    public function deleteUsersIdByIdFuncionario($idFuncionario)
    {
        if($usersId = $this->getUsersIdByIdFuncionario($idFuncionario))
        {
            $this->load->model('tank_auth/users', 'tankmodel');
            $this->tankmodel->delete_user($usersId);
        }

        return true;
    }

    public function changeUsersEmail($usersId, $email)
    {
        $this->ci =& get_instance();

        $this->ci->load->config('tank_auth', TRUE);

        $this->ci->load->library('session');
        $this->ci->load->database();
        $this->ci->load->model('tank_auth/users');

        $errors = false;

        if (!is_null($user = $this->ci->users->get_user_by_id($usersId, FALSE))) {

            $data = array(
                'user_id'	=> $usersId,
                'username'	=> $user->username,
                'email'		=> $email,
            );

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                throw new Exception("E-mail inválido");
            }

            if (strtolower($user->email) == strtolower($email)) {		
            // leave activation key as is
                $data['new_email_key'] = $user->new_email_key;
                return $data;

            } elseif ($this->ci->users->is_email_available($email)) {
                $data['new_email_key'] = md5(rand().microtime());
                $this->ci->users->set_new_email($usersId, $email, $data['new_email_key'], FALSE);
                return $data;

            } else {
                throw new Exception("Este e-mail já em uso por outro usuário");
            }
        }

        return null;
    }

    public function changeUsersPassword($usersId, $new_pass)
    {

        $this->ci =& get_instance();

        $this->ci->load->config('tank_auth', TRUE);

        $this->ci->load->library('session');
        $this->ci->load->database();
        $this->ci->load->model('tank_auth/users');

        if (!is_null($user = $this->ci->users->get_user_by_id($usersId, TRUE))) {

            // Check if old password correct
            $hasher = new PasswordHash(
                $this->ci->config->item('phpass_hash_strength', 'tank_auth'),
                $this->ci->config->item('phpass_hash_portable', 'tank_auth'));
            // Hash new password using phpass
            $hashed_password = $hasher->HashPassword($new_pass);

            // Replace old password with new one
            $this->ci->users->change_password($usersId, $hashed_password);
            return TRUE;
        }

        throw new Exception("Usuário não encontrado");
    }

    public function createUserStatic( $data )
    {
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $email_activation = $this->config->item('email_activation', 'tank_auth');

        if (!is_null($dataRegister = $this->tank_auth->create_user(
            '',
            $data['email'],
            $data['password'],
            $email_activation)))
        {
            // success
            $dataRegister['site_name'] = $this->config->item('website_name', 'tank_auth');

            if ($email_activation)
            {
                $dataRegister['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;
                unset($dataRegister['password']);

            }
            else
            {
                if ($this->config->item('email_account_details', 'tank_auth'))
                {
                }
            }

            return $dataRegister['user_id'];

        }
        else
        {
            $errors = $this->tank_auth->get_error_message();
            $error = "";
            foreach ($errors as $k => $v){
                $error .= $this->lang->line($v)."\n";
            }
            throw new Exception($error);
        }

    }

    public function createNewUser()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');

        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash');
        }
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'Confirmação de senha', 'trim|required|xss_clean|matches[password]');

        $captcha_registration	= $this->config->item('captcha_registration', 'tank_auth');
        $use_recaptcha			= $this->config->item('use_recaptcha', 'tank_auth');

        if ($captcha_registration) {
            if ($use_recaptcha) {
                $this->form_validation->set_rules('recaptcha_response_field', 'Código de confirmação', 'trim|xss_clean|required|callback__check_recaptcha');
            } else {
                $this->form_validation->set_rules('captcha', 'Código de confirmação', 'trim|xss_clean|required|callback__check_captcha');
            }
        }

        $email_activation = $this->config->item('email_activation', 'tank_auth');

        if ($this->form_validation->run())
        {
            if (!is_null($dataRegister = $this->tank_auth->create_user(
                $use_username ? $this->form_validation->set_value('username') : '',
                $this->form_validation->set_value('email'),
                $this->form_validation->set_value('password'),
                $email_activation)))
            {
                // success
                $dataRegister['site_name'] = $this->config->item('website_name', 'tank_auth');

                if ($email_activation)
                {
                    $dataRegister['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;
                    // FIXME : utils
                    // $this->_send_email('activate', $dataRegister['email'], $dataRegister);
                    unset($dataRegister['password']);

                }
                else
                {
                    if ($this->config->item('email_account_details', 'tank_auth'))
                    {
                        // FIXME : utils
                        // send "welcome" email
                        //  $this->_send_email('welcome', $dataRegister['email'], $dataRegister);
                    }
                }

                return $dataRegister['user_id'];

            }
            else
            {
                $errors = $this->tank_auth->get_error_message();
                $error = "";
                foreach ($errors as $k => $v){
                    $error .= $this->lang->line($v)."\n";
                }


                throw new Exception($error);
            }
        }else
        {
            $errors = "";
            foreach($this->form_validation->error_array() as $k => $v)
            {
                $errors .= $v."\n";
            }
            throw new Exception($errors);
        }
    }

    //Funcao para buscar funcionarios da empresa (usuario logado empresaID)
    public function procurarfuncionario($keyword, $idEmpresa){
            $this->db->select('*')->from('funcionario');
            $this->db->where('fk_empresa', $idEmpresa );

            $this->db->like('nome',$keyword,'after');
            $this->db->or_like('email',$keyword,'after');

            $query = $this->db->get();
            return $query->result();
        }

}
