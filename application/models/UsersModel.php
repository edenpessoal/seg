<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends MY_Model {

    public $table = 'users'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key

    public function __construct()
    {
        $this->return_as = 'object';
        parent::__construct();
    }

    public function getUserById($idUser){

        $query = $this->db->where('id', $idUser)
            ->get('users');

        $result = $query->result();

        if($query->num_rows() == 1) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    public function updateUserById($idUser, $data){

        $this->db->where('id', $idUser)
            ->update('users', $data);

        return $this->db->affected_rows();
    }

    public function getUsersByIdGroup($idGroup){

        $query = $this->db
            ->where('id_group', $idGroup)
            ->get('users');

        if($query->num_rows() > 0) {
            return $query->result();
        }
        else{
            return false;
        }

    }

    public function setGroupIdByIdUser($idUser, $idGroup){

        $data = array(
            'id_group' => $idGroup
        );

        $this->db->where('id', $idUser);
        $this->db->update('users', $data);

    }

    public function getGroupIdByIdUser($idUser){

        $query = $this->db->select('id_group')
            ->where('id', $idUser)
            ->get('users');

        $ret = $query->row();
        return $ret->id_group;

    }
}