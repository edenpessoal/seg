<?php
/**
 * Model para gerenciar os status dos usuários
 *
 * @copyright Copyright (c) 2016 TopChamps (http://topchamps.com.br)
 */
namespace Application\Models\Users;

use Application\Models\StatusAbstract;

class Status extends StatusAbstract
{
    CONST APROVADO  = 'A';
    CONST LIBERADO  = 'L';
    CONST PENDENTE  = 'P';
    CONST CANCELADO = 'C';
    CONST BLOQUEADO = 'B';

    static protected $status = array(
        self::APROVADO   => array('Aprovado',  'Aprovado'),
        self::LIBERADO   => array('Liberado',  'Liberado'),
        self::PENDENTE   => array('Pendente',  'Pendente'),
        self::CANCELADO  => array('Cancelado', 'Cancelado'),
        self::BLOQUEADO  => array('Bloqueado', 'Bloqueado'),
    );
}