<?php
namespace Application\Models\Avaliacoes\Dw;

use Application\Models\Avaliacoes\DwAbstract;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Model para setar os DW com infos do ano
 * @author mario
 */
class Ano extends DwAbstract
{
    /**
     *  Insere uma linha completa no dw com informações dos pontos
     *  do Ano
     *
     * @param array $set
     * @return int/array
     */
    public function insertFuncionario($set = nul)
    {
        $set = $this->_formatSet($set);


        if (isset($set['semana'])) {
            $set['semana'] = 0;
        }

        if (isset($set['mes'])) {
            $set['mes'] = 0;
        }

        if (!isset($set['data_created'])) {
            $set['data_created'] = date('Y-m-d H:i:s');
        }

        return parent::_insert($set);
    }

    /**
     *  Insere uma linha completa no dw com informações dos pontos
     *  do Ano
     *
     * @param array $set
     * @return int/array
     */
    public function insertEmpresa($set = nul)
    {
        $set = $this->_formatSet($set);

        if (isset($set['fk_funcionario'])) {
            $set['fk_funcionario'] = 0;
        }

        if (isset($set['semana'])) {
            $set['semana'] = 0;
        }

        if (isset($set['mes'])) {
            $set['mes'] = 0;
        }

        if (!isset($set['data_created'])) {
            $set['data_created'] = date('Y-m-d H:i:s');
        }

        return parent::_insert($set);
    }

    /**
     *  Atualiza uma linha completa no dw com informações dos pontos
     *  do Ano, Mes
     *
     * @param array $set
     * @return int/array
     */
    public function updateFuncionario($set = null, $column_name_where = NULL, $escape = TRUE)
    {
        $set = $this->_formatSet($set);

        if (isset($set['semana'])) {
            $set['semana'] = 0;
        }

        if (isset($set['mes'])) {
            $set['mes'] = 0;
        }

        if (!isset($set['data_updated'])) {
            $set['data_updated'] = date('Y-m-d H:i:s');
        }

        return parent::_update($set, $column_name_where, $escape);
    }

    /**
     *  Atualiza uma linha completa no dw com informações dos pontos
     *  do Ano, Mes
     *
     * @param array $set
     * @return int/array
     */
    public function updateEmpresa($set = null, $column_name_where = NULL, $escape = TRUE)
    {
        $set = $this->_formatSet($set);


        if (isset($set['fk_funcionario'])) {
            $set['fk_funcionario'] = 0;
        }

        if (isset($set['semana'])) {
            $set['semana'] = 0;
        }

        if (isset($set['mes'])) {
            $set['mes'] = 0;
        }

        if (!isset($set['data_updated'])) {
            $set['data_updated'] = date('Y-m-d H:i:s');
        }

        return parent::_update($set, $column_name_where, $escape);
    }

    /*
     * Método para calcular a média (pontos ou salario) do ano X para o funcionario
     */
    public function calculateFuncionario($set)
    {
        if (!isset($set['fk_funcionario'])) {
            throw new \Exception("O fk_funcionario é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['fk_meta'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['fk_empresa'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['ano'])) {
            throw new \Exception("O ano é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['valor']) && !isset($set['ponto'])) {
            throw new \Exception("O valor ou ponto é obrigatório ".get_class($this)."::calculate().");
        }

        $keyToSearch = array(
                'fk_empresa'    => $set['fk_empresa'],
                'fk_funcionario'=> $set['fk_funcionario'],
                'fk_meta'   => $set['fk_meta'],
                'ano'       => $set['ano'],
                'mes <>'    => 0,
                'semana <>' => 0,
            );

        //busca todos os registros
        $query = $this->db->get_where("{$this->table}", $keyToSearch);
        $result = $query->result();

        $somaPontoOuValor = $media = 0;

        //verifica se é pra utilizar o campo valor ou ponto para somar
        $fieldToUse = $this->getUseValorOuPonto($set);

        //verifica se retornou resultado
        if ($query->num_rows() >= 1) {
            //percorre os resultados
            foreach ($result as $linha) {
                //soma os valores
                $somaPontoOuValor+= $linha->$fieldToUse;
            }

            //media
            $media = ($somaPontoOuValor/$query->num_rows());
        }

        // Iniciando dados da média montando a chave para verifica se ja existe
        $setInsert = array(
            'fk_empresa'     => $set['fk_empresa'],
            'fk_meta'        => $set['fk_meta'],
            'fk_funcionario' => $set['fk_funcionario'],
            'ano'            => $set['ano'],
            'mes'            => 0,
            'semana'         => 0,
        );

        //busca na base
        $lookUp = $this->db->get_where("{$this->table}", $setInsert);
        $resLookUp =  $lookUp->result_array();

        $setInsert["media_{$fieldToUse}"] = $media;
        $setInsert[$fieldToUse]           = $somaPontoOuValor;
        $setInsert['salario']             = $set['salario'];

        if (isset($setInsert['ponto'])) {
            $setInsert['valor'] = 0;
        } else {
            $setInsert['ponto'] = 0;
        }

        //se ja existe, atualiza
        if ($lookUp->num_rows() >= 1) {
            $this->updateFuncionario($setInsert, $resLookUp[0]);

        //senao insere
        } else {
            $this->insertFuncionario($setInsert);
        }
    }

    /*
     * Método para calcular a média (pontos E salario) do ano X para a empresa Geral
     */
    public function calculateEmpresa($set)
    {
        if (!isset($set['fk_meta'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['fk_empresa'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['ano'])) {
            throw new \Exception("O ano é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['valor']) && !isset($set['ponto'])) {
            throw new \Exception("O valor ou ponto é obrigatório ".get_class($this)."::calculate().");
        }

        $keyToSearch = array(
            'fk_empresa'=> $set['fk_empresa'],
            'fk_meta'   => $set['fk_meta'],
            'ano'       => $set['ano'],
            'mes <>'    => 0,
            'semana <>' => 0,
            'fk_funcionario <>' => 0,
        );

        //busca todos os registros
        $query = $this->db->get_where("{$this->table}", $keyToSearch);
        $result = $query->result();

        $somaPonto = $somaValor = $somaSalario = $mediaPonto = $mediaValor = 0;

        //verifica se retornou resultado
        if ($query->num_rows() >= 1) {
            //percorre os resultados
            foreach ($result as $linha) {
                //soma os valores
                $somaPonto += $linha->ponto;
                $somaValor += $linha->valor;
                $somaSalario += $linha->salario;
            }

            //media
            $mediaPonto = ($somaPonto/$query->num_rows());
            $mediaValor = ($somaValor/$query->num_rows());
        }

        // Iniciando dados da média montando a chave para verifica se ja existe
        $setInsert = array(
            'fk_empresa'=> $set['fk_empresa'],
            'fk_meta'   => $set['fk_meta'],
            'ano'       => $set['ano'],
            'mes'             => 0,
            'fk_funcionario'  => 0,
            'semana'          => 0,
        );

        //busca na base
        $lookUp = $this->db->get_where("{$this->table}", $setInsert);
        $resLookUp =  $lookUp->result_array();

        $setInsert['media_ponto']     = $mediaPonto;
        $setInsert['media_valor']     = $mediaValor;
        $setInsert['ponto']           = $somaPonto;
        $setInsert['valor']           = $somaValor;
        $setInsert['salario']         = $somaSalario;

        //se ja existe, atualiza
        if ($lookUp->num_rows() >= 1) {
            $this->updateEmpresa($setInsert, $resLookUp[0]);

            //senao insere
        } else {
            $this->insertEmpresa($setInsert);
        }
    }



    /**
     * Retorna o ranking dos acumulado até a data da meta X
     *
     * @param int $fk_empresa
     * @param int $fk_meta
     * @return unknown|boolean
     */
    public function getRanking(Int $fk_empresa, Int $fk_meta)
    {
        $where = array(
                "{$this->table}.fk_empresa"=> $fk_empresa,
                "{$this->table}.fk_meta"   => $fk_meta,
                "{$this->table}.ano"       => 2016,
                "{$this->table}.mes"       => 0,
                "{$this->table}.semana"    => 0,
            );

        //busca todos os funcionarios
        $query = $this->db->join('funcionario', "funcionario.id_funcionario={$this->table}.fk_funcionario", 'left')->get_where("{$this->table}", $where);
        $result = $query->result();
        if ($query->num_rows() >= 1) {

            $ranking = array();

            //calcula o ranking Salario X ponto
            foreach ($result as $r) {
                $set['id_funcionario'] = $r->fk_funcionario;
                $set['nome'] = $r->nome;
                $set['fator'] = $r->ponto * $r->salario;
//                 echo 'Funcionario - ' . $r->fk_funcionario . ' Fator Ponto x Salario = '.$r->ponto * $r->salario;
//                 echo '<br>';

                $ranking[] = $set;
            }

            //ordena o array pelo maior fator
            usort($ranking, function ($a, $b) {
                return $a['fator'] < $b['fator'];
            });

            return $result;
        } else {
            return false;
        }
    }


    protected function _formatSet($set)
    {
        $set = parent::_formatSet($set);

        return $set;
    }
}