<?php
namespace Application\Models\Avaliacoes\Dw;

use Application\Models\Avaliacoes\DwAbstract;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Model para setar os DW com infos da semana
 * @author mario
 */
class Semana extends DwAbstract
{
    protected $useCache = false;

    /**
     *  Insere uma linha completa no dw com informações dos pontos
     *  do Ano, Mes, Semana
     *
     * @param array $set
     * @return int/array
     */
    public function insertFuncionario($set = null)
    {
        $set = $this->_formatSet($set);

        if (!isset($set['created_at'])) {
            $set['created_at'] = date('Y-m-d H:i:s');
        }

        return parent::_insert($set);
    }

    /**
     *  Insere uma linha completa no dw com informações dos pontos
     *  do Ano, Mes, Semana
     *
     * @param array $set
     * @return int/array
     */
    public function insertEmpresa($set = null)
    {
        $set = $this->_formatSet($set);

        if (isset($set['fk_funcionario'])) {
            $set['fk_funcionario'] = 0;
        }

        if (!isset($set['created_at'])) {
            $set['created_at'] = date('Y-m-d H:i:s');
        }

        return parent::_insert($set);
    }

    /**
     *  Insere uma linha completa no dw com informações dos pontos
     *  do Ano, Mes, Semana
     *
     * @param array $set
     * @return int/array
     */
    public function updateFuncionario($set = null, $column_name_where = NULL, $escape = TRUE)
    {
        $set = $this->_formatSet($set);

        if (!isset($set['updated_at'])) {
            $set['updated_at'] = date('Y-m-d H:i:s');
        }

        return parent::_update($set, $column_name_where, $escape);
    }

    /**
     *  Insere uma linha completa no dw com informações dos pontos
     *  do Ano, Mes, Semana
     *
     * @param array $set
     * @return int/array
     */
    public function updateEmpresa($set = null, $column_name_where = NULL, $escape = TRUE)
    {
        $set = $this->_formatSet($set);

        if (isset($set['fk_funcionario'])) {
            $set['fk_funcionario'] = 0;
        }

        if (!isset($set['updated_at'])) {
            $set['updated_at'] = date('Y-m-d H:i:s');
        }

        return parent::_update($set, $column_name_where, $escape);
    }

    /*
     * Método para calcular a média (pontos ou salario) da semana X
     */
    public function calculateFuncionario($set)
    {
        if (!isset($set['fk_funcionario'])) {
            throw new \Exception("O fk_funcionario é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['fk_meta'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['fk_empresa'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['ano'])) {
            throw new \Exception("O ano é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['mes'])) {
            throw new \Exception("O mes é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['semana'])) {
            throw new \Exception("A semana é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['valor']) && !isset($set['ponto'])) {
            throw new \Exception("O valor ou ponto é obrigatório ".get_class($this)."::calculate().");
        }

        //verifica se é pra utilizar o campo valor ou ponto para somar
        $fieldToUse = $this->getUseValorOuPonto($set);

        // Iniciando dados da média montando a chave para verifica se ja existe
        $setInsert = array(
            'fk_funcionario'  => $set['fk_funcionario'],
            'fk_empresa'=> $set['fk_empresa'],
            'fk_meta'   => $set['fk_meta'],
            'ano'       => $set['ano'],
            'mes'       => $set['mes'],
            'semana'    => $set['semana'],
        );

        //busca na base
        $lookUp = $this->db->get_where("{$this->table}", $setInsert);
        $resLookUp =  $lookUp->result_array();

        $setInsert[$fieldToUse]           = $set[$fieldToUse];
        $setInsert["media_{$fieldToUse}"] = $set[$fieldToUse];
        $setInsert['salario']             = $set['salario'];

        if (isset($setInsert['ponto'])) {
            $setInsert['valor'] = 0;
        } else {
            $setInsert['ponto'] = 0;
        }

        //verifica se ja existe
        if ($lookUp->num_rows() >= 1) {
            $this->updateFuncionario($setInsert, $resLookUp[0]);
        //senao insere
        } else {
            $this->insertFuncionario($setInsert);
        }
    }

    /*
     * Método para calcular a média (pontos ou salario) da semana X para a Empresa
     */
    public function calculateEmpresa($set)
    {
        if (!isset($set['fk_meta'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['fk_empresa'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['ano'])) {
            throw new \Exception("O ano é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['mes'])) {
            throw new \Exception("O mes é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['semana'])) {
            throw new \Exception("A semana é obrigatório ".get_class($this)."::calculate().");
        }

        if (!isset($set['valor']) && !isset($set['ponto'])) {
            throw new \Exception("O valor ou ponto é obrigatório ".get_class($this)."::calculate().");
        }

        $keyToSearch = array(
            'fk_empresa'    => $set['fk_empresa'],
            'fk_meta'       => $set['fk_meta'],
            'ano'           => $set['ano'],
            'mes'           => $set['mes'],
            'semana'        => $set['semana'],
            'fk_funcionario <>' => 0,
        );

        //busca todos os registros
        $query = $this->db->get_where("{$this->table}", $keyToSearch);
        $result = $query->result();

        $somaPonto = $somaValor = $somaSalario = $mediaPonto = $mediaValor = 0;
        //verifica se é pra utilizar o campo valor ou ponto para somar
        $fieldToUse = $this->getUseValorOuPonto($set);

         //verifica se retornou resultado
        if ($query->num_rows() >= 1) {
            //percorre os resultados
            foreach ($result as $linha) {
                //soma os valores
                $somaPonto += $linha->ponto;
                $somaValor += $linha->valor;
                $somaSalario += $linha->salario;
            }

            //media
            $mediaPonto = ($somaPonto/$query->num_rows());
            $mediaValor = ($somaValor/$query->num_rows());
        }

        // Iniciando dados da média montando a chave para verifica se ja existe
        $setInsert = array(
            'fk_empresa'=> $set['fk_empresa'],
            'fk_meta'   => $set['fk_meta'],
            'ano'       => $set['ano'],
            'mes'       => $set['mes'],
            'semana'    => $set['semana'],
            'fk_funcionario'  => 0,
        );

        //busca na base
        $lookUp = $this->db->get_where("{$this->table}", $setInsert);
        $resLookUp =  $lookUp->result_array();

        $setInsert['media_ponto']     = $mediaPonto;
        $setInsert['media_valor']     = $mediaValor;
        $setInsert['ponto']           = $somaPonto;
        $setInsert['valor']           = $somaValor;
        $setInsert['salario']         = $somaSalario;

        //se ja existe, atualiza
        if ($lookUp->num_rows() >= 1) {
            $this->updateEmpresa($setInsert, $resLookUp[0]);

            //senao insere
        } else {
            $this->insertEmpresa($setInsert);
        }
    }


    /**
     * Retorna o ranking dos acumulado até a data da meta X
     *
     * @param int $fk_empresa
     * @param int $fk_meta
     * @param int $ano
     * @return unknown|boolean
     */
    public function getRankingOutrasAreas(Int $fk_empresa, Int $fk_meta, Int $ano = null)
    {
        if (empty($ano)) $ano = date('Y');

       $where = array(
            "{$this->table}.fk_empresa"=> $fk_empresa,
            "{$this->table}.fk_meta"   => $fk_meta,
            "{$this->table}.ano"       => $ano,
            "{$this->table}.mes"       => 0,
            "{$this->table}.semana"    => 0,
            "{$this->table}.fk_funcionario <>"    => 0,
        );

        $where['ponto is not'] = NULL;
        $where['(valor is null or valor = 0)'] = NULL;

        //busca todos os funcionarios
        $query = $this->db->join('funcionario', "funcionario.id_funcionario={$this->table}.fk_funcionario", 'left')->get_where("{$this->table}", $where);
        $result = $query->result();

        if ($query->num_rows() >= 1) {

            $ranking = array();

            //calcula o ranking Salario X ponto
            foreach ($result as $r) {
                $set['id_funcionario'] = $r->fk_funcionario;
                $set['nome'] = $r->nome;
                $set['fator'] = $r->ponto * $r->salario;
                $set['salario'] = $r->salario;
                $set['ponto'] = $r->ponto;
//                 echo 'Funcionario - ' . $r->fk_funcionario . ' Fator Ponto x Salario = '.$r->ponto * $r->salario;
//                 echo '<br>';

                $ranking[] = $set;
            }

            //ordena o array pelo maior fator
            usort($ranking, function ($a, $b) {
                return $a['fator'] < $b['fator'];
            });

            return $ranking;
        } else {
            return false;
        }
    }

    /**
     * Retorna o ranking dos acumulado até a data da meta X
     *
     * @param int $fk_empresa
     * @param int $fk_meta
     * @param string $setor Vendas ou Outros
     * @param int $ano
     * @return unknown|boolean
     */
    public function getRankingVendas(Int $fk_empresa, Int $fk_meta, Int $ano = null)
    {
        if (empty($ano)) $ano = date('Y');

        $where = array(
            "{$this->table}.fk_empresa"=> $fk_empresa,
            "{$this->table}.fk_meta"   => $fk_meta,
            "{$this->table}.ano"       => $ano,
            "{$this->table}.mes"       => 0,
            "{$this->table}.semana"    => 0,
            "{$this->table}.fk_funcionario <>"    => 0,
        );

        $where['valor is not'] = NULL;
        $where['(ponto is null or ponto = 0) '] = NULL;

        //busca todos os funcionarios
        $query = $this->db->join('funcionario', "funcionario.id_funcionario={$this->table}.fk_funcionario", 'left')->get_where("{$this->table}", $where);
        $result = $query->result();

        //seto a semana atual para eu buscar todos os resultados entre o inicio da meta e semana atual
        $dayOfWeek = date('w');
        $week_start = date('Y-m-d', strtotime('-'.$dayOfWeek.' days'));
        $week_end = date('Y-m-d', strtotime('+'.(6-$dayOfWeek).' days'));

        $metasQuery = $this->db->get_where("meta_semanal", array("fk_empresa" => $fk_empresa, "fk_meta" => $fk_meta, "data_final <" => $week_start));
        $metasResult = $metasQuery->result();
        $acumuladoMeta = 0;

        if ($metasQuery->num_rows() >= 1) {
            foreach ($metasResult as $r) {
                $acumuladoMeta+=$r->meta_semanal;
            }
        } else {
            throw new \Exception('Erro ao definir acumulado de meta semanal em '.get_class($this) . '::getRankingVendas()');
        }

        if ($query->num_rows() >= 1) {

            $ranking = array();

            //calcula o ranking Salario X ponto
            foreach ($result as $r) {
                $set['id_funcionario'] = $r->fk_funcionario;
                $set['nome'] = $r->nome;
                $set['fator'] = $r->valor / $acumuladoMeta;
                $set['meta'] = $acumuladoMeta;
                $set['valor'] = $r->valor;

                $ranking[] = $set;
            }

            //ordena o array pelo maior fator
            usort($ranking, function ($a, $b) {
                return $a['fator'] < $b['fator'];
            });

            return $ranking;
        } else {
            return false;
        }
    }

    public function fetchAssocPorSemana($where = null)
    {
        $fetchAll = $this->get_all($where);

        if (empty($fetchAll)) {
            return null;
        }

        // Associa pela chave da tabela
        $fetchAssoc = array();

        foreach ($fetchAll as $k => $u) {
            $fetchAssoc[(int)$u->semana] = $u;
            unset($fetchAll[$k]);
        }

        // Some garbage collection
        unset($fetchAll);

        // Retorna o array reordenado
        return $fetchAssoc;
    }


    protected function _formatSet($set)
    {
        $set = parent::_formatSet($set);

        return $set;
    }
}