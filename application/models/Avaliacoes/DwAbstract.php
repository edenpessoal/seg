<?php
namespace Application\Models\Avaliacoes;

use MY_Model;

class DwAbstract extends MY_Model
{
    const VALOR = 'valor';
    const PONTO = 'ponto';

    public $table = 'avaliacoes_dw';
    // Set the name of the table for this model.
    public $primary_key = array('fk_funcionario', 'fk_meta', 'fk_empresa', 'ano', 'mes', 'semana') ;
    // Set the primary key
    public function __construct ()
    {
        $this->has_one['funcionario'] = array(
                'Funcionario',
                'funcionario.id_funcionario',
                'fk_funcionario'
        );

        $this->has_one['meta'] = array(
                'Meta',
                'meta.id_meta',
                'fk_meta'
        );

        $this->has_one['empresa'] = array(
                'Empresa',
                'empresa.id_empresa',
                'fk_empresa'
        );

        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->timestamps_format = 'Y-m-d H:i:s';
        $this->return_as = 'object';

        parent::__construct();
    }

    /**
     * (non-PHPdoc)
     * @see \Realejo\App\Model\Db::insert()
     */
    protected function _insert($set)
    {
        $set = $this->_formatSet($set);

        // Grava o relatÃ³rio
        $insert = parent::insert($set);

        return $insert;
    }

    /**
     * (non-PHPdoc)
     * @see \Realejo\App\Model\Db::update()
     */
    protected function _update($set, $key)
    {
        $set = $this->_formatSet($set);

        // Grava as alteraÃ§Ãµes
        $update = parent::update($set, $key);
        return $update;
    }

    /**
     * Formata os dados antes do insert/update
     *
     * @param array $set
     * @return array
     */
    protected function _formatSet($set)
    {
        if (!isset($set['fk_meta'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['fk_empresa'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['fk_funcionario'])) {
            throw new \Exception("O fk_funcionario é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['ano'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['mes'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['semana'])) {
            throw new \Exception("O fk_meta é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['valor']) && !isset($set['ponto'])) {
            throw new \Exception("O valor ou ponto é obrigatório ".get_class($this)."::formatSet().");
        }

        if (!isset($set['salario']) && !isset($set['salario'])) {
            throw new \Exception("O salario é obrigatório ".get_class($this)."::formatSet().");
        }

        // Reotrna o set com as altreraÃ§Ãµes necessÃ¡rias
        return $set;
    }

    /**
     * @return \Application\Models\Avaliacoes
     */
    protected function _getAvaliacoes()
    {
        return $this->_getLoader()->getModel('\Application\Models\Avaliacoes');
    }

    /**
     * @return \Application\Models\Meta
     */
    protected function _getMeta()
    {
        return $this->_getLoader()->getModel('\Application\Models\Meta');
    }


    /**
     * @param Array $set do set do usuário
     * @return String 'valor' 'ponto'
     */
    protected function getUseValorOuPonto($set)
    {
        //Se não tem nenhum retorna erro
        if (!isset($set['valor']) && !isset($set['ponto'])) {
            throw new \Exception("O valor ou ponto é obrigatório ".get_class($this)."::getUseValorOuPonto().");
        }

        //Se tem os dois preenchidos com valores diferentes de Zero ou Null da erro
        if (isset($set['valor']) && !empty($set['valor']) && isset($set['ponto']) && !empty($set['ponto'])) {
            throw new \Exception("Erro ao identificar, favor enviar somente valor ou ponto ".get_class($this)."::getUseValorOuPonto().");
        }

        //Se tem valor preenchido mas nao tem ponto
        if (isset($set['valor']) && !empty($set['valor']) && empty($set['ponto'])) {
           $return = self::VALOR;

       //Se tem ponto preenchido mas nao tem valor
        } else {
           $return = self::PONTO;
        }

        return $return;
    }
}