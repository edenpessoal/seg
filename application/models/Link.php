<?php
namespace Application\Models;

use MY_Model;
 defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends MY_Model {

    public $table = 'link';
    public $primary_key = 'id_link';

    public function __construct()
    {
        $this->return_as = 'object';
        parent::__construct();
    }

    public function getMenuLinks(){

        $query = $this->db->query("SELECT id_link, valor, id_bloco, imagem FROM link ORDER BY ordem ASC");
        return $query->result_array();

    }

    public function getMenuSublinkByIdLink($idLink){

        $query = $this->db->query("SELECT id_sublink, url, link, imagem FROM sublink WHERE isApi = 0 AND fk_link = " . $idLink);
        return $query->result_array();

    }

}