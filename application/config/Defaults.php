<?php

class Defaults {

    public static $defaultImagem = "/assets/img/menu/eu.png";

    public static function getShowMenuScript($idItem){

        if(!empty($idItem) && !is_null($idItem)) {
            return "
                <script>
                $('{$idItem}').collapse('show');
                </script>
            ";
        }

        return "";

    }

}
