<?php

    function formataData($data)
    {
        if(empty($data))
            return "";

        $data = date("Y-m-d", strtotime($data));
        return $data;
    }

    function formataDataParaTela($data){

        if(empty($data))
            return "";

        $data = date("d-m-Y", strtotime($data));
        return $data;
    }