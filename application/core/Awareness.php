<?php

namespace Application\Core;

abstract class Awareness
{
	/**
	 * @var \Application\Core\Loader
	 */
	private $_loader;
	/**
	 * Retorna o App_Loader a ser usado
	 *
	 * @return \Application\Core\Loader
	 */
	public function getLoader()
	{
		if (! isset($this->_loader)) {
			$this->setLoader(new \Application\Core\Loader());
		}
		return $this->_loader;
	}
	/**
	 * Grava o App_Loader que deve ser usado
	 * Ele é usado com DI durante a criação do model no App_Loader
	 *
	 * @param \Application\Core\Loader $loader
	 */
	public function setLoader($loader)
	{
		$this->_loader = $loader;
	}
}