<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/config/Defaults.php';

    abstract class My_Controller extends REST_Controller {

        protected $userId = false;
        protected $userMenu = "";

        function __construct() {
            parent::__construct();

            $this->load->library('tank_auth');

//        if (!$this->tank_auth->is_logged_in() || !$this->hasPermission())
//        {
//            redirect('/');
//        }
//        else
//        {
            $this->userId = $this->tank_auth->get_user_id();

//            $this->userMenu = $this->load->view('menu/' . $this->tank_auth->get_user_menu(),
//                '',
//                true
//            );
//        }


            $this->userMenu = $this->menu();
        }

        protected function menu() {

            $this->load->model("linkModel");
            $menu = $this->linkModel->getMenuLinks();

            $content = $bloco = '';

            $pagina_atual = $this->uri->segment(1, 0) . '/' . $this->uri->segment(2, 0);

            foreach ($menu as &$posicao) {

                $contentSublink = "";
                $sublinks = $this->linkModel->getMenuSublinkByIdLinkAndIdUser($posicao['id'], $this->userId);

                if(count($sublinks) <= 0)
                {
                    unset($posicao);
                    continue;
                }

                foreach ($sublinks as $sublink) {

                    if(!is_null($sublink['imagem']) && !empty($sublink['imagem'])){
                        $imagem = $sublink['imagem'];
                    }else{
                        $imagem = Defaults::$defaultImagem;
                    }

                    if($this->removeTrainlingSlash($sublink['url']) == $pagina_atual){
                        $bloco = $posicao['id_bloco'];
                    }

                    $contentSublink .= $this->load->view('menu/menu_sublink',
                        array('sublink' => $this->removeTrainlingSlash($sublink['url']), 'texto_sublink' => $sublink['link'], 'imagem' => $imagem),
                        true
                    );

                }

                $content .= $this->load->view('menu/menu_categoria_bloco',
                    array('link' => $posicao['id_bloco'],
                        'valor' => $posicao['valor'],
                        'id_bloco' => str_replace("#", "", $posicao['id_bloco']),
                        'bloco' => $contentSublink,
                        'imagem' => $posicao['imagem'],
                        'scriptAbreMenuAtualPosicao' => Defaults::getShowMenuScript($bloco)
                    ),
                    true
                );

            }

            return $content;

        }

        protected function removeTrainlingSlash($string){

            if($string{0} == "/"){
                $string = substr($string, 1);
            }

            return $string;

        }

        protected function hasPermission() {
            $controller = $this->router->fetch_class();
            $method = $this->router->fetch_method();

            return $this->tank_auth->hasPermission($controller, $method);
        }

        protected function issetOrEmpty($vetor, $key) {

            if (is_array($vetor)) {
                if (isset($vetor[ $key ])) {
                    return $vetor[ $key ];
                } else {
                    return "";
                }
            } elseif (is_object($vetor)) {
                if (isset($vetor->$key)) {
                    return $vetor->$key;
                } else {
                    return "";
                }
            }

            return "";
        }
    }
