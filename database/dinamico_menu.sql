-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.7.11 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para mpweb
CREATE DATABASE IF NOT EXISTS `mpweb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mpweb`;


-- Copiando estrutura para tabela mpweb.menu_dinamico
CREATE TABLE IF NOT EXISTS `menu_dinamico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_tipo` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uri',
  `id_pagina` int(11) NOT NULL DEFAULT '0',
  `modulo_nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_menu_grupo` int(11) NOT NULL DEFAULT '0',
  `posicao` int(5) NOT NULL DEFAULT '0',
  `target` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_filho` int(11) NOT NULL DEFAULT '0',
  `menu_pai` tinyint(1) NOT NULL DEFAULT '0',
  `visibilidade` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_menu_grupo - normal` (`id_menu_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela mpweb.menu_dinamico: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_dinamico` DISABLE KEYS */;
INSERT INTO `menu_dinamico` (`id`, `titulo`, `link_tipo`, `id_pagina`, `modulo_nome`, `url`, `uri`, `id_menu_grupo`, `posicao`, `target`, `menu_filho`, `menu_pai`, `visibilidade`) VALUES
	(1, 'Mural', 'page', 1, '', '/admin/mural', '', 1, 0, '', 0, 1, '1'),
	(2, 'Mensagens', 'page', 2, '', '/admin/mensagem', '', 1, 0, '', 0, 0, '1'),
	(3, 'Tarefas', 'page', 3, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(4, 'Resultados', 'page', 4, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(5, 'Recompensas', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(6, 'Proventos', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(7, 'Bonus/PRL', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(8, 'Promocao', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(9, 'Acoes', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(11, 'Stock Option', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(12, 'Avaliacoes', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(13, 'configuracoes', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(14, 'Rankings', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(15, 'Valuation', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1'),
	(16, 'regras', 'page', 0, '', '/admin/', '', 1, 0, '', 0, 0, '1');
/*!40000 ALTER TABLE `menu_dinamico` ENABLE KEYS */;


-- Copiando estrutura para tabela mpweb.menu_grupo
CREATE TABLE IF NOT EXISTS `menu_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `abreviacao` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='agrupamentos de navegação. Por exemplo, cabeçalho, barra lateral, rodapé, etc';

-- Copiando dados para a tabela mpweb.menu_grupo: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_grupo` DISABLE KEYS */;
INSERT INTO `menu_grupo` (`id`, `titulo`, `abreviacao`) VALUES
	(1, 'Admin', 'admin'),
	(2, 'Vendas', 'vendas'),
	(3, 'Demais Areas', 'nvendas'),
	(4, 'Supervisor', 'supervisor');
/*!40000 ALTER TABLE `menu_grupo` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
