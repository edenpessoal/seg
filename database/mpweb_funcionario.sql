-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.7.11 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para mpweb
CREATE DATABASE IF NOT EXISTS `mpweb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mpweb`;


-- Copiando estrutura para tabela mpweb.funcionario
CREATE TABLE IF NOT EXISTS `funcionario` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `id_empresa` int(6) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `salario` varchar(12) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `setor` varchar(255) NOT NULL,
  `supervisor` int(9) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `departamento` varchar(70) DEFAULT NULL,
  `foto_perfil` varchar(250) DEFAULT NULL,
  `foto_capa` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mpweb.funcionario: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `funcionario` DISABLE KEYS */;
INSERT INTO `funcionario` (`id`, `users_id`, `id_empresa`, `nome`, `email`, `salario`, `cargo`, `setor`, `supervisor`, `telefone`, `departamento`, `foto_perfil`, `foto_capa`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(7, 8, 26, 'Michelle Pacheco', 'michelle@teste.com', '5800', '', 'Outros', NULL, '21980839698', 'Analista de Rede', '/assets/empresas/imagens/26/michelle.jpg', NULL, '2016-07-18 21:05:00', '2016-07-18 21:05:00', '2016-07-18 21:05:00'),
	(14, 15, 26, 'Michelle Pacheco3', 'michelle3@teste.com', '5800', '', 'Outros', NULL, NULL, NULL, NULL, NULL, '2016-08-15 19:19:56', '2016-08-15 19:19:56', '2016-08-15 19:19:56');
/*!40000 ALTER TABLE `funcionario` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
