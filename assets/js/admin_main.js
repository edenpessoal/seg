$(document).ready(function() {

    $("#cadastar_funcionario").click( function() {
        var $myForm = $('#formCadastroFuncionario');
        if (!$myForm[0].checkValidity()) {
            $myForm.submit();
        }else{


            $.post(
                '/admin/add_funcionario',
                $myForm.serialize(),
                function(response)
                {
                    if(response.error){
                        alert(response.error);
                    }else{
                        //$("#myModal").close();
                        $(location).attr('href', '/admin/funcionarios')
                    }
                },
                'json');

            return false;

        }
    });

});

function excluir_funcionario(idFuncionario){

    $.post(
        '/admin/delete_funcionario/' +idFuncionario,
        '',
        function(response)
        {
            if(response.error){
                alert(response.error);
            }else{
                //$("#myModal").close();
                $(location).attr('href', '/admin/funcionarios')
            }
        },
        'json');

}