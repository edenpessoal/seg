$(document).ready(function() {
    $("#envio_cadastro").click( function() {
        var $myForm = $('#formCadastroBasico');
        if (!$myForm[0].checkValidity()) {
            $myForm.submit();
        }else{
            var $btn = $(this);
            $btn.button('loading');

            $.post(
                $myForm.attr('action'),
                $myForm.serialize(),
                function (response) {

                    if(response.errors){
                        for(var error in response.errors){
                            alert(response.errors[error]);
                        }
                    }else{
                        document.location = "/admin/";
                    }

                    $btn.button('reset');
                },
                'json'
            );

            return false;
        }
    });

    $("#loginSubmit").click( function() {
        var $myForm = $('#formLogin');
        if (!$myForm[0].checkValidity()) {
            $myForm.submit();
        }else{
            var $btn = $(this);
            $btn.button('loading');

            $.post(
                $myForm.attr('action'),
                $myForm.serialize(),
                function (response) {

                    if(response.errors){
                        for(var error in response.errors){
                            alert(response.errors[error]);
                        }
                    }else{
                        document.location = "/admin/";
                    }

                    $btn.button('reset');
                },
                'json'
            );

            return false;
        }
    });

    $("#forgotPasswordModalSend").click(function(e)
    {
        var $myForm = $('#formForgotPassword');
        if (!$myForm[0].checkValidity()) {
            $myForm.submit();
        }else{
            e.preventDefault();

            var $btn = $(this);
            $btn.button('loading');

            $.post(
                '/login_cadastro/forgot_password',
                $myForm.serialize(),
                function (response) {

                    if(response.errors){
                        alert(response.errors);
                    }else{
                        alert("Foi enviado um e-mail para o seu endereço com as instruções para recuperar sua senha.");
                    }

                    $btn.button('reset');
                },
                'json'
            );

            return false;
        }
    });

    var $resetPassModal = $("#reset_password_modal");
    if($resetPassModal.length > 0){
        $resetPassModal.modal('toggle');
        $("#forgotPasswordResetSend").off('click').click(function(e){

            var $myForm = $('#formResetPassword');
            if (!$myForm[0].checkValidity()) {
                $myForm.submit();
            }else{
                e.preventDefault();

                var $btn = $(this);
                $btn.button('loading');

                $.post(
                    document.location,
                    $myForm.serialize(),
                    function (response)
                    {
                        $btn.button('reset');

                        if(response.errors){
                            alert(response.errors);
                        }else{
                            alert("Senha alterada com sucesso!");
                            document.location = "/admin/";
                        }
                    },
                    'json'
                );

                return false;
            }

        });
    }
});
