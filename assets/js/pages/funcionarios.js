var funcionarios = {

    table: null,
    buttonAdd: null,
    funcionarioArray: [],
    editForm: null,

    get_funcionarios: function()
    {
        $.get(
            '/api/funcionariosAPI/getFuncionarios',
            '',
            function (response)
            {
                if(!response.errors)
                {
                    funcionarios.mountDatatable(response.data);

                    funcionarios.funcionarioArray = [];
                    // correspondencia de chaves na resposta
                    // 0=id; 1=setor; 2=nome; 3=cargo; 4=salario; 5=email
                    for(var id in response.data)
                    {
                        var temp = response.data[id];
                        funcionarios.funcionarioArray.push(
                            new funcionario(temp[0], temp[2])
                        );
                    }

                    var modal = $("#myModalAddFuncionario");
                    var supervisores = modal.find('#supervisor');
                    supervisores.empty().append($('<option>', {
                        value: '',
                        text: 'Nenhum'
                    }));

                    $.each(funcionarios.funcionarioArray, function (i, item) {
                        supervisores.append($('<option>', {
                            value: item.id,
                            text: item.nome,
                            selected: false
                        }));
                    });
                }
            },
            'json'
        );
    },

    delete: function(id_funcionario){

        $.post(
            '/api/funcionariosAPI/deleteFuncionario',
            {
                funcionarioId: id_funcionario
            },
            function (response) {
                funcionarios.table.destroy();
                funcionarios.get_funcionarios();
            },
            'json'
        );

    },

    openModalChangePassword: function(idFuncionario)
    {
        var password_modal = $("#password_modal");
        password_modal.modal('toggle');

        password_modal.find("#passwordModalSave").off('click').click( function(e) {

            var $myForm = $('#formChangePassword');

            if($myForm.find("#password").val() != $myForm.find("#confirm_password").val())
            {
                alert('Os campos SENHA e CONFIRMAÇÃO DE SENHA precisam ser iguais!');
            }
            else
            {
                password_modal.modal('toggle');
                funcionarios.changePassword(idFuncionario, $myForm);
            }

        });
    },

    changePassword: function(idFuncionario, form)
    {
        var data = form.serializeArray();
        data.push({name: "idFuncionario", value: idFuncionario});

        $.post(
            '/api/funcionariosAPI/changePassword',
            $.param(data),

            function (response) {
                if(response.errors)
                {
                    alert(response.errors);
                    funcionarios.openModalChangePassword(idFuncionario);
                }
            },
            'json'
        );
    },

    testeIsAdmin: function(form, method)
    {
        // if(form.find("#isAdmin").val() == 1)
        // {
        //     funcionarios.buttonAdd.button('reset');
        //     var password_modal = $("#password_modal");
        //     password_modal.modal('toggle');
        //
        //     password_modal.find("#passwordModalSave").off('click').click( function(e) {
        //
        //         var $myForm = $('#formChangePassword');
        //
        //         if($myForm.find("#password").val() != $myForm.find("#confirm_password").val())
        //         {
        //             alert('Os campos SENHA e CONFIRMAÇÃO DE SENHA precisam ser iguais!');
        //         }
        //         else
        //         {
        //             funcionarios.buttonAdd.button('loading');
        //             password_modal.modal('toggle');
        //             var formsSerialized = form.serialize() + "&" + $myForm.serialize();
        //             if(method == "add")
        //                 funcionarios.add(formsSerialized);
        //             else
        //                 funcionarios.save(formsSerialized);
        //         }
        //
        //     });
        //
        // }else{
        //     if(method == "add")
        //         funcionarios.add(form.serialize());
        //     else
        //         funcionarios.save(form.serialize());
        // }

        if(method == "add")
            funcionarios.add(form.serialize());
        else
            funcionarios.save(form.serialize());

        funcionarios.editForm=null;
    },

    add: function(formSerialized){

        $.post(
            '/api/funcionariosAPI/addFuncionario',
            formSerialized,
            function (response) {

                funcionarios.buttonAdd.button('reset');

                if(response.errors){
                    alert(response.errors);
                }else{
                    var table = $('#funcionariosDataTable').DataTable();
                    table.rows.add(response).draw();
                    var modal = $("#myModalAddFuncionario");
                    modal.find('form')[0].reset();
                    modal.modal('toggle');

                    var temp = response[0];
                    funcionarios.funcionarioArray.push(
                        new funcionario(temp[0], temp[2])
                    );

                    var supervisores = modal.find('#supervisor');
                    supervisores.empty().append($('<option>', {
                        value: '',
                        text: 'Nenhum'
                    }));

                    $.each(funcionarios.funcionarioArray, function (i, item) {
                        supervisores.append($('<option>', {
                            value: item.id,
                            text: item.nome,
                            selected: false
                        }));
                    });
                }

                page.get_percentage();
            },
            'json'
        );

    },

    edit: function(id_funcionario){

        $.get(
            '/api/funcionariosAPI/getFuncionarioById',
            {
                funcionarioId: id_funcionario
            },
            function (response) {

                if(!response.errors)
                {
                    var supervisor = response[0].f_supervisor;

                    var modal = $("#myModalEditFuncionario");
                    page.monta_camada('myModalEditFuncionario', response[0]);

                    var supervisores = modal.find('#supervisor');
                    supervisores.empty().append($('<option>', {
                        value: '',
                        text: 'Nenhum'
                    }));

                    $.each(funcionarios.funcionarioArray, function (i, item) {
                        if(item.id != id_funcionario) {

                            var selected = (supervisor == item.id);

                            supervisores.append($('<option>', {
                                value: item.id,
                                text: item.nome,
                                selected: selected
                            }));
                        }
                    });

                    if(response[0].admin == "SIM")
                        modal.find("#isAdmin").val("1");

                    modal.modal('toggle');

                }
            },
            'json'
        );
    },

    save: function(formSerialized){

        $.post(
            '/api/funcionariosAPI/editFuncionario',
            formSerialized,
            function (response) {

                funcionarios.buttonAdd.button('reset');

                if(response.errors){
                    alert(response.errors);
                }
                else if(response.message && response.message == "NEEDS_PASSWORD")
                {
                    funcionarios.testeIsAdmin(
                        funcionarios.editForm,
                        'save'
                    );
                }
                else
                {
                    funcionarios.table.destroy();
                    funcionarios.get_funcionarios();
                    var modal = $("#myModalEditFuncionario");
                    modal.modal('toggle');
                }
            },
            'json'
        );

    },

    importCsvVendas: function(form){

        $.ajax( {
            url: '/api/funcionariosAPI/importCsvVendas',
            type: 'POST',
            data:  form ,
            processData: false,
            contentType: false
        } ).done(function( result ) {

            if(result.errors)
                alert(result.errors[0]);
            else
            {
                $('#myModal2').modal('hide');
                var $modalPreview = $("#myModalImportFuncionarios");
                $modalPreview.find("#tabela_preview").html( result.preview );
                $modalPreview.find("#import_ok").off().click( function(e){
                	$.get(
            			'/api/funcionariosAPI/importCsvFinalize',
                        {},
                        function (response) {

                            if(!response.errors)
                            {
                            	funcionarios.table.destroy();
                        		funcionarios.get_funcionarios();
                        		alert('Importação realizada com sucesso!');
                        		$modalPreview.modal('hide');
                            }
                        },
                        'json'
                    );
                });
                $modalPreview.modal('show');
            }

        });

    },

    importCsvOutros: function(form){

        $.ajax( {
            url: '/api/funcionariosAPI/importCsvOutros',
            type: 'POST',
            data:  form ,
            processData: false,
            contentType: false
        } ).done(function( result ) {

            if(result.errors)
                alert(result.errors[0]);
            else
            {
            	$('#myModal3').modal('hide');
                var $modalPreview = $("#myModalImportFuncionarios");
                $modalPreview.find("#tabela_preview").html( result.preview );
                $modalPreview.find("#import_ok").off().click( function(e){
                	$.get(
            			'/api/funcionariosAPI/importCsvFinalize',
                        {},
                        function (response) {

                            if(!response.errors)
                            {
                            	funcionarios.table.destroy();
                        		funcionarios.get_funcionarios();
                        		alert('Importação realizada com sucesso!');
                        		$modalPreview.modal('hide');
                            }
                        },
                        'json'
                    );
                });
                $modalPreview.modal('show');
            }

        });

    },

    mountDatatable: function(data){

        // 0=id; 1=setor; 2=nome; 3=cargo; 4=salario; 5=email
        funcionarios.table = $('#funcionariosDataTable').DataTable( {
            //"ajax": '/api/funcionariosAPI/getFuncionarios',
            "data": data,
            "columns":[
                // {"data":0},
                {"data":2},
                {"data":1},
                // {"data":3},
                {"data":4},
                {"data":5},
                {"data":7},
                // {"data":8},
                {
                    sortable: false,
                    "render": function ( data, type, full, meta ) {
                        var reg_id = full[0];
                        var ret = '<a onclick="funcionarios.edit('+reg_id+')" class="btn btn-info resetBtn buttonEdit" role="button">Editar</a>' +
                            '<a onclick="funcionarios.delete('+reg_id+')" class="btn btn-danger resetBtn buttonDelete" role="button">Deletar</a>';

                        if(data == "SIM")
                            ret += '<a onclick="funcionarios.openModalChangePassword('+reg_id+')" class="btn btn-warning resetBtn buttonEdit" role="button">Alterar Senha</a>';

                        return ret;
                    }
                }
            ]

        } );

    },

    get_groups: function(){

        var callback = function (response)
        {
            if(!response.errors)
            {
                var $myForm = $('#formCadastroFuncionario');

                $.each(response.data, function (i, item)
                {
                    $myForm.find("#id_group").append($('<option>', {
                        value: item[0],
                        text: item[1]
                    }));
                });
            }
        };

        groups.get_groups(callback);

    },
};

var funcionario = function(id, nome) {
    this.id = id;
    this.nome = nome;  // Chamando um método que está fora do prototype da classe
};

$(function() {

    //funcionarios.mountDatatable();
    funcionarios.get_funcionarios();
    funcionarios.get_groups();

    //$('#funcionariosDataTable tbody').on( 'click', '.buttonDelete', function () {
    //    funcionarios.table
    //        .row( $(this).parents('tr') )
    //        .remove()
    //        .draw(false);
    //} );


    $("#cadastar_funcionario").click( function(e) {

        var $myForm = $('#formCadastroFuncionario');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            e.preventDefault();

            funcionarios.buttonAdd = $(this);
            funcionarios.buttonAdd.button('loading');
            funcionarios.testeIsAdmin($myForm, 'add');
            return false;
        }
    });

    $("#editar_funcionario").click( function(e) {

        var $myForm = $('#formEditarFuncionario');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            e.preventDefault();

            funcionarios.buttonAdd = $(this);
            funcionarios.buttonAdd.button('loading');
            funcionarios.editForm = $myForm;
            funcionarios.save($myForm.serialize());
            return false;
        }
    });

    $("#outrosImportSubmit").click( function(e) {

        var $myForm = $('#formUploadOutros');

        var formData = new FormData();
        formData.append('file', $('input[type=file]')[1].files[0] );

        funcionarios.importCsvOutros(formData);


    });

    $("#vendasImportSubmit").click( function(e) {

        var $myForm = $('#formUploadVendas');

        var formData = new FormData();
        formData.append('file', $('input[type=file]')[0].files[0] );

        funcionarios.importCsvVendas(formData);


    });
    
});