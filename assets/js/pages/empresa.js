var empresa = {

    button: null,
    id_tb_cidades: null,
    id_tb_estados: null,

    get_empresa: function()
    {
        $.get(
            '/api/empresaAPI/getEmpresa',
            '',
            function (response) {
                if(!response.errors)
                {
                    empresa.id_tb_cidades = response.id_tb_cidades;
                    empresa.id_tb_estados = response.id_tb_estados;

                    page.monta_pagina(response);
                }

                empresa.monta_endereco();
                empresa.mask_inputs();
            },
            'json'
        );
    },

    update_empresa: function(form){

        $.post(
            '/api/empresaAPI/updateEmpresa',
            form.serialize(),
            function (response) {

                empresa.button.button('reset');

                if(response.errors){
                    alert(response.errors);
                }

                page.get_percentage();
            },
            'json'
        );

    },

    monta_endereco: function(){

        page.get_estados(function(dados){

            var option = '<option value="">Selecione o Estado</option>';
            $.each(dados, function(i, obj){
                var selected = "";
                if(obj.id == empresa.id_tb_estados){
                    selected = "selected";
                }else{
                    selected = "";
                }
                option += '<option value="'+obj.id+'" '+selected+'>'+obj.nome+'</option>';
            });

            $('#id_tb_estados').html(option)
                .change(function(e) {

                    var estado = $(this).val();

                    page.get_cidades(estado, function (dados)
                    {
                        var option = '<option value="">Selecione a Cidade</option>';

                        $.each(dados, function(i, obj){

                            if(obj.id == empresa.id_tb_cidades)
                            {
                                selected = "selected";
                            }
                            else
                            {
                                selected = "";
                            }

                            option += '<option value="'+obj.id+'" '+ selected +'>'+obj.nome+'</option>';
                        });

                        $('#id_tb_cidades').html(option).prop('disabled', false);
                    });
                });

        });

        if(empresa.id_tb_cidades != null){

            var estado = empresa.id_tb_estados;

            page.get_cidades(estado, function (dados)
            {
                var option = '<option value="">Selecione a Cidade</option>';

                $.each(dados, function(i, obj){

                    if(obj.id == empresa.id_tb_cidades)
                    {
                        selected = "selected";
                    }
                    else
                    {
                        selected = "";
                    }

                    option += '<option value="'+obj.id+'" '+ selected +'>'+obj.nome+'</option>';
                });

                $('#id_tb_cidades').html(option).prop('disabled', false);
            });
        }

    },

    mask_inputs: function(){
        $("#cnpj").mask("99.999.999/9999-99");
        $("#cep").mask("99999-999");
    }
};


$(function() {

    empresa.get_empresa();

    $("#empresaSubmit").click( function(e) {

        var $myForm = $('#formEmpresa');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            empresa.button = $(this);
            empresa.button.button('loading');
            empresa.update_empresa($myForm);
            e.preventDefault();
            return false;
        }
    });
});