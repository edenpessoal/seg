var page = {

    get_percentage: function(){
        $.get(
            '/api/utilsAPI/getPercentage',
            '',
            function(response){
                // alert(response.percentual + '% até o momento.\nEste valor tem que entrar na barra que indica o percentual!');
            },
            'json'
        );
    },

    monta_pagina: function (data)
    {
        for(key in data)
        {
            if(key != 'id_tb_cidades' && key != 'id_tb_estados') {
                $("#page_content")
                    .find("#" + key).val(data[key])
                    .find("#" + key).html(data[key]);
            }
        }

        page.get_percentage();
    },

    monta_camada: function (camada, data)
    {
        for(key in data)
        {
            if(key != 'id_tb_cidades' && key != 'id_tb_estados') {
                $("#" + camada)
                    .find("#" + key).val(data[key])
                    .find("#" + key).html(data[key]);
            }
        }

        page.get_percentage();
    },

    monta_camada_custom: function(attr, data)
    {
        for(key in data)
        {
            $("#page_content").find(('input['+attr+' = "'+ key +'"]'))
                .val(data[key])
                .html(data[key]);
        }

        page.get_percentage();

    },

    get_estados: function(callback)
    {
        $.get(
            '/api/utilsAPI/getEstados',
            '',
            callback,
            'json'
        );
    },

    get_cidades: function(id_uf, callback)
    {
        $.get(
            '/api/utilsAPI/getCidades/idUf/' + id_uf,
            '',
            callback,
            'json'
        );
    }

};

$(function() {

    page.get_percentage();


    $("form").each(function() {
        $(this).submit(function(e){
            return false;
        })
    });


});