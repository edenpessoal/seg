function Chart(container, series=[]){
  var chart = new Highcharts.Chart({
                 chart:{
                   renderTo:container,
                   type:"areaspline"
                 },
                 xAxis: {
                     categories: x_series
                 },
                 series:[{
                   name: "Meta",
                   draggableY: true,
                   dragMinY: 0
                 }],
                 plotOptions: {
                   series:{
                     point:{
                       events:{
                         drag:function(e){
                           handleDragAndDrop(e, "drag");
                         },
                         drop:function(){
                           handleDragAndDrop(this, "drop");
                         }
                       }
                     }
                   }
                 },
                 line: {
                   cursor: 'ns-resize'
                 }
               });
  return chart;
}
GOAL=120000;

var x_series = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var map = [], last_index = -1, is_dirty = false;

function getWeekNumber ()
{
  var d = new Date(+this);
  d.setHours(0,0,0);
  d.setDate(d.getDate()+4-(d.getDay()||7));
  return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
}

function generateSeriesByGoal(goal, timeframe){
  var newSeries = [];
  var metaDividida = goal / timeframe;
  for(var i = 0; i < timeframe; i++){
    newSeries[i] = metaDividida;
  }

  return newSeries;
}

function recalculo(index, newValue, valorDesconto) {
  chart = jQuery("#container_grafico").highcharts();
  points = chart.series[0].points;

  for(var i = 0; i < points.length; i++){
    if(i != index){
      chart.series[0].data[i].update(points[i].y + valorDesconto);
    }
  }

}

var History = {};
History.old_y = -100;

function handleDragAndDrop(e, status){
  if(status == "drag"){
    History.old_y = e.dragStart.y;
  }else{
    var delta = History.old_y - e.y;
    recalculo(e.index, e.y, delta);
  }
}


