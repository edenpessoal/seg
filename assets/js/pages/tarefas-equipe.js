var tarefas = {

    table: null,
    buttonAdd: null,

    add: function(form){

        $.post(
            '/api/tarefasAPI/addTarefa',
            form.serialize(),
            function (response) {

                tarefas.buttonAdd.button('reset');
                if(response.errors){
                    alert(response.errors);
                }else{
                    tarefas.get_tarefas();
                    $('#AdicionarTarefa').modal('toggle');
                    form.find("#tarefa").val("");
                }
            },
            'json'
        );

    },

    save: function(form){

        $.post(
            '/api/tarefasAPI/updateTarefa',
            form.serialize(),
            function (response) {

                tarefas.buttonAdd.button('reset');
                if(response.errors){
                    alert(response.errors);
                }else{
                    tarefas.get_tarefas();
                    $('#EditarTarefa').modal('toggle');
                    form.find("#tarefa").val("");
                }
            },
            'json'
        );

    },

    finaliza: function(id_tarefa){

        $.post(
            '/api/tarefasAPI/finalizaTarefa',
            {
                tarefaId: id_tarefa
            },
            function (response) {
                tarefas.get_tarefas();
            },
            'json'
        );

    },

    edit: function(id_tarefa){

        $.get(
            '/api/tarefasAPI/getTarefa',
            {
                tarefaId: id_tarefa
            },
            function (response) {

                var $modal = $("#EditarTarefa");
                $modal.find("#id").val( response.id );
                $modal.find("#tarefa").val( response.tarefa );
                $modal.find("#date_task").html( response.data_abertura );
            },
            'json'
        );

    },

    delete: function(id_tarefa){

        $.post(
            '/api/tarefasAPI/deleteTarefa',
            {
                tarefaId: id_tarefa
            },
            function (response) {
                tarefas.get_tarefas();
            },
            'json'
        );

    },

    get_subordinadosteste: function()
    {
        $.get(
            '/api/tarefasAPI/getMeusSubordinados',
            '',
            function (response)
            {
                if(!response.errors)
                {
                    var div = $("#listasubordinadosteste");
                    var subordinados = div.find('#id_funcionarioteste');
                    subordinados.empty().append($('<td class="testando">', {
                        id: 'primeiro',
                        text: 'Meus subordinados'
                    }));

                    $.each(response, function (i, item) {
                        subordinados.append($('<td>', {
                            id: item.id,
                            text: item.nome
                        }));
                    });
                }
            },
            'json'
        );
    },


    get_subordinados: function()
    {
        $.get(
            '/api/tarefasAPI/getMeusSubordinados',
            '',
            function (response)
            {
                if(!response.errors)
                {
                    var modal = $("#myModalAddTarefa");
                    var subordinados = modal.find('#id_funcionario');
                    subordinados.empty().append($('<option>', {
                        value: '',
                        text: 'Eu mesmo'
                    }));

                    $.each(response, function (i, item) {
                        subordinados.append($('<option>', {
                            value: item.id,
                            text: item.nome,
                            selected: false
                        }));
                    });
                }
            },
            'json'
        );
    },


    get_tarefas: function()
    {
        $.get(
            '/api/tarefasAPI/getTarefas',
            '',
            function (response)
            {
                if(!response.errors)
                {
                    var $table = $("#tarefas_table");
                    //$table.not(".hide").remove();
                    $table.find('tr:not(.hide)').remove();

                    for(var tarefa in response)
                    {
                        var $clone = $("#tarefa_pendente").clone();
                        $clone.prop("id", 'linha_' + response[tarefa].id);
                        $table.prepend($clone);

                        var $model = $("#linha_" + response[tarefa].id);

                        var details = $model.find("#task_details");
                        var checkbox = $model.find("#success_taskAberto");

                        if(response[tarefa].status == 'Finalizado')
                        {
                            checkbox.attr('checked', true);
                        }

                        checkbox.prop('id', 'check_' + response[tarefa].id);
                        checkbox.prop('name', 'check_' + response[tarefa].id);

                        $('#check_' + response[tarefa].id).change(function(e){
                            tarefas.finaliza( response[tarefa].id );
                        });

                        var delete_task = $model.find('#delete_task');
                        delete_task.click( function(e){
                            tarefas.delete(response[tarefa].id);
                        });

                        var success_task = $model.find('#success_task');
                        success_task.click( function(e){
                            tarefas.finaliza(response[tarefa].id);
                        });

                        var edit_task = $model.find('#edit_task');
                        edit_task.click( function(e){
                            tarefas.edit(response[tarefa].id);
                        });

                        details.html( response[tarefa].tarefa );
                        $model.removeClass('hide');

                    }
                }
            },
            'json'
        );
    },

    addComment: function(data){
        $.post(
            '/api/tarefasAPI/addComment',
            formSerialized,
            function (response) {
                if(response.errors){
                    alert(response.errors);
                }
            },
            'json'
        );
    }
};

$(function() {
    tarefas.get_subordinadosteste();
    tarefas.get_subordinados();
    tarefas.get_tarefas();

    $("#cadastrar_tarefa").click( function(e) {

        var $myForm = $('#formCadastroTarefa');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            e.preventDefault();

            tarefas.buttonAdd = $(this);
            tarefas.buttonAdd.button('loading');
            tarefas.add($myForm);
            return false;
        }
    });

    $("#editar_tarefa").click( function(e) {

        var $myForm = $('#formEditarTarefa');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            e.preventDefault();

            tarefas.buttonAdd = $(this);
            tarefas.buttonAdd.button('loading');
            tarefas.save($myForm);
            return false;
        }
    });

    $('.star').on('click', function () {
        $(this).toggleClass('star-checked');
    });

    $('.ckbox label').on('click', function () {
        $(this).parents('tr').toggleClass('selected');
    });

    $('.btn-filter').on('click', function () {
        var $target = $(this).data('target');
        if ($target != 'all') {
            $('.table tr').css('display', 'none');
            $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
        } else {
            $('.table tr').css('display', 'none').fadeIn('slow');
        }
    });
});
