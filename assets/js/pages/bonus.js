var bonus = {

    buttonSave: null,

    save: function(form){
        $.post(
            '/api/bonusAPI/upsertBonus',
            form.serialize(),
            function (response) {

                bonus.buttonSave.button('reset');

                if(response.errors){
                    alert(response.errors);
                }

                page.get_percentage();
            },
            'json'
        );
    },

    getBonus: function(){
        $.get(
            '/api/bonusAPI/getBonus',
            '',
            function (response) {
                if(!response.errors)
                {
                    page.monta_pagina(response);
                    $('input:radio[name="socios_participam"]').filter('[value="' +response['socios_participam']+ '"]').attr('checked', true);
                    $('input:radio[name="distribuicao"]').filter('[value="' +response['distribuicao']+ '"]').attr('checked', true);
                }
            },
            'json'
        );
    }
};

$(function() {

    bonus.getBonus();

    $("#saveBonus").click( function(e) {

        var $myForm = $('#formDadosBonus');

        bonus.buttonSave = $(this);
        bonus.buttonSave.button('loading');
        e.preventDefault();
        bonus.save($myForm);
        return false;
    });


});