var groups = {

    table: null,
    buttonAdd: null,

    get_groups: function( callback )
    {
        $.get(
            '/api/groupsAPI/getGroups',
            callback,
            'json'
        );

        groups.get_sublinks();
    },

    get_groups_mount_datatable: function(){

        var callback = function (response)
        {
            if(!response.errors)
            {
                groups.mountDatatable(response.data);
            }
        };

        groups.get_groups(callback);

    },

    get_sublinks: function()
    {
        $.get(
            '/api/groupsAPI/getSublinks',
            '',
            function (response)
            {
                if(!response.errors)
                {
                    groups.montaTabela(response);
                }
            },
            'json'
        );
    },

    montaTabela: function(response){
        var grupo = response[0][0];
        var linhas = "<tr>";

        for(var i=0; i < response.length; i++)
        {

            var checked = '';
            if(response[i][3]){
                checked = 'checked';
            }

            if(grupo != "" && grupo != response[i][0])
            {
                linhas += "</tr><tr>";
                grupo = response[i][0];
            }

            linhas += "<td><input type='checkbox' id='check" + i + "' name='permissoes[]' value='" + response[i][2] + "' " + checked + "> ";
            linhas += "<label for='check" + i + "'> " + response[i][0] + " >> " + response[i][1] + "</label></td>";
        }
        linhas += "</tr>";

        $("#tabelaNovoGrupo").find("> tbody").empty().append(linhas);
    },

    mountDatatable: function(data){

        groups.table = $('#groupsDataTable').DataTable( {
            "data": data,
            "columns":[
                {"data":0},
                {"data":1},
                {
                    sortable: true,
                    "render": function ( data, type, full, meta ) {
                        var reg_id = full[0];
                        return '<a onclick="groups.edit(' + reg_id + ')" class="btn btn-info resetBtn buttonEdit" role="button">Editar</a>' +
                            '<a onclick="groups.delete(' + reg_id + ')" class="btn btn-danger resetBtn buttonDelete" role="button">Deletar</a>';
                    }
                }
            ]

        } );

    },

    edit: function(id){

        var modal = $("#myModalAddGrupo");
        modal.find('form')[0].reset();
        modal.modal('toggle');

        $("#formCadastroGrupo").find("#idGroup").val(id);

        $.get(
            '/api/groupsAPI/getSublinksForEdit',
            {idGroup: id},
            function (response) {

                $("#tabelaNovoGrupoHeader").find("#nome").val(response['name']);
                groups.montaTabela(response['data']);

            },
            'json'
        );

    },

    delete: function(id){

        $.post(
            '/api/groupsAPI/deleteGroup',
            {idGroup: id},
            function (response) {

                if(response.errors) {
                    alert(response.errors);
                }else{
                    groups.table.destroy();
                    groups.get_groups_mount_datatable();
                }

            },
            'json'
        );

    },

    selectAllCheckboxes: function(element){
        $("#tabelaNovoGrupo").find("> tbody").find('input:checkbox').not(element).prop('checked', element.checked);
    },

    add: function(formSerialized){

        $.post(
            '/api/groupsAPI/addGroup',
            formSerialized,
            function (response) {

                groups.buttonAdd.button('reset');

                if(response.errors){
                    alert(response.errors);
                }else{

                    groups.table.destroy();
                    groups.get_groups_mount_datatable();

                    var modal = $("#myModalAddGrupo");
                    modal.find('form')[0].reset();
                    modal.modal('toggle');

                }
            },
            'json'
        );

    },

};

$(function() {
    groups.get_groups_mount_datatable();
    groups.get_sublinks();

    $("#selectAll").click(function(){
        groups.selectAllCheckboxes(this);
    });

    $("#cadastar_grupo").click( function(e) {

        var $myForm = $('#formCadastroGrupo');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            e.preventDefault();

            groups.buttonAdd = $(this);
            groups.buttonAdd.button('loading');
            groups.add($myForm.serialize());
            return false;
        }
    });
});