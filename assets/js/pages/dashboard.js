var dashboard = {

    button: null,

    get_perfil: function()
    {
        $.get(
            '/api/perfilAPI/getPerfil',
            '',
            function (response) {
                if(!response.errors){
                    page.monta_pagina(response);
                }
            },
            'json'
        );
    },

    update_perfil: function(form){

        $.post(
            '/api/perfilAPI/updatePerfil',
            form.serialize(),
            function (response) {

                dashboard.button.button('reset');

                if(response.errors){
                    alert(response.errors);
                }

                page.get_percentage();
            },
            'json'
        );
    },

    change_password: function(form){

        $.post(
            '/api/perfilAPI/changePassword',
            form.serialize(),
            function (response) {

                dashboard.button.button('reset');

                if(response.errors){
                    for(var error in response.errors){
                        alert(response.errors[error]);
                    }
                }
            },
            'json'
        );

    }
};


$(function() {
    dashboard.get_perfil();

    $("#perfilSubmit").click( function(e) {

        var $myForm = $('#formPerfil');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            dashboard.button = $(this);
            dashboard.button.button('loading');
            dashboard.update_perfil($myForm);
            e.preventDefault();
            return false;
        }
    });

    $("#passwordModalSave").click( function(e) {

        var $myForm = $('#formChangePassword');

        if (!$myForm[0].checkValidity())
        {
            $myForm.submit();
        }
        else
        {
            dashboard.button = $(this);
            dashboard.button.button('loading');
            dashboard.change_password($myForm);
            e.preventDefault();
            return false;
        }
    });
});