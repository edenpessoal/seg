var metas = {

    page: null,
    funcionariosTable: null,
    buttonSave: null,
    novas_camadas_ps: 0,

    getMetas: function(){
        $.get(
            '/api/metasAPI/getMetas',
            '',
            function (response) {
                if(!response.errors)
                {
                    page.monta_pagina(response);

                    var startDate = $( "#meta_inicio" ).datepicker('getDate');
                    var endDate = $( "#meta_termino" ).datepicker('getDate');
                    if(startDate != null && endDate != null) {
                        var difference = endDate - startDate;
                        var weeks = difference / 1000 / 60 / 60 / 24 / 7;
                        $("#meta_qtd_semanas").val( Math.ceil(weeks) );
                        $("#meta_faturamento_qtd_semanas").val( Math.ceil(weeks) );
                    }

                    page.monta_camada_custom('idfuncionario', response['meta_faturamento_divisao']);

                    if(response['radioEqual']){
                        $("#meta_faturamento_divisao_0").prop('checked', true);
                        $("#meta_faturamento_divisao_1").prop('checked', false);
                    }else{
                        $("#meta_faturamento_divisao_1").prop('checked', true);
                        $("#meta_faturamento_divisao_0").prop('checked', false);
                        var $funcionarios = $("#tabela_meta_funcionarios_divisao #meta_funcionario_faturamento");
                        $funcionarios.prop("readonly", false);
                    }

                    if(response['meta_produtos_servicos'])
                    {
                        $("#meta_produtos").prop("checked", true);
                        $("#cadastro_metas_faturamento_produto_servicos").show();

                        for(var mps in response['meta_produtos_servicos'])
                        {
                            var nova_camada = $("#camada_novo_produto_servico").clone();
                            nova_camada.show();
                            nova_camada.attr("id", "novo_ps_" + metas.novas_camadas_ps);
                            nova_camada.find("#camada_novo_produto_index").val(metas.novas_camadas_ps);
                            nova_camada.find("#produto_servico_nome").val(response['meta_produtos_servicos'][mps]);

                            if (metas.novas_camadas_ps == 0) {
                                nova_camada.insertAfter("#camada_novo_produto_servico");
                            } else {
                                nova_camada.insertAfter("#novo_ps_" + (metas.novas_camadas_ps - 1));
                            }

                            $('#tabela_vendedor_x_ps').find('thead tr').each(function () {
                                $(this).find('th').eq(-1).after('<th>'+response['meta_produtos_servicos'][mps]+'</th>');
                            });

                            $('#tabela_vendedor_x_ps').find('tbody tr')
                            .each(function ()
                            {
                                var id_vendedor = $(this).find("#vendedor_ps_funcionario_id").val();

                                var camada = "<td><input type='text' id='vendedor_ps_meta' " +
                                    "name='vendedor_ps_meta[" + id_vendedor + "][]' " +
                                    "class='form-control' " +
                                    "idvendedor='" +id_vendedor+ "' idproduto='" +mps+ "' required /></td>";
                                $(this).find('td').eq(-1).after(camada);
                                $(this).find('#vendedor_ps_meta').maskMoney({prefix:'R$ ', allowNegative: false, thousands:'', decimal:'.', affixesStay: false})
                            });

                            nova_camada.find('#produto_servico_nome').prop('required', true).change(function () {
                                var index = $(this).parent().find("#camada_novo_produto_index").val();
                                index++;
                                $('#tabela_vendedor_x_ps').find('th:eq(' + index + ')').html($(this).val());
                            });

                            metas.novas_camadas_ps++;
                        }

                    }

                    if(response['meta_novos_clientes_value'])
                    {
                        $("#meta_adesao").prop("checked", true);
                        $("#cadastro_metas_adesao_novos_clientes").show();
                    }

                    if(response['meta_visitas_quantidade'])
                    {
                        $("#meta_visitas").prop("checked", true);
                        $("#cadastro_metas_visita_clientes").show();
                        $('input:radio[name="meta_visitas_somente_novos"]').filter('[value="' +response['apenas_novos']+ '"]').attr('checked', true);
                    }

                }
            },
            'json'
        );
    },

    checkMaxMetasAdicionais: function(element){

        var maxMetasAdicionais = 2;
        var currentlyChecked = 0;

        if($("#meta_produtos").is(':checked')){
            currentlyChecked++;
        }

        if($("#meta_adesao").is(':checked')){
            currentlyChecked++;
        }

        if($("#meta_visitas").is(':checked')){
            currentlyChecked++;
        }

        if(currentlyChecked > maxMetasAdicionais){
            alert("Você só pode selecionar 2 metas adicionais.");
            element.prop('checked', false);
            return false;
        }
        else{
            return true;
        }

    },

    hideElements: function(){
        $("#meta_funcionarios_divisao, " +
        "#cadastro_metas_faturamento_produto_servicos, #cadastro_metas_adesao_novos_clientes, " +
        "#cadastro_metas_visita_clientes, #meta_funcionarios_divisao, #vendedor_x_ps, " +
        "#camada_novo_produto_servico").hide();
    },

    mountTableMeta: function(){

        $.get(
            '/api/funcionariosAPI/getFuncionariosForMetas',
            '',
            function(response)
            {

                for(var i=0; i<response.length; i++)
                {
                    var novo_funcionario = $("#meta_funcionarios_divisao").clone();
                    novo_funcionario.find('#meta_funcionario_nome').html(response[i].nome);
                    novo_funcionario.find('#meta_funcionario_id').val(response[i].id);
                    novo_funcionario.find('#meta_funcionario_faturamento').attr('idfuncionario', response[i].id)
                        .maskMoney({prefix:'R$ ', allowNegative: false, thousands:'', decimal:'.', affixesStay: false});

                    novo_funcionario.find(':input').prop('required', true);

                    var novo_funcionario_ps = $("#vendedor_x_ps").clone();
                    novo_funcionario_ps.find('#vendedor_x_ps_nome').html(response[i].nome);
                    novo_funcionario_ps.find('#vendedor_ps_funcionario_id').val(response[i].id);
                    novo_funcionario_ps.find('#vendedor_ps_funcionario_index').val(i);

                    novo_funcionario.find('#meta_funcionario_faturamento').change(function(){
                        var soma_funcionarios = "0.00";

                        var $funcionarios = $("#tabela_meta_funcionarios_divisao #meta_funcionario_faturamento");
                        $funcionarios.each(function(index){
                            if($(this).is(':visible')) {
                                // soma_funcionarios += parseInt($(this).val(), 10);
                                soma_funcionarios = parseFloat(soma_funcionarios) + parseFloat($(this).val());
                            }
                        });

                        $("#meta_funcionario_totalizacao").html(soma_funcionarios.toFixed(2));
                        $("#meta_faturamento_valor").val(soma_funcionarios.toFixed(2));
                    });

                    novo_funcionario.show();
                    novo_funcionario_ps.show();

                    $("#tabela_meta_funcionarios_divisao > tbody").append(novo_funcionario);
                    $("#tabela_vendedor_x_ps > tbody").append(novo_funcionario_ps);
                }

                metas.maskInputs();
                metas.getMetas();
                metas.mountTableProdutosEServicos();

            },
            'json');

    },

    mountTableProdutosEServicos: function()
    {
       $.get(
            '/api/metasAPI/getProdutosEServicos',
            '',
            function (response)
            {
                if(!response.errors)
                {
                    var tabela = $("#tabela_vendedor_x_ps");

                    for(mps in response)
                    {
                        tabela.find('input[idvendedor='+mps+']').each(function () {
                            $(this).val(
                                response[mps][ $(this).attr('idproduto') ]
                            );
                        });
                    }


                }
            },
            'json'
        );
    },

    save: function(form){
        $.post(
            '/api/metasAPI/upsertMetas',
            form.serialize(),
            function (response) {

                metas.buttonSave.button('reset');

                if(response.errors){
                    alert(response.errors);
                }

                page.get_percentage();
            },
            'json'
        );
    },

    maskInputs: function (){
        $("#meta_faturamento_valor").maskMoney({prefix:'R$ ', allowNegative: false, thousands:'', decimal:'.', affixesStay: false});
    }

};

$(function() {


    metas.hideElements();
    metas.mountTableMeta();

    metas.page = $("#page_content");

    metas.page.find( "#meta_inicio" ).datepicker({
        dateFormat: 'dd-mm-yy',
        beforeShowDay: function(date){
            var day = date.getDay();
            return [day == 1, ""];
        },
        onSelect: function() {
            var startDate = $(this).datepicker('getDate');
            var endDate = $( "#meta_termino" ).datepicker('getDate');
            if(startDate != null && endDate != null) {
                var difference = endDate - startDate;
                var weeks = difference / 1000 / 60 / 60 / 24 / 7;
                $("#meta_qtd_semanas").val( Math.ceil(weeks) );
                $("#meta_faturamento_qtd_semanas").val( Math.ceil(weeks) );
            }
        }
    });

    metas.page.find( "#meta_termino" ).datepicker({
        dateFormat: 'dd-mm-yy',
        beforeShowDay: function(date){
            var day = date.getDay();
            return [day == 5, ""];
        },
        onSelect: function() {
            var startDate = $( "#meta_inicio" ).datepicker('getDate');
            var endDate = $(this).datepicker('getDate');
            if(startDate != null && endDate != null) {
                var difference = endDate - startDate;
                var weeks = difference / 1000 / 60 / 60 / 24 / 7;
                $("#meta_qtd_semanas").val( Math.ceil(weeks) );
                $("#meta_faturamento_qtd_semanas").val( Math.ceil(weeks) );
            }
        }
    });

    $("#meta_faturamento_valor").change(function(){

        var $funcionarios = $("#tabela_meta_funcionarios_divisao #meta_funcionario_faturamento");
        var qtd_funcionarios = $funcionarios.length - 1;
        var meta = $(this).val() / qtd_funcionarios;

        $funcionarios.each(function(index){
            $(this).val(meta);
        });


        // TODO(pplanel): inserir regra de grafico
        var chart = new Chart("container_grafico");
        var goal  = generateSeriesByGoal(parseInt($(this).val()), $("#meta_faturamento_qtd_semanas").val());
        console.log("goal: ", goal);
        var i = 0;
        var dataInicio = moment($("#meta_inicio").val(), 'DD-MM-YYYY');
        var dataTermino = moment($("#meta_termino").val(), 'DD-MM-YYYY')
        var range = moment.range(dataInicio, dataTermino);
        var xAxisCategories = [];

        range.by('week', function(moment){
          xAxisCategories.push(moment.toString());
          chart.xAxis[0].update({categories:xAxisCategories}, true);
          chart.series[0].addPoint(goal[i]);
          i++;
        });
        $("#container_grafico").show("fast");

        $("#meta_funcionario_totalizacao").html($(this).val());
    });

    $("#meta_faturamento_divisao_0").change(function () {
        if($('#meta_faturamento_divisao_0').is(':checked')){
            var $funcionarios = $("#tabela_meta_funcionarios_divisao #meta_funcionario_faturamento");
            $funcionarios.prop("readonly", true);

            var qtd_funcionarios = $funcionarios.length - 1;
            var meta = $("#meta_faturamento_valor").val() / qtd_funcionarios;

            $funcionarios.each(function(index){
                $(this).val(meta);
            });

            $("#meta_funcionario_totalizacao").html($("#meta_faturamento_valor").val());
        }
    });

    $("#meta_faturamento_divisao_1").change(function () {
        if($('#meta_faturamento_divisao_1').is(':checked')){
            var $funcionarios = $("#tabela_meta_funcionarios_divisao #meta_funcionario_faturamento");
            $funcionarios.prop("readonly", false);
        }
    });

    $("#meta_produtos").change(function(){
        if($(this).is(':checked') && metas.checkMaxMetasAdicionais($(this))){
            $("#cadastro_metas_faturamento_produto_servicos").show();
        }else{
            $("#cadastro_metas_faturamento_produto_servicos").hide();
        }
    });

    $("#meta_adesao").change(function(){
        if($(this).is(':checked') && metas.checkMaxMetasAdicionais($(this))){
            $("#cadastro_metas_adesao_novos_clientes").show();
        }else{
            $("#cadastro_metas_adesao_novos_clientes").hide();
        }
    });

    $("#meta_visitas").change(function(){
        if($(this).is(':checked') && metas.checkMaxMetasAdicionais($(this))){
            $("#cadastro_metas_visita_clientes").show();
        }else{
            $("#cadastro_metas_visita_clientes").hide();
        }
    });

    $("#add_service").click(function(){

        var nova_camada = $("#camada_novo_produto_servico").clone();
        nova_camada.show();
        nova_camada.attr("id", "novo_ps_" + metas.novas_camadas_ps);
        nova_camada.find("#camada_novo_produto_index").val(metas.novas_camadas_ps);

        if(metas.novas_camadas_ps == 0){
            nova_camada.insertAfter("#camada_novo_produto_servico");
        }else{
            nova_camada.insertAfter("#novo_ps_" + (metas.novas_camadas_ps - 1));
        }

        $('#tabela_vendedor_x_ps').find('thead tr').each(function(){
            $(this).find('th').eq(-1).after('<th></th>');
        });

        $('#tabela_vendedor_x_ps').find('tbody tr').each(function(){
            var id_vendedor = $(this).find("#vendedor_ps_funcionario_id").val();
            var camada = "<td><input type='text' id='vendedor_ps_meta' name='vendedor_ps_meta[" +id_vendedor+ "][]' class='form-control' required /></td>";
            $(this).find('td').eq(-1).after(camada);
            $(this).find('#vendedor_ps_meta').maskMoney({prefix:'R$ ', allowNegative: false, thousands:'', decimal:'.', affixesStay: false});
        });

        nova_camada.find('#produto_servico_nome').prop('required', true).change(function(){
            var index = $(this).parent().find("#camada_novo_produto_index").val();
            index++;
            $('#tabela_vendedor_x_ps').find('th:eq('+index+')').html($(this).val());
        });

        metas.novas_camadas_ps++;
    });

    $("#saveMetas").click( function(e) {

        var $myForm = $('#formDadosMetas');

        //if (!$myForm[0].checkValidity())
        //{
        //    $myForm.submit();
        //}
        //else
        //{
            metas.buttonSave = $(this);
            metas.buttonSave.button('loading');
            e.preventDefault();
            metas.save($myForm);
            return false;
        //}
    });


});
